"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description: State model testing of all pipelines
"""
import unittest

HOST = "127.0.0.1"
PORT = 1337

from mpikat.utils.testing import state_change_patch, Test_StateModel, Test_StateModelOnRequests
# Patch the state_change decorator / Must be called before importing pipeline implementations
unittest.mock.patch("mpikat.core.edd_pipeline_aio.state_change", wraps=state_change_patch).start()

from eddpaf.pipelines import ACMWriter, Beamformer, ComplexGainCalibrator, Correlator, WeightCalibrator

class Test_ACMWriter_StateModel(Test_StateModel):
    def setUp(self) -> None:
        super().setUpBase(ACMWriter(HOST, PORT), False)

class Test_ComplexGainCalibrator_StateModel(Test_StateModel):
    def setUp(self) -> None:
        super().setUpBase(ComplexGainCalibrator(HOST, PORT), False)

class Test_WeightCalibrator_StateModel(Test_StateModel):
    def setUp(self) -> None:
        super().setUpBase(WeightCalibrator(HOST, PORT), False)

class Test_Beamformer_StateModel(Test_StateModel):
    def setUp(self) -> None:
        super().setUpBase(Beamformer(HOST, PORT), True)

class Test_Correlator_StateModel(Test_StateModel):
    def setUp(self) -> None:
        super().setUpBase(Correlator(HOST, PORT), True)

# class Test_ACMWriter_StateModel(Test_StateModelOnRequests):
#     async def asyncSetUp(self) -> None:
#         await super().setUpBase(ACMWriter(HOST, PORT), False)

# class Test_ComplexGainCalibrator_StateModel(Test_StateModelOnRequests):
#     async def asyncSetUp(self) -> None:
#         await super().setUpBase(ComplexGainCalibrator(HOST, PORT), False)

# class Test_WeightCalibrator_StateModel(Test_StateModelOnRequests):
#     async def asyncSetUp(self) -> None:
#         await super().setUpBase(WeightCalibrator(HOST, PORT), False)

# class Test_Beamformer_StateModel(Test_StateModelOnRequests):
#     async def asyncSetUp(self) -> None:
#         await super().setUpBase(Beamformer(HOST, PORT), True)

# class Test_Correlator_StateModel(Test_StateModelOnRequests):
#     async def asyncSetUp(self) -> None:
#         await super().setUpBase(Correlator(HOST, PORT), True)

if __name__ == "__main__":
    unittest.main()
