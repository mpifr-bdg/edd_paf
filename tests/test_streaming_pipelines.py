"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description: Test all pipelines which starts streaming on capture_start.
    Here these are called streaming-pipelines
"""
import unittest
import json

from mpikat.utils.testing import setup_redis
from mpikat.core.edd_pipeline_aio import EDDPipeline
from mpikat.core import logger as logging

from eddpaf.pipelines import Beamformer, Correlator


_LOG = logging.getLogger("mpikat.dbbc.tests.test_streaming_pipelines")
HOST = "127.0.0.1"

class TEST_StreamingPipelines(unittest.IsolatedAsyncioTestCase):

    def setUp(self) -> None:
        print("Streaming pipelines")
        self.redis = setup_redis(HOST)
        return super().setUp()

    def tearDown(self) -> None:
        self.redis.shutdown()
        return super().tearDown()

    async def __sequence(self, pipeline: EDDPipeline, test_conf: dict={}):
        test_conf["data_store"] = {}
        test_conf["data_store"]["port"] = self.redis.server_config["port"]
        test_conf["data_store"]["ip"] = HOST

        await pipeline.set(json.dumps(test_conf).replace(" ", ""))
        self.assertEqual(pipeline.state, 'idle')
        await pipeline.configure()
        self.assertEqual(pipeline.state, 'configured')
        await pipeline.capture_start()
        self.assertEqual(pipeline.state, 'streaming')
        await pipeline.measurement_prepare()
        self.assertEqual(pipeline.state, 'streaming')
        await pipeline.measurement_start()
        self.assertEqual(pipeline.state, 'streaming')
        await pipeline.measurement_stop()
        self.assertEqual(pipeline.state, 'streaming')
        await pipeline.deconfigure()
        self.assertEqual(pipeline.state, 'idle')
        _LOG.info("Pipeline deconfigured")

    async def test_Beamformer_sequence(self):
        _LOG.info("Testing Beamformer with default configuration")
        pipeline = Beamformer("localhost", 1234)
        test_conf = {
            "input_type":"dummy",
            "output_type":"null",
            "nonfatal_numacheck":False
        }
        _LOG.info("Setted configuration")
        await self.__sequence(pipeline, test_conf)

    async def test_Correlator_sequence(self):
        _LOG.info("Testing Correlator with default configuration")
        pipeline = Correlator("localhost", 1234)
        test_conf = {
            "input_type":"dummy",
            "output_type":"null",
            "nonfatal_numacheck":False
        }
        _LOG.info("Setted configuration")
        await self.__sequence(pipeline, test_conf)

if __name__ == '__main__':
    unittest.main()
