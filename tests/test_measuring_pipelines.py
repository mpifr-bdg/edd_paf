"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description: Test al pipelines which are implementing the commands measurement_prepare, measurement_start,
    measruement_stop. Here these are called measure-pipelines
"""

import unittest

from mpikat.core.edd_pipeline_aio import EDDPipeline
from mpikat.core import logger as logging

from eddpaf.pipelines.acm_writer import ACMWriter
from eddpaf.pipelines.cg_calibrator import ComplexGainCalibrator
from eddpaf.pipelines.weight_calibrator import WeightCalibrator


_LOG = logging.getLogger()

class Test_MeasuringPipelines(unittest.IsolatedAsyncioTestCase):


    async def __sequence(self, pipeline: EDDPipeline):
        await pipeline.configure()
        self.assertEqual(pipeline.state, 'configured')
        await pipeline.capture_start()
        self.assertEqual(pipeline.state, 'ready')
        await pipeline.measurement_prepare()
        self.assertEqual(pipeline.state, 'set')
        await pipeline.measurement_start()
        self.assertEqual(pipeline.state, 'measuring')
        await pipeline.measurement_stop()
        self.assertEqual(pipeline.state, 'ready')
        await pipeline.capture_stop()
        self.assertEqual(pipeline.state, 'idle')
        await pipeline.deconfigure()
        self.assertEqual(pipeline.state, 'idle')

    async def test_ACMWriter_sequence(self):
        pipeline = ACMWriter("localhost", 1234)
        self.assertEqual(pipeline.state, 'idle')
        await pipeline.set('{"input_type":"dummy","output_type":"null"}')
        await self.__sequence(pipeline)

    async def test_ComplexGainCalibrator_sequence(self):
        pipeline = ComplexGainCalibrator("localhost", 1234)
        self.assertEqual(pipeline.state, 'idle')
        await pipeline.set('{"input_type":"dummy","output_type":"null"}')
        await self.__sequence(pipeline)

    async def test_WeightCalibrator_sequence(self):
        pipeline = WeightCalibrator("localhost", 1234)
        self.assertEqual(pipeline.state, 'idle')
        await pipeline.set('{"input_type":"dummy","output_type":"null"}')
        await self.__sequence(pipeline)


if __name__ == '__main__':
    unittest.main()
