# find_package(tensor_core_correlator)
# if(NOT tensor_core_correlator_FOUND)
    message(STATUS "TCC package not found, installing from remote")
    include(ExternalProject)
    ExternalProject_Add(
        tcc
        GIT_REPOSITORY https://git.astron.nl/RD/tensor-core-correlator
        PREFIX ${CMAKE_BINARY_DIR}/tcc
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=/usr/local/ -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE})
    set(TCC_INCLUDE_DIR ${CMAKE_INSTALL_PREFIX}/include/)
    set(TCC_LIBRARY_DIR ${CMAKE_INSTALL_PREFIX}/lib/)
    set(TCC_LIBRARIES libtcc.so)
    add_dependencies(tcc cuwrappers)
    include_directories(${TCC_INCLUDE_PATH})
# endif()