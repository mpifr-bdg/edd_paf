# find_package(cudawrappers)
# if(NOT cudawrappers_FOUND)
    message(STATUS "cuwrappers package not found, installing from remote")
    include(ExternalProject)
    ExternalProject_Add(
        cuwrappers
        GIT_REPOSITORY https://github.com/nlesc-recruit/cudawrappers
        GIT_TAG main
        PREFIX ${CMAKE_BINARY_DIR}/cuwrappers
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=/usr/local/ -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE})
    set(CUWRAPPERS_INCLUDE_DIR ${CMAKE_INSTALL_PREFIX}/include/)
    include_directories(${CUWRAPPERS_INCLUDE_DIR})
# endif()