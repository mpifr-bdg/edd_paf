include(cmake/cuda.cmake)
include(cmake/compiler_settings.cmake)
include(cmake/psrdada.cmake)
include(cmake/psrdadacpp.cmake)
# include(cmake/redis.cmake)
include(cmake/cuwrappers.cmake)
include(cmake/googletest.cmake)
include(cmake/googlebenchmark.cmake)
include(cmake/tcc.cmake)
find_package(Boost COMPONENTS log program_options system date_time thread REQUIRED)

set(DEPENDENCY_LIBRARIES
    ${PSRDADA_LIBRARIES}
    ${PSRDADACPP_LIBRARIES}
    ${CUDA_LIBRARIES}
    ${OpenMP_EXE_LINKER_FLAGS}
    -lcuda
    -lboost_log
    -lboost_log_setup
    -lboost_system
    -lboost_date_time
    -lboost_program_options
    -lboost_thread
)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")