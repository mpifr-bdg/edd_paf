find_package(CUDA REQUIRED)
include_directories(${CUDA_TOOLKIT_INCLUDE})

set(CUDA_HOST_COMPILER ${CMAKE_CXX_COMPILER})
set(CUDA_PROPAGATE_HOST_FLAGS OFF)
set(CMAKE_CUDA_STANDARD 17)
list(APPEND CUDA_NVCC_FLAGS -DENABLE_CUDA --std=c++${CMAKE_CXX_STANDARD} -Wno-deprecated-gpu-targets --ptxas-options=-v)
list(APPEND CUDA_NVCC_FLAGS_DEBUG -O0 -g -G -Xcompiler "-Wextra"  -Xcudafe "--diag_suppress=20012") #--Werror all-warnings)
list(APPEND CUDA_NVCC_FLAGS_PROFILE --generate-line-info)
list(APPEND CUDA_NVCC_FLAGS_RELEASE -O3 -use_fast_math -restrict)
# list(APPEND CUDA_NVCC_FLAGS_RELEASE -gencode=arch=compute_75,code=sm_75) # GeForce 2080
# list(APPEND CUDA_NVCC_FLAGS_RELEASE -gencode=arch=compute_80,code=sm_80) # A100
if(CUDA_VERSION GREATER_EQUAL 11.1)
  message(STATUS "Enabling device specific (arch=86)")
  list(APPEND CUDA_NVCC_FLAGS_RELEASE -gencode=arch=compute_86,code=sm_86) # GeForce 3090
endif(CUDA_VERSION GREATER_EQUAL 11.1)
if(CUDA_VERSION GREATER_EQUAL 11.8)
  message(STATUS  "Enabling device specific (arch=89)")
  list(APPEND CUDA_NVCC_FLAGS_RELEASE -gencode=arch=compute_89,code=sm_89) # L40
endif(CUDA_VERSION GREATER_EQUAL 11.8)

set(CMAKE_CXX_FLAGS "-DENABLE_CUDA ${CMAKE_CXX_FLAGS}")
message(STATUS "CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE_UPPER}")
if(CMAKE_BUILD_TYPE_UPPER STREQUAL "DEBUG")
    set(CUDA_NVCC_FLAGS "${CUDA_NVCC_FLAGS} ${CUDA_NVCC_FLAGS_DEBUG}")
elseif(CMAKE_BUILD_TYPE_UPPER STREQUAL "RELEASE")
    set(CUDA_NVCC_FLAGS "${CUDA_NVCC_FLAGS} ${CUDA_NVCC_FLAGS_RELEASE}")
elseif(CMAKE_BUILD_TYPE_UPPER STREQUAL "PROFILE")
    set(CUDA_NVCC_FLAGS "${CUDA_NVCC_FLAGS} ${CUDA_NVCC_FLAGS_PROFILE}")
endif()
