if(ENABLE_TESTING)
    include(ExternalProject)
    ExternalProject_Add(
        googletest
        GIT_REPOSITORY https://github.com/google/googletest.git
        GIT_TAG v1.14.0
        INSTALL_COMMAND ""
        CMAKE_ARGS -DINSTALL_GTEST=OFF
        )
    set(GTEST_LIBRARIES libgtest_main.a libgtest.a libgmock_main.a libgmock.a)

    #     Specify include dir
ExternalProject_Get_Property(googletest source_dir)
set(GTEST_INCLUDE_DIR ${source_dir}/googletest/include ${source_dir}/googlemock/include)

# Library
ExternalProject_Get_Property(googletest binary_dir)
set(GTEST_LIBRARY_DIR ${binary_dir}/${CMAKE_FIND_LIBRARY_PREFIXES})

message(STATUS "GTEST_INCLUDE_DIR: ${GTEST_INCLUDE_DIR}")
message(STATUS "GTEST_LIBRARY_DIR: ${GTEST_LIBRARY_DIR}")


endif()
