include(ExternalProject)
ExternalProject_Add(
    hiredis
    GIT_REPOSITORY https://github.com/redis/hiredis.git
    PREFIX ${CMAKE_BINARY_DIR}/hiredis
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=/usr/local/ -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE})
set(HIREDIS_INCLUDE_DIR ${CMAKE_INSTALL_PREFIX}/include/)
set(HIREDIS_LIBRARY_DIR ${CMAKE_INSTALL_PREFIX}/lib/)
set(HIREDIS_LIBRARIES libhiredis.so)
include_directories(${HIREDIS_INCLUDE_DIR})

include(ExternalProject)
ExternalProject_Add(
    redis-plus-plus
    GIT_REPOSITORY https://github.com/sewenew/redis-plus-plus.git
    PREFIX ${CMAKE_BINARY_DIR}/redis-plus-plus
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=/usr/local/ -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE})
set(REDISCPP_INCLUDE_DIR ${CMAKE_INSTALL_PREFIX}/include/)
set(REDISCPP_LIBRARY_DIR ${CMAKE_INSTALL_PREFIX}/lib/)
set(REDISCPP_LIBRARIES libredis++.a)
include_directories(${REDISCPP_INCLUDE_DIR})