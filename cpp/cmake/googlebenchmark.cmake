if(ENABLE_BENCHMARK)
    include(ExternalProject)
    ExternalProject_Add(
        googlebenchmark
        GIT_REPOSITORY https://github.com/google/benchmark.git
        GIT_TAG v1.8.3
        INSTALL_COMMAND ""
        CMAKE_ARGS -DBENCHMARK_ENABLE_TESTING=FALSE -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
        )

    ExternalProject_Get_Property(googlebenchmark  source_dir)
    set(BENCHMARK_INCLUDE_DIR ${source_dir}/include)

    # Library
    ExternalProject_Get_Property(googlebenchmark binary_dir)
    set(BENCHMARK_LIBRARY_DIR ${binary_dir}/src)

    message(STATUS "BENCHAMRK_INCLUDE_DIR: ${BENCHMARK_INCLUDE_DIR}")
    message(STATUS "BENCHMARK_LIBRARY_DIR: ${BENCHMARK_LIBRARY_DIR}")

    set(BENCHMARK_LIBRARIES libbenchmark_main.a libbenchmark.a)
endif()

