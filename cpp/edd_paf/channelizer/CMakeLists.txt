set_target_properties(${CMAKE_PROJECT_NAME} PROPERTIES LINKER_LANGUAGE CXX)

add_executable(dump_udp dump_udp.cpp)
target_link_libraries(dump_udp -lm)
install(TARGETS dump_udp DESTINATION bin)
