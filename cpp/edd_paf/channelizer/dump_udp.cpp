// udprec_cpaf.c	19. October 2023	by eddy

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <endian.h>
#include <time.h>
#include <math.h>

#define UDP_PORT 7148
#define BUFSIZE 9000
#define HEAD_SIZE 96

unsigned long forced_sample_frequency = 0;
int selected_element = -1;
int get_payloadlength(char *buf);
uint64_t get_heapid(char *buf);
uint64_t get_timestamp(char *buf);
uint16_t get_channelid(char *buf);
uint16_t get_elementid(char *buf);
uint16_t bitreverse10(uint16_t);


void handle_packet_default(char *buf) // Print SPEAD header and data as 64 bit values.
  {
  int i,len;
  unsigned char *p;
  uint64_t val;
  printf("###\n");
  p = (unsigned char*)buf;
  for(i=0;i<12;i++) // SPEAD header
    {
    val = *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    printf("SPEAD 0x%016lX\n",val);
    }
  len = 8*get_payloadlength((char*)buf)/64; // 1024
  p = (unsigned char*)buf+HEAD_SIZE;
  for(i=0;i<len;i++) // Data
    {
    val = *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    printf("DATA 0x%016lX\n",val);
    }
  }


void handle_packet_list64bit(char *buf) // Print SPEAD header and data as 64 bit values.
  {
  int i,len;
  unsigned char *p;
  uint64_t val;
  printf("###\n");
  p = (unsigned char*)buf;
  for(i=0;i<12;i++) // SPEAD header
    {
    val = *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    printf("SPEAD 0x%016lX\n",val);
    }
  len = 8*get_payloadlength(buf)/64; // 1024
  p = (unsigned char*)buf+HEAD_SIZE;
  for(i=0;i<len;i++) // Data
    {
    val = *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    val = (val << 8) + *p++;
    printf("DATA 0x%016lX\n",val);
    }
  }


void handle_packet_test(char *buf)
  {
  int i, n, flag;
  uint16_t *p;
  uint16_t ref, val, fst, lst;
  char *msg;
  uint16_t elementid, channelid;
  p = (uint16_t*)(buf+HEAD_SIZE);
  elementid = get_elementid(buf);
  channelid = get_channelid(buf);
//  printf("element id %5u\n", elementid);
//  printf("channel id %5u\n", channelid);
  msg = "ok";
  n = get_payloadlength(buf)/sizeof(uint16_t);
  fst = p[0];   // First word (0xABBA).
//  ref = p[1];   // Second word = reference.
  ref = p[0];   // First word = reference.
  lst = p[n-1]; // Last word (0xEBBE).
  flag = 0;
//  if(ntohs(fst) != 0xABBA) flag = 1;
//  if(ntohs(lst) != 0xEBBE) flag = 1;
  val = ntohs(ref);
  if((val >> 12)    != elementid) flag = 1;
  if((val & 0x0FFF) != channelid) flag = 1;
//  for(i=2; i<n-1; ++i)
  for(i=1; i<n; ++i)
    {
    val = p[i];
    if(val != ref) flag = 1;
    }
  if(flag) msg = "bad";
//  printf("%04X %04X %04X %04X %s\n", ntohs(fst), ntohs(ref), ntohs(val), ntohs(lst), msg);
  printf("%04X %04X %s\n", ntohs(ref), ntohs(val), msg);
  }


void handle_packet_continuity(char *buf)
  {
  uint16_t elementid, channelid;
  int64_t missing;
  uint64_t timestamp, count;
  static uint64_t oldcount = 0;
  elementid = get_elementid(buf);
  timestamp = get_timestamp(buf);
  count = (timestamp/4096*8) + elementid;
//  printf("cnt %lu  ele %u  time %lu \n", count, elementid, timestamp);
  if(count != oldcount+1)
    {
    if(oldcount !=0)
      {
      missing = count - (oldcount+1);
      printf("channel=%u count=%lu missing %ld packets\n", get_channelid(buf), count, missing);
      }
    }
  if((count & 0x000000000000FFFF) == 0)
    {
    printf("channel=%u count=%lu\n", get_channelid(buf), count);
    }
  oldcount = count;
  }


void handle_packet_increment(char *buf)
  {
  int i,len;
  unsigned char *p;
  uint16_t val, tmp, elementid, channelid, eidfromdif, cidfromdif;
  int dif, eid, refdif;
  static uint16_t oldval[8] = {0,0,0,0,0,0,0,0};
  uint64_t timestamp;
  static uint64_t oldtimestamp[8] = {0,0,0,0,0,0,0,0};
  int timedif;

  timestamp = get_timestamp(buf);
  channelid = get_channelid(buf);
  channelid = bitreverse10(channelid);
  elementid = get_elementid(buf);
  eid = elementid & 0x0007;

//  if(eid!=0) return;

  timedif = timestamp - oldtimestamp[eid];
  oldtimestamp[eid] = timestamp;
  refdif = 1024*2*elementid+2*channelid+1;
//  printf("### td=%d ch=%d tr=%d rd=%d\n", timedif, channelid, elementid, refdif);

  len = 8*get_payloadlength(buf)/16; // 4096
  p = (unsigned char*)buf+HEAD_SIZE;
  for(i=0;i<len;i++)
    {
    val = *p++;
    val = (val << 8) + *p++;

    dif = (int)val - (int)oldval[eid];
    if(dif<0) dif += 65536;

    tmp = dif;
    cidfromdif = (tmp & 0x07FE) >> 1;
    eidfromdif = (tmp & 0x3800) >> 11;

    oldval[eid] = val;

    if(dif != refdif)
      {
      if(timedif > 0)
        printf("err ch=%4d tr=%3d s=%4d v=0x%04X td=%d\n", channelid, elementid, i, val, timedif);
      else printf("syncdetected element %d\n",eid);
      }
//    printf("0x%04X %d %d %d\n",val, dif, cidfromdif, eidfromdif);
    }
  }


void handle_packet_spead(char *buf) // Print detailed SPEAD information.
  {
  uint16_t item_dscr;
  uint64_t item_addr, val, header, heapsize;
  uint16_t elementid, channelid, hardwareid;
  uint64_t *qword;
  qword = (uint64_t*)buf;
  int badflag  = 0;
  char sbad[]  = "bad!";
  char sfine[] = "";
  char *msg = sfine;
  // spead header:
  header = be64toh(*qword);
  //printf("### raw 0x%016lX\n",header);
  // Todo: analyze header in more detail like flavour, # of item pointers, etc.
  badflag = 0;
  if(header != 0x530402060000000B) badflag = 1;
  if(badflag) msg = sbad; else msg = sfine;
  printf("SPEAD  header       0x%016lX %s\n", header, msg);
  // item 01 heap ID:
  qword += 1;
  val = be64toh(*qword);
  //printf("### raw 0x%016lX\n",val);
  item_dscr = val >> 48;
  item_addr  = 0x0000FFFFFFFFFFFF & val;
  badflag = 0;
  if(item_dscr != 0x8001) badflag = 1;
  item_dscr &= 0x7FFF;
  if(item_addr != get_heapid(buf)) badflag = 1;
  if(badflag) msg = sbad; else msg = sfine;
  printf("0x%04X heap ID          0x%012lX %s\n", item_dscr, item_addr, msg);
  // item 02 heap size:
  qword += 1;
  val = be64toh(*qword);
  //printf("### raw 0x%016lX\n",val);
  item_dscr = val >> 48;
  item_addr = 0x0000FFFFFFFFFFFF & val;
  badflag = 0;
  if(item_dscr != 0x8002) badflag = 1;
  item_dscr &= 0x7FFF;
  if(item_addr < 8192) badflag = 1;
  if(item_addr % 8192) badflag = 1;
  if(badflag) msg = sbad; else msg = sfine;
  printf("0x%04X heap size       %15lu %s\n", item_dscr, item_addr, msg);
  heapsize = item_addr;
  // item 03 heap offset:
  qword += 1;
  val = be64toh(*qword);
  //printf("### raw 0x%016lX\n",val);
  item_dscr = val >> 48;
  item_addr = 0x0000FFFFFFFFFFFF & val;
  badflag = 0;
  if(item_dscr != 0x8003) badflag = 1;
  item_dscr &= 0x7FFF;
  if(item_addr % 8192) badflag = 1;
  if(item_addr > heapsize-8192) badflag = 1;
  if(badflag) msg = sbad; else msg = sfine;
  printf("0x%04X heap offset     %15lu %s\n", item_dscr, item_addr, msg);
  // item 04 payload size:
  qword += 1;
  val = be64toh(*qword);
  //printf("### raw 0x%016lX\n",val);
  item_dscr = val >> 48;
  item_addr = 0x0000FFFFFFFFFFFF & val;
  badflag = 0;
  if(item_dscr != 0x8004) badflag = 1;
  item_dscr &= 0x7FFF;
  if(item_addr != 8192) badflag = 1;
  if(badflag) msg = sbad; else msg = sfine;
  printf("0x%04X payload size    %15lu %s\n", item_dscr, item_addr, msg);
  // item 05 time stamp:
  qword += 1;
  val = be64toh(*qword);
  //printf("### raw 0x%016lX\n",val);
  item_dscr = val >> 48;
  item_addr = 0x0000FFFFFFFFFFFF & val;
  badflag = 0;
  if(item_dscr != 0x9600) badflag = 1;
  item_dscr &= 0x7FFF;
  if(item_addr % 4096) badflag = 1;
  if(badflag) msg = sbad; else msg = sfine;
  printf("0x%04X time stamp      %15lu %s\n", item_dscr, item_addr, msg);
  //printf("0x%04X time stamp       0x%012lX %s\n",item_dscr,item_addr,msg);
  // item 06 order vector
  qword += 1;
  val = be64toh(*qword);
  //printf("### raw 0x%016lX\n",val);
  item_dscr = val >> 48;
  item_addr = 0x0000FFFFFFFFFFFF & val;
  badflag = 0;
  if(item_dscr != 0x9601) badflag = 1;
  item_dscr &= 0x7FFF;
  elementid  =  0x000000000000FFFF & item_addr;
  channelid  = (0x00000000FFFF0000 & item_addr) >> 16;
  hardwareid = (0x0000FFFF00000000 & item_addr) >> 32;
  if((elementid >= 256) || badflag) msg = sbad; else msg = sfine;
  printf("0x%04X element id                %5u %s\n", item_dscr, elementid, msg);
  if((channelid >= 1024) || badflag) msg = sbad; else msg = sfine;
  printf("0x%04X channel id                %5u %s\n", item_dscr, channelid, msg);
  if(badflag) msg = sbad; else msg = sfine;
  printf("0x%04X hardware id              0x%04X %s\n", item_dscr, hardwareid, msg);
  // item 07 clipping count:
  qword += 1;
  val = be64toh(*qword);
  //printf("### raw 0x%016lX\n",val);
  item_dscr = val >> 48;
  item_addr = 0x0000FFFFFFFFFFFF & val;
  badflag = 0;
  if(item_dscr != 0x9602) badflag = 1;
  item_dscr &= 0x7FFF;
  if(badflag) msg = sbad; else msg = sfine;
  printf("0x%04X clipping cnt    %15lu %s\n", item_dscr, item_addr, msg);
  // item 08 scale factor
  qword += 1;
  val = be64toh(*qword);
  //printf("### raw 0x%016lX\n",val);
  item_dscr = val >> 48;
  item_addr = 0x0000FFFFFFFFFFFF & val;
  badflag = 0;
  if(item_dscr != 0x9603) badflag = 1;
  item_dscr &= 0x7FFF;
  if(badflag) msg = sbad; else msg = sfine;
  printf("0x%04X scale factor    %15lu %s\n", item_dscr, item_addr, msg);
  // item 09 error vector:
  qword += 1;
  val = be64toh(*qword);
  //printf("### raw 0x%016lX\n",val);
  item_dscr = val >> 48;
  item_addr = 0x0000FFFFFFFFFFFF & val;
  badflag = 0;
  if(item_dscr != 0x9604) badflag = 1;
  item_dscr &= 0x7FFF;
  if(item_addr) badflag = 1;
  if(badflag) msg = sbad; else msg = sfine;
  printf("0x%04X error vector     0x%012lX %s\n", item_dscr, item_addr, msg);
  // item 10 reserved
  qword += 1;
  val = be64toh(*qword);
  //printf("### raw 0x%016lX\n",val);
  item_dscr = val >> 48;
  item_addr = 0x0000FFFFFFFFFFFF & val;
  badflag = 0;
  if(item_dscr != 0x9605) badflag = 1;
  item_dscr &= 0x7FFF;
  //if(item_addr) badflag = 1;
  if(badflag) msg = sbad; else msg = sfine;
  printf("0x%04X reserved         0x%012lX %s\n", item_dscr, item_addr, msg);
  // item 11 data offset
  qword += 1;
  val = be64toh(*qword);
  //printf("### raw 0x%016lX\n",val);
  item_dscr = val >> 48;
  item_addr = 0x0000FFFFFFFFFFFF & val;
  badflag = 0;
  if(item_dscr != 0x1606) badflag = 1;
  item_dscr &= 0x7FFF;
  if(item_addr) badflag = 1;
  if(badflag) msg = sbad; else msg = sfine;
  printf("0x%04X data offset     %15lu %s\n", item_dscr, item_addr, msg);
  // end:
  printf("\n");
  }


void handle_packet_timestamp(char *buf) // ueberarbeiten !!!
  {
  unsigned char *p;
  uint64_t sampfreq;
  uint64_t val,heapid,speadtime,timebysamp;
  uint64_t *qword;
  struct timespec tsnow,tsstamp,tsepoch;
  struct timeval tvepoch;
  uint64_t sec,nsec;
  char timestr[64];
  char fraction[16];
  // Get time:
  clock_gettime(CLOCK_REALTIME,&tsnow);
  // Get sample frequency:
  if(forced_sample_frequency == 0) sampfreq = 778646;
  else sampfreq = forced_sample_frequency;
  // Heap ID:
  qword = (uint64_t*)buf;
  val = be64toh(*(qword+1));
  heapid = 0x0000FFFFFFFFFFFF & val;
  // SPEAD time:
  val = be64toh(*(qword+5));
  speadtime = 0x0000FFFFFFFFFFFF & val;
  // Calculate Epoch:
  tsstamp.tv_sec  = speadtime/sampfreq;
  tsstamp.tv_nsec = 1000000000*(speadtime%sampfreq)/sampfreq;
  tsepoch.tv_sec  = tsnow.tv_sec  - tsstamp.tv_sec;
  tsepoch.tv_nsec = tsnow.tv_nsec - tsstamp.tv_nsec;
  while(tsepoch.tv_nsec < 0)
    {
    tsepoch.tv_sec  -= 2;
    tsepoch.tv_nsec += 2000000000;
    }
  strftime(timestr, 25, "%d.%m.%Y %Z %H:%M:%S", localtime(&tsepoch.tv_sec));
  snprintf(fraction,sizeof(fraction),".%09ld",tsepoch.tv_nsec);
  strcat(timestr,fraction);
  printf("heapid 0x%012lX, speadtime %015lu, time %s, epoch %jd\n", heapid, speadtime, timestr, (intmax_t)tsepoch.tv_sec);
}


void handle_packet_hexdump(char *buf)
  {
  int i,j,len;
  unsigned char *p;
  len = get_payloadlength(buf)/32; // 256
  printf("###\n");
  p = (unsigned char*)buf+HEAD_SIZE;
  for(j=0;j<len;j++)
    {
//  for(i=0;i<32;i++) printf("%02X ",p[i]);
    for(i=31;i>=0;i--) printf("%02X ",p[i]);
    printf("\n");
    p += 32;
    }
  }


void handle_packet_real_imag(char *buf)
  { // Print 4096 complex numbers as real and imaginary, 8 bit signed integer each.
  int i, len, eid, cid;
  int8_t re, im;
  uint64_t val64;
  uint64_t *qword;
  eid = get_elementid(buf);
  cid = get_channelid(buf);
  if(selected_element >= 0)
    {
    if(selected_element != eid) return;
    }
  qword = (uint64_t *)(buf+HEAD_SIZE);
  len = get_payloadlength(buf)*8/64; // 1024
  /**/printf("### channel=%d element=%d\n", cid, eid);
  for(i=0;i<len;i++)
    {
    val64 = be64toh(*qword);
    qword++;
    re = (int64_t)((0xFF00000000000000 & val64) <<  0) >> 56;
    im = (int64_t)((0x00FF000000000000 & val64) <<  8) >> 56; printf("%4d %4d\n",re,im);
    re = (int64_t)((0x0000FF0000000000 & val64) << 16) >> 56;
    im = (int64_t)((0x000000FF00000000 & val64) << 24) >> 56; printf("%4d %4d\n",re,im);
    re = (int64_t)((0x00000000FF000000 & val64) << 32) >> 56;
    im = (int64_t)((0x0000000000FF0000 & val64) << 40) >> 56; printf("%4d %4d\n",re,im);
    re = (int64_t)((0x000000000000FF00 & val64) << 48) >> 56;
    im = (int64_t)((0x00000000000000FF & val64) << 56) >> 56; printf("%4d %4d\n",re,im);
    }
  }


void handle_packet_mag_phase(char *buf)
  { // Print 4096 complex numbers as magnitude and phase.
  int i, len, eid, cid;
  int8_t re, im;
  double mag, pha;
  const static double DEGREE=180/M_PI;
  uint64_t val64;
  uint64_t *qword;
  eid = get_elementid(buf);
  cid = get_channelid(buf);
  if(selected_element >= 0)
    {
    if(selected_element != eid) return;
    }
  qword = (uint64_t *)(buf+HEAD_SIZE);
  len = get_payloadlength(buf)*8/64; // 1024
  /**/printf("### channel=%d element=%d\n", cid, eid);
  for(i=0;i<len;i++)
    {
    val64 = be64toh(*qword);
    qword++;
    re = (int64_t)((0xFF00000000000000 & val64) <<  0) >> 56;
    im = (int64_t)((0x00FF000000000000 & val64) <<  8) >> 56;
    mag = sqrt(re*re+im*im);
    pha = atan2(im,re) * DEGREE;
    printf("%7.3f %8.3f\n",mag,pha);
    re = (int64_t)((0x0000FF0000000000 & val64) << 16) >> 56;
    im = (int64_t)((0x000000FF00000000 & val64) << 24) >> 56;
    mag = sqrt(re*re+im*im);
    pha = atan2(im,re) * DEGREE;
    printf("%7.3f %8.3f\n",mag,pha);
    re = (int64_t)((0x00000000FF000000 & val64) << 32) >> 56;
    im = (int64_t)((0x0000000000FF0000 & val64) << 40) >> 56;
    mag = sqrt(re*re+im*im);
    pha = atan2(im,re) * DEGREE;
    printf("%7.3f %8.3f\n",mag,pha);
    re = (int64_t)((0x000000000000FF00 & val64) << 48) >> 56;
    im = (int64_t)((0x00000000000000FF & val64) << 56) >> 56;
    mag = sqrt(re*re+im*im);
    pha = atan2(im,re) * DEGREE;
    printf("%7.3f %8.3f\n",mag,pha);
    }
  }


void handle_packet_numbers(char *buf)
  {
  handle_packet_real_imag(buf);
  }


void handle_packet_dummy(char *buf)
  {
  int val;
  val = get_payloadlength(buf);
//  printf("payloadlength: %d\n",val);
  }


void usage(void)
  {
  fprintf(stderr,"Usage: udprec [-p portnumber] [-m multicastgroup] [-e elementnumber] [-s samplefreq] | [-i interface address]\n");
  fprintf(stderr,"              [-spead | -timestamp | -hexdump | -list64bit | -numbers | -real_imag |\n");
  fprintf(stderr,"              [-mag_phase | -test | -continuity | -increment | -dummy | -help \n");
  fprintf(stderr,"Example: udprec -p 7148 -m 225.0.1.55 -e 1023 -real_imag\n");
  exit(0);
  }


int main(int argc,char *argv[])
  {
  int arg,s,rc,n,len;
  uint64_t i;
  struct sockaddr_in cliAddr,servAddr;
  static struct ip_mreq command;
  char buffer[BUFSIZE];
  const int    y = 1;
  const u_char z = 1;
  void (*handle_packet)(char *buf);
  uint16_t udp_port;
  char *mcast_grp; char* interface_address;
  in_addr_t interface;

  // Set up handle_packet function, parse arguments:
  arg = 1;
  handle_packet = handle_packet_default;
  udp_port = UDP_PORT;
  mcast_grp = NULL;
  while(arg < argc)
    {
    //printf("%s\n",argv[arg]);
    if(     !strcmp(argv[arg],"-spead"     )) handle_packet = handle_packet_spead;
    else if(!strcmp(argv[arg],"-timestamp" )) handle_packet = handle_packet_timestamp;
    else if(!strcmp(argv[arg],"-hexdump"   )) handle_packet = handle_packet_hexdump;
    else if(!strcmp(argv[arg],"-list64bit" )) handle_packet = handle_packet_list64bit;
    else if(!strcmp(argv[arg],"-numbers"   )) handle_packet = handle_packet_numbers;
    else if(!strcmp(argv[arg],"-real_imag" )) handle_packet = handle_packet_real_imag;
    else if(!strcmp(argv[arg],"-mag_phase" )) handle_packet = handle_packet_mag_phase;
    else if(!strcmp(argv[arg],"-test"      )) handle_packet = handle_packet_test;
    else if(!strcmp(argv[arg],"-continuity")) handle_packet = handle_packet_continuity;
    else if(!strcmp(argv[arg],"-increment" )) handle_packet = handle_packet_increment;
    else if(!strcmp(argv[arg],"-dummy"     )) handle_packet = handle_packet_dummy;
    else if(!strcmp(argv[arg],"-p")) { arg++; udp_port = atoi(argv[arg]); }
    else if(!strcmp(argv[arg],"-i")) { arg++; interface_address = argv[arg]; }
    else if(!strcmp(argv[arg],"-m")) { arg++; mcast_grp = argv[arg]; }
    else if(!strcmp(argv[arg],"-e")) { arg++; selected_element = atoi(argv[arg]); }
    else if(!strcmp(argv[arg],"-s")) { arg++; forced_sample_frequency = atol(argv[arg]); }
    else usage();
    arg++;
    }

  // Create socket:
  s = socket(AF_INET,SOCK_DGRAM,0);
  if(s<0)
    {
    fprintf(stderr,"ERROR: can't create socket: %s\n",strerror(errno));
    exit(EXIT_FAILURE);
    }
  (inet_addr(interface_address) != INADDR_NONE) ? interface = inet_addr(interface_address) : interface = INADDR_ANY;

  // Bind local server port:
  servAddr.sin_family = AF_INET;
  servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servAddr.sin_port = htons(udp_port);
  setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &y, sizeof(int));
  rc = bind (s, (struct sockaddr *) &servAddr, sizeof (servAddr));
  if(rc < 0)
  {
    char ip_str[INET_ADDRSTRLEN];
    const char *result = inet_ntop(AF_INET, &servAddr, ip_str, INET_ADDRSTRLEN);
    fprintf(stderr,"ERROR: bind() on %s:%d, %s\n", interface_address, servAddr.sin_port, strerror(errno));
    exit(EXIT_FAILURE);
  }

  if(mcast_grp != NULL)
    {
    fprintf(stderr,"Joining multicast group %s\n",mcast_grp);
    // Allow multicast:
    if (setsockopt(s, IPPROTO_IP, IP_MULTICAST_LOOP, &z, sizeof(z)) < 0)
      {
      perror("setsockopt:IP_MULTICAST_LOOP");
      exit(EXIT_FAILURE);
      }
    // Join the multicast group:
    command.imr_multiaddr.s_addr = inet_addr(mcast_grp);
    command.imr_interface.s_addr = interface;
    if(command.imr_multiaddr.s_addr == INADDR_NONE)
      {
      fprintf(stderr,"ERROR: %s is not a multicast address.\n",mcast_grp);
      exit(EXIT_FAILURE);
      }
    if(setsockopt(s, IPPROTO_IP, IP_ADD_MEMBERSHIP, &command, sizeof(command)) < 0)
      {
      perror("setsockopt:IP_ADD_MEMBERSHIP");
      exit(EXIT_FAILURE);
      }
    }

  // Server loop:
  fprintf(stderr,"Listening on %s:%d...\n", interface_address, udp_port);
  i = 0;
  while(1)    {
    len = sizeof(cliAddr);
    n = recvfrom(s,buffer,BUFSIZE,0,(struct sockaddr *) &cliAddr, (socklen_t*)&len);
    if (n < 0)
      {
      fprintf(stderr,"Can't receive data...\n");
      continue;
      }
    if( (n != 8288) && (n != 96) )
      {
      fprintf(stderr,"ERROR: unsupported packet size %d \n",n);
      // continue;
      }

    (*handle_packet)(buffer);

    if(handle_packet==handle_packet_dummy)
      {
      if((i & 0x000000000000FFFF)==0) // Sometimes.
        {
        fprintf(stderr,"%10lu: %d bytes from %s.%u\n",
            i,n,inet_ntoa(cliAddr.sin_addr),ntohs(cliAddr.sin_port));
        }
      }
    i++;
//    if(i==4)
    if(i==37)
//    if(i==100)
      {
      if(handle_packet==handle_packet_numbers) break;
      }
    }
  return(0);
  }


int get_payloadlength(char *buf)
  {
  int len;
  uint64_t val;
  uint64_t *qword;
  qword = ((uint64_t*)buf) + 4;
  val = be64toh(*qword);
  val &= 0x0000FFFFFFFFFFFF;
  len = (int)val;
  if( (len!=8192) && (len!=0) )
    {
    fprintf(stderr,"ERROR: unsupported payload length %d\n",len);
    //return(0);
    }
  return(len);
  }


uint64_t get_heapid(char *buf)
  {
  int packet_number_in_heap;
  uint64_t val, heapoffset, timestamp, channelid, elementid;
  // Fetch values:
  uint64_t *qword = ((uint64_t*)buf);
  val = be64toh(*(qword+3));
  heapoffset = val & 0x0000FFFFFFFFFFFF;
  val = be64toh(*(qword+5));
  timestamp = val & 0x0000FFFFFFFFFFFF;
  val = be64toh(*(qword+6));
  channelid = (val & 0x00000000FFFF0000) >> 16;
  elementid = (val & 0x000000000000FFFF);
  // Modify and assemble values:
  packet_number_in_heap = heapoffset / 8192;
  timestamp = timestamp >> 12;
  timestamp = timestamp - packet_number_in_heap;
  channelid = channelid << 30;
  elementid = elementid << 40;
  val =       (elementid & 0x0000FF0000000000);
  val = val | (channelid & 0x000000FFC0000000);
  val = val | (timestamp & 0x000000003FFFFFFF);
  return(val);
  }


uint64_t get_timestamp(char *buf)
  {
  uint64_t val;
  uint64_t *qword = ((uint64_t*)buf);
  val = be64toh(*(qword+5));
  val = val & 0x0000FFFFFFFFFFFF;
  return(val);
  }


uint16_t get_channelid(char *buf)
  {
  uint64_t val;
  uint64_t *qword = ((uint64_t*)buf);
  val = be64toh(*(qword+6));
  val = (val & 0x00000000FFFF0000) >> 16;
  return((uint16_t)val);
  }


uint16_t get_elementid(char *buf)
  {
  uint64_t val;
  uint64_t *qword = ((uint64_t*)buf);
  val = be64toh(*(qword+6));
  val = val & 0x000000000000FFFF;
  return((uint16_t)val);
  }


uint16_t bitreverse10(uint16_t ival)
  {
  uint16_t oval = 0;
  oval |= (ival & 0x0001) << 9;
  oval |= (ival & 0x0002) << 7;
  oval |= (ival & 0x0004) << 5;
  oval |= (ival & 0x0008) << 3;
  oval |= (ival & 0x0010) << 1;
  oval |= (ival & 0x0020) >> 1;
  oval |= (ival & 0x0040) >> 3;
  oval |= (ival & 0x0080) >> 5;
  oval |= (ival & 0x0100) >> 7;
  oval |= (ival & 0x0200) >> 9;
  return(oval);
  }