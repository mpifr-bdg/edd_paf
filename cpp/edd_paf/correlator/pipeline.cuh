#pragma once

#include <complex>
#include <cuda.h>

#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/raw_bytes.hpp>
#include <psrdada_cpp/double_device_buffer.cuh>
#include <psrdada_cpp/double_host_buffer.cuh>

#include <cudawrappers/cu.hpp>
#include <libtcc/Correlator.h>

#include <edd_paf/cuda_utils.cuh>

namespace psr = psrdada_cpp;

namespace edd_paf{
namespace correlator{

struct pipe_config{
    key_t in_key;
    key_t out_key;
    std::size_t device_id;
    std::size_t n_sample;
    std::size_t n_channel;
    std::size_t n_element;
    std::size_t n_pol;
    std::size_t n_acc;
    std::size_t n_bit;

    std::size_t baselines()
    {
        return (n_element * (n_element + 1)) / 2;
    }
    std::size_t coarse_time()
    {
        if(n_sample / n_acc < 1)
            return 1;
        return n_sample / n_acc;
    }
    std::size_t output_items()
    {
        return n_channel * this->baselines() * n_pol * n_pol;
    }
    std::size_t input_items()
    {
        return n_channel * n_element * n_sample * n_pol;
    }
    std::size_t input_size()
    {
        return this->input_items() * n_bit * 2 / 8;
    }
    std::size_t output_size()
    {
        return this->output_items() * sizeof(int32_t) * 2;
    }
    void print()
    {
        std::cout << "device: " << device_id << "\n"
            << "n samples: " << n_sample << "\n"
            << "n channel: " << n_channel << "\n"
            << "n element: " << n_element << "\n"
            << "n pol:     " << n_pol << "\n"
            << "n acc:     " << n_acc << "\n"
            << "n bit:     " << n_bit << std::endl;
    }
};


template<typename HandlerType, typename InputType, typename OutputType>
class Pipeline
{
private:
    typedef psr::DoubleBuffer<thrust::device_vector<InputType>> InputVector;    // Type for unpacked raw input data (voltage)
    typedef psr::DoubleBuffer<thrust::device_vector<OutputType>> OutputVector;// Type for beamfored output data
    typedef psr::DoublePinnedHostBuffer<OutputType> OutputHostVector;

public:
    Pipeline(pipe_config& conf, HandlerType& handler, cu::Device& device);

    ~Pipeline();

    /**
     * @brief      Initialise the pipeline with a DADA header block
     *
     * @param      block  A RawBytes object wrapping the DADA header block
    */
	void init(psr::RawBytes &block);


    /**
     * @brief      Process the data in a DADA data buffer
     *
     * @param      block  A RawBytes object wrapping the DADA data block
    */
	bool operator()(psr::RawBytes &block);

private:
    std::size_t _call_count;
    pipe_config _conf;
    HandlerType &_handler;

    // Data arrays / buffers
    InputVector input_buffer;
    OutputVector output_buffer;
    OutputHostVector host_output_buffer;
    // Processing objects
    std::unique_ptr<tcc::Correlator> processor;
    cu::Device _device;
    // CUDA streams
    cudaStream_t host2dev_stream;
    cudaStream_t processing_stream;
    cudaStream_t dev2host_stream;
};

}
}

#include "edd_paf/correlator/src/pipeline.cu"
