#pragma once

namespace edd_paf{
namespace correlator{

template<typename HandlerType, typename InputType, typename OutputType>
Pipeline<HandlerType, InputType, OutputType>::Pipeline(pipe_config& conf, HandlerType& handler, cu::Device& device)
    : _call_count(0), _conf(conf), _handler(handler), _device(device)
{
    processor.reset(new tcc::Correlator(_device,
        _conf.n_bit,
        _conf.n_element,
        _conf.n_channel,
        _conf.n_sample,
        _conf.n_pol, 0));
    std::cout << "Created correlator instance" << std::endl;

    input_buffer.resize(_conf.input_items(), {0x00, 0x00});
    output_buffer.resize(_conf.output_items(), {0,0});
    host_output_buffer.resize(_conf.output_items(), {0,0});

    CUDA_ERROR_CHECK(cudaStreamCreate(&host2dev_stream));
    CUDA_ERROR_CHECK(cudaStreamCreate(&processing_stream));
    CUDA_ERROR_CHECK(cudaStreamCreate(&dev2host_stream));
}

template<typename HandlerType, typename InputType, typename OutputType>
Pipeline<HandlerType, InputType, OutputType>::~Pipeline()
{
    CUDA_ERROR_CHECK(cudaDeviceSynchronize());
    CUDA_ERROR_CHECK(cudaStreamDestroy(host2dev_stream));
    CUDA_ERROR_CHECK(cudaStreamDestroy(processing_stream));
    CUDA_ERROR_CHECK(cudaStreamDestroy(dev2host_stream));
    std::cout << "Destructing Pipeline" << std::endl;
}

template<typename HandlerType, typename InputType, typename OutputType>
void Pipeline<HandlerType, InputType, OutputType>::init(psr::RawBytes &block)
{
    BOOST_LOG_TRIVIAL(debug) << "Pipeline initialising ";
    _handler.init(block);
}


template<typename HandlerType, typename InputType, typename OutputType>
bool Pipeline<HandlerType, InputType, OutputType>::operator()(psr::RawBytes &block)
{
    ++_call_count;
    BOOST_LOG_TRIVIAL(debug) << "Pipeline operator() called " << _call_count;
    /* Unexpected buffer size */
    if (block.used_bytes() != _conf.input_size())
    {
        BOOST_LOG_TRIVIAL(error)
            << "Unexpected Buffer Size - Got "
            << block.used_bytes() << " byte, expected "
            << _conf.input_size() << " byte)";
        CUDA_ERROR_CHECK(cudaDeviceSynchronize());
        return true;
    }
    BOOST_LOG_TRIVIAL(debug)
        << "Processing " << block.used_bytes() << "/"
        << input_buffer.size()*_conf.n_bit*2/8 << " bytes \n";

    CUDA_ERROR_CHECK(cudaStreamSynchronize(host2dev_stream));

    input_buffer.swap();
    // Copy new data from host to device
    CUDA_ERROR_CHECK(cudaMemcpyAsync(
        static_cast<void *>(input_buffer.a_ptr()),
        static_cast<void *>(block.ptr()),
        _conf.input_size(), cudaMemcpyHostToDevice, host2dev_stream));

    if (_call_count == 1) {return false;}

    CUDA_ERROR_CHECK(cudaStreamSynchronize(processing_stream));
    output_buffer.swap();
    processor->launchAsync(processing_stream,
        reinterpret_cast<CUdeviceptr>(output_buffer.a_ptr()),
        reinterpret_cast<CUdeviceptr>(input_buffer.b_ptr()));

    if (_call_count == 2) {return false;}

    CUDA_ERROR_CHECK(cudaStreamSynchronize(dev2host_stream));
    host_output_buffer.swap();

    CUDA_ERROR_CHECK(cudaMemcpyAsync(
        static_cast<void*>(host_output_buffer.a_ptr()),
        static_cast<void*>(output_buffer.b_ptr()),
        _conf.output_size(),
        cudaMemcpyDeviceToHost, dev2host_stream));

    if (_call_count == 3) {return false;}

    psr::RawBytes bytes(
        reinterpret_cast<char *>(host_output_buffer.b_ptr()),
        _conf.output_size(), _conf.output_size());

    _handler(bytes);
    return false;

}

}
}