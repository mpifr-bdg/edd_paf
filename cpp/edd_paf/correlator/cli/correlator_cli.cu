#include <cuda.h>
#include <complex>

#include <boost/program_options.hpp>


#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/cli_utils.hpp>
#include <psrdada_cpp/dada_input_stream.hpp>
#include <psrdada_cpp/dada_output_stream.hpp>
#include <psrdada_cpp/multilog.hpp>


#include "edd_paf/correlator/pipeline.cuh"


const size_t SUCCESS = 0;
const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

namespace psr = psrdada_cpp;

namespace cx  = edd_paf::correlator;

template<typename InputType, typename OutputType>
void launch(cx::pipe_config& conf)
{
    cu::init();
    cu::Device device(conf.device_id);
    cu::Context context(0, device);
    context.setCurrent();
    psr::MultiLog log("edd_paf::correlator");
    psr::DadaOutputStream ostream(conf.out_key, log);
    cx::Pipeline<decltype(ostream), InputType, OutputType> pipeline(conf, ostream, device);
    psr::DadaInputStream<decltype(pipeline)> istream(conf.in_key, log, pipeline);
    istream.start();
}

int main(int argc, char** argv)
{
    try
    {
        // Variables to store command line options
        cx::pipe_config conf;
        // Parse command line
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
        ("help,h", "Print help messages")
        ("in_key", po::value<std::string>()->required()
          ->notifier([&conf](std::string key){conf.in_key = psr::string_to_key(key);}), "Input dada key")
        ("out_key", po::value<std::string>()->required()
          ->notifier([&conf](std::string key){conf.out_key = psr::string_to_key(key);}), "Output dada key")
        ("sample", po::value<std::size_t>(&conf.n_sample)->default_value(262144), "Number of samples")
        ("channel", po::value<std::size_t>(&conf.n_channel)->default_value(16), "Number of channels")
        ("element", po::value<std::size_t>(&conf.n_element)->default_value(32), "Number of elments")
        ("npol", po::value<std::size_t>(&conf.n_pol)->default_value(2), "Number of polarizations")
        ("nacc", po::value<std::size_t>(&conf.n_acc)->default_value(1), "Integration interval")
        ("nbit", po::value<std::size_t>(&conf.n_bit)->default_value(8), "Bit depth of input data")
        ("device", po::value<std::size_t>(&conf.device_id)->default_value(0), "The GPU to use");

        po::variables_map vm;
        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if ( vm.count("help")  )
            {
                std::cout << "edd_paf -- The edd_paf Correlator" << std::endl
                << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        }
        catch(po::error& e)
        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }
        launch<std::complex<int8_t>, std::complex<int32_t>>(conf);
    }
    catch(std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
            << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;
}
