#include <benchmark/benchmark.h>
#include <cuda.h>
#include <psrdada_cpp/cli_utils.hpp>
#include <psrdada_cpp/testing_tools/streams.hpp>
#include <psrdada_cpp/raw_bytes.hpp>

int main(int argc, char** argv) {
    char arg0_default[] = "benchmark";
    char* args_default = arg0_default;
    psrdada_cpp::set_log_level("INFO");
    if (!argv) {
      argc = 1;
      argv = &args_default;
    }
    ::benchmark::Initialize(&argc, argv);
    if (::benchmark::ReportUnrecognizedArguments(argc, argv)) return 1;
    ::benchmark::RunSpecifiedBenchmarks();
    ::benchmark::Shutdown();
    return 0;
}