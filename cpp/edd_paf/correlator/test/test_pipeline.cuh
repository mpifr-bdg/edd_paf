#pragma once

#include <gtest/gtest.h>
#include <thrust/host_vector.h>

#include <psrdada_cpp/testing_tools/streams.hpp>
#include <psrdada_cpp/testing_tools/tools.cuh>

#include "edd_paf/correlator/pipeline.cuh"

namespace psr_testing = psrdada_cpp::testing_tools;

namespace edd_paf {
namespace correlator {
namespace test {

const std::size_t header_size = 4096;

class PipelineStreamTester : public ::testing::TestWithParam<pipe_config>
{
protected:
  void SetUp(){};
  void TearDown(){
    std::cout << "TearDown PipelineStreamTester" << std::endl;
  };
public:
    PipelineStreamTester() : ::testing::TestWithParam<pipe_config>(),
    _config(GetParam()){
        _config.print();
    };

    template<typename InputType, typename OutputType>
    void test(std::size_t nbuffers=4)
    {
        // Nvidia Driver Context
        cu::init();
        cu::Device device(_config.device_id);
        cu::Context context(0, device);
        context.setCurrent();
        // Construct objects for the test
        psr_testing::DummyStream<char> consumer;
        Pipeline<psr_testing::DummyStream<char>, InputType, OutputType> pipeline(_config, consumer, device);
        char header[header_size] = {0};
        psrdada_cpp::RawBytes hdr(header, header_size, header_size);
        pipeline.init(hdr);

        // Allocate memory
        thrust::host_vector<InputType> idata = psr_testing::random_vector<InputType>(0, 64, _config.input_items() * nbuffers);
        thrust::host_vector<OutputType> odata_gpu(_config.output_items() * nbuffers, {0,0}); // Needs 3 calls to pipeline until the first output
        thrust::host_vector<OutputType> odata_ref(_config.output_items() * nbuffers, {0,0});

        // Run both - reference and GPU implemenetation
        std::cout << "Computing CPU results.. " << std::flush;
        this->reference(idata, odata_ref);
        std::cout << "done!" << std::endl;

        for(std::size_t buf = 0; buf < nbuffers; buf++)
        {
            // The whole array is splitted into nbuffers chunk and passed to the pipeline as raw bytes
            psrdada_cpp::RawBytes raw_data(
                reinterpret_cast<char *>(idata.data() + buf * _config.input_items()),
                _config.input_size(), _config.input_size());
            ASSERT_FALSE(pipeline(raw_data));
            // If the consumer was called, processed data is available and copied.
            if(consumer.call_count() > 0)
            {
                std::memcpy(
                    static_cast<void*>(&odata_gpu[(consumer.call_count()-1) * _config.output_items()]),
                    static_cast<void*>(consumer.data_ptr()), consumer.data_size());
            }
        }

        std::cout << "Comparing GPU and CPU results" << std::endl;
        // Compare reference against pipeline data
        ASSERT_TRUE(odata_gpu.size() == odata_ref.size());
        for(std::size_t i = 0; i < consumer.call_count() * _config.output_items(); i++)
        {
            // std::cout << "reference[" << i << "]=\t" << odata_ref[i].real() << " +i* " << odata_ref[i].imag() << "\t\tpipeline[" << i << "]="
            //     << odata_gpu[i].real() << " +i* " << odata_gpu[i].imag() <<  std::endl;
            ASSERT_EQ(odata_ref[i].real(), odata_gpu[i].real())
                << "reference[" << i << "]=" << odata_ref[i].real() << " pipeline[" << i << "]="
                << odata_gpu[i].real() << " @ position " << i;
            ASSERT_EQ(odata_ref[i].imag(), odata_gpu[i].imag())
                << "reference[" << i << "]=" << odata_ref[i].imag() << " pipeline[" << i << "]="
                << odata_gpu[i].imag() << " @ position " << i;
        }
        std::cout << "Test done!" << std::endl;
    }

private:
    template<typename InputType, typename OutputType>
    void reference(const thrust::host_vector<InputType>& idata, thrust::host_vector<OutputType>& odata)
    {

        std::size_t oidx, iidxY, iidxX, ioffset, ooffset;
        std::size_t n_buf = idata.size() / _config.input_items();
        std::size_t n_pol = _config.n_pol;
        std::size_t coarse_time = _config.coarse_time();
        std::size_t n_baseline = _config.baselines();
        for(std::size_t buf = 0; buf < n_buf; buf++)
            for(std::size_t ch = 0; ch < _config.n_channel; ch++)
                for (std::size_t t = 0; t < coarse_time; t ++){
                    ioffset = buf * _config.input_items()
                        + ch * coarse_time * _config.n_element * n_pol * _config.n_acc
                        + t * _config.n_element * n_pol * _config.n_acc;

                    ooffset = buf * _config.output_items()
                        + ch * n_baseline * n_pol * n_pol;

                    for (std::size_t recvX = 0, baseline = 0; recvX < _config.n_element; recvX ++)
                        for (std::size_t recvY = 0; recvY <= recvX; recvY ++, baseline++)
                            for (std::size_t time = 0; time < _config.n_acc; time ++)
                                for (std::size_t polY = 0; polY < n_pol; polY ++)
                                    for (std::size_t polX = 0; polX < n_pol; polX ++) {
                                        iidxY = ioffset + recvY * n_pol * _config.n_acc
                                            + polY * _config.n_acc + time;
                                        iidxX = ioffset + recvX * n_pol * _config.n_acc
                                            + polX * _config.n_acc + time;
                                        oidx = ooffset + baseline * n_pol * n_pol
                                            + polY * n_pol + polX;
                                        if(oidx < odata.size())
                                        {
                                            odata[oidx] += static_cast<OutputType>(idata[iidxY]) * std::conj(static_cast<OutputType>(idata[iidxX]));
                                        }
                                    }
                }
    }

private:
    pipe_config _config;
};


TEST_P(PipelineStreamTester, test_streaming_float)
{
    if(!sufficientComputeCap(7, 5)){return;}
    std::cout << std::endl
        << "-------------------------------------------------\n"
        << " Testing Correlator Pipeline with complex int8_t \n"
        << "-------------------------------------------------" << std::endl;
    test<std::complex<int8_t>, std::complex<int32_t>>();
}

#include <psrdada_cpp/cli_utils.hpp>
namespace psr = psrdada_cpp;
const key_t ikey = psr::string_to_key(std::string("dada"));
const key_t okey = psr::string_to_key(std::string("dadc"));

INSTANTIATE_TEST_SUITE_P(PipelineStreamTesterInstantiation, PipelineStreamTester, ::testing::Values(
    //          in_key | out_key | device_id | n_sample | n_channel | n_element | n_pol | n_acc | n_bit
    pipe_config{ikey,       okey,   0,          32,       1,         64,          2,        16,        8 },
    pipe_config{ikey,       okey,   0,          16,       1,         64,          2,        16,        8 },
    pipe_config{ikey,       okey,   0,          16,       1,         32,          2,        16,        8 },
    pipe_config{ikey,       okey,   0,          32,       1,         16,          2,        16,        8 },
    pipe_config{ikey,       okey,   0,          16,       1,         64,          2,        16,        8 },
    pipe_config{ikey,       okey,   0,          16,       2,         32,          2,        16,        8 },
    pipe_config{ikey,       okey,   0,          16,       4,         16,         2,        16,        8 }
));

}
}
}