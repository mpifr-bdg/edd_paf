#ifndef EDD_PAF_MEMORY_CU
#define EDD_PAF_MEMORY_CU

namespace edd_paf{

namespace bip = boost::interprocess;

template<class T>
InterprocessMemory<T>::InterprocessMemory(std::size_t size, std::string smem_key, bool force_creation)
{
    // Prepare IPC POSSIX shared memory
    _smem_name = smem_key;
    _bytes = size * sizeof(T);
    // Try to open first
    try
    {
        if(force_creation)
        {
            BOOST_LOG_TRIVIAL(info) << "Force creation of shared memory key=" << _smem_name;
            bip::shared_memory_object::remove(_smem_name.c_str());
            _smem = bip::shared_memory_object(bip::create_only, _smem_name.c_str(), bip::read_write);
            _smem.truncate(sizeof(QueueHeader) + _bytes);
            _owner = true;
        }
        else
        {
            _smem = bip::shared_memory_object(bip::open_only, _smem_name.c_str(), bip::read_write);
            BOOST_LOG_TRIVIAL(info) << "Opened shared memory with key=" << _smem_name;
        }
    }
    catch(const bip::interprocess_exception &ex)
    {
        // Catch if the the shared memory does not exists and create it.
        if(ex.get_error_code() == bip::not_found_error)
        {
            try
            {
                BOOST_LOG_TRIVIAL(info) << "Shared memory with key=" << _smem_name << "does not exists, creating it";
                _smem = bip::shared_memory_object(bip::create_only, _smem_name.c_str(), bip::read_write);
                _smem.truncate(sizeof(QueueHeader) + _bytes);
                _owner = true;
            }
            catch(const bip::interprocess_exception &ex)
            {
                std::cerr << "Failed to create shared memory: " << ex.what();
                throw std::runtime_error("Failed to create shared memory");
            }
        }
        else
        {
            std::cerr << "Failed to open shared memory: " << ex.what();
            throw std::runtime_error("Failed to open shared memory");
        }
    }
    try
    {
        region = bip::mapped_region(_smem, bip::read_write);
        smem_addr = region.get_address();
    }
    catch(const bip::interprocess_exception &ex)
    {
        std::cerr << "Failed to create mapped memory region: " << ex.what();
        throw std::runtime_error("Failed to create mapped memory region");
    }
    _data.resize(size);
    qheader = new (smem_addr) QueueHeader();
    _smem_data = &(static_cast<T*>(smem_addr)[sizeof(QueueHeader)]);
}

template<class T>
InterprocessMemory<T>::~InterprocessMemory()
{
    stop();
    if(_owner)
    {
        bip::shared_memory_object::remove(_smem_name.c_str());
    }
}

template<class T>
void InterprocessMemory<T>::stop()
{
    if(_active){
        // bip::scoped_lock<bip::interprocess_mutex> lock(qheader->mutex);
        qheader->stop = true;
        qheader->ready_to_write.notify_all();
        qheader->ready_to_read.notify_all();
        _thread->join();
        BOOST_LOG_TRIVIAL(info) << "Stopped InterprocessMemory::thread";
    }
    _update_cnt = 0;
}

template<class T>
void InterprocessMemory<T>::writing()
{
    if(_active){
        throw std::runtime_error("Tried to activate InterprocessMemory multiple times");
    }
    _thread = new std::thread(&InterprocessMemory<T>::run_writing, this);
    _active = true;
}

template<class T>
void InterprocessMemory<T>::reading()
{
    if(_active){
        throw std::runtime_error("Tried to activate InterprocessMemory multiple times");
    }
    _thread = new std::thread(&InterprocessMemory<T>::run_reading, this);
    _active = true;
}

template<class T>
void InterprocessMemory<T>::run_writing()
{
    while(!qheader->stop)
    {
        if(_updated)
        {
            bip::scoped_lock<bip::interprocess_mutex> lock(qheader->mutex);
            if(qheader->data_in)
            {
                BOOST_LOG_TRIVIAL(debug) << "Waiting for data readout of interprocess memory";
                qheader->ready_to_write.wait(lock);
            }
            BOOST_LOG_TRIVIAL(info) << "Writing new data to interprocess memory ";
            memcpy(_smem_data, (void*)_data.data(), _bytes);
            qheader->data_in = true;
            qheader->ready_to_read.notify_all();
            _update_cnt++;
            _updated = false;
        }
        usleep(1000);
    }
    _active = false;
}

template<class T>
void InterprocessMemory<T>::run_reading()
{
    while(!qheader->stop)
    {
        bip::scoped_lock<bip::interprocess_mutex> lock(qheader->mutex);
        if(!qheader->data_in)
        {
            BOOST_LOG_TRIVIAL(debug) << "Waiting for new data";
            qheader->ready_to_read.wait(lock); // Wait for read out
        }
        memcpy((void*)_data.data(), _smem_data, _bytes);
        //Notify the other process that the buffer is empty
        _update_cnt++;
        qheader->data_in = false;
        qheader->ready_to_write.notify_all();
    }
    _active = false;
}


template<class T>
void InterprocessMemory<T>::set_data(std::vector<T> vec)
{
    _data = vec;
    _updated = true;
}


template<class T>
SharedPipelineBuffer<T>::SharedPipelineBuffer(std::size_t size, std::string smem_name)
    : psrdada_cpp::DoubleBuffer<thrust::device_vector<T>>()
{
    this->resize(size);
    _bytes = size * sizeof(T);
    _reader = new InterprocessMemory<T>(size, smem_name);
}

template<class T>
SharedPipelineBuffer<T>::~SharedPipelineBuffer()
{
    stop();
}

template<class T>
void SharedPipelineBuffer<T>::start()
{
    _handle_thread = new std::thread(&SharedPipelineBuffer<T>::run, this);
    _active = true;
}


template<class T>
void SharedPipelineBuffer<T>::run()
{
    _update_cnt = 0;
    _reader->reading();
    while(!_reader->is_active()){usleep(1000);}
    BOOST_LOG_TRIVIAL(info) << "Reader is active..";
    while(_reader->is_active())
    {
        if(_update_cnt >= _reader->updates())
        {
            usleep(1000);
        }
        else
        {
            CUDA_ERROR_CHECK(cudaMemcpy((void*)this->b_ptr(), (void*)_reader->get_data().data(),
                _bytes, cudaMemcpyHostToDevice));
            this->swap();
            BOOST_LOG_TRIVIAL(info) << "Copied new data from interprocess memory to GPU";
            _update_cnt = _reader->updates();
        }
    }
}

template<class T>
void SharedPipelineBuffer<T>::stop()
{
    BOOST_LOG_TRIVIAL(info) << "Stopping SharedPipelineBuffer";
    _reader->stop();
    if(_handle_thread->joinable())
    {
        _handle_thread->join();
    }
    _active = false;
    BOOST_LOG_TRIVIAL(info) << "Stopped SharedPipelineBuffer";
}

} // namespace edd_paf

#endif // EDD_PAF_MEMORY_CU