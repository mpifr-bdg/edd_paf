#pragma once
#include <psrdada_cpp/common.hpp>
#include "edd_paf/beamformer/beamformer.cuh"

namespace edd_paf{
namespace beamformer{

struct PipelineConfig{
    // Total number of samples in the passed buffer
    std::size_t n_samples = 32768;
    // Number of channels in the buffer
    std::size_t n_channel = 24;
    // Number of dual pol elements / antenna
    std::size_t n_elements = 128;
    // Number of output beams
    std::size_t n_beam = 128;
    // The number of integrations of the full Stokes high resolution spectra
    std::size_t integration = 64;
    // The number of fft bin of the synthesis filter (Only 32 supported)
    std::size_t nfft = 32;
    // The batch size of the fast time dimension
    std::size_t batch_size = 4096;
    // The batch size of the output voltage beams
    std::size_t ovol_batch_size = 4096;
    // Oversample numerator -> os ratio is os_numerator/os_denominator
    std::size_t os_numerator = 32;
    // Oversample denominator -> os ratio is os_numerator/os_denominator
    std::size_t os_denominator = 27;
    // Number of polarisations
    const std::size_t n_pol = 2;

    /**
     * @brief prints the configuration parameters
     *
     */
    void print()
    {
        BOOST_LOG_TRIVIAL(debug) << "Pipeline configuration";
        BOOST_LOG_TRIVIAL(debug) << "n_samples: " << n_samples;
        BOOST_LOG_TRIVIAL(debug) << "n_channel: " << n_channel;
        BOOST_LOG_TRIVIAL(debug) << "n_elements: " << n_elements;
        BOOST_LOG_TRIVIAL(debug) << "n_pol: " << n_pol;
        BOOST_LOG_TRIVIAL(debug) << "n_beam: " << n_beam;
        BOOST_LOG_TRIVIAL(debug) << "integration: " << integration;
        BOOST_LOG_TRIVIAL(debug) << "nfft: " << nfft;
        BOOST_LOG_TRIVIAL(debug) << "batch_size: " << batch_size;
    }

    /**
     * @brief returns the size of the input buffer in items
     *
     * @return std::size_t
     */
    std::size_t input_size()
    {
        return n_samples * n_channel * n_elements * n_pol;
    }

    /**
     * @brief returns the size of the voltage beam output buffer in items
     *
     * @return std::size_t
     */
    std::size_t output_raw_voltage_size()
    {
        return raw_beams() * n_channel * n_samples * n_pol;
    }

    /**
     * @brief returns the size of the stokes I high temporal resolution buffer
     *
     * @return std::size_t
     */
    std::size_t output_stokes_i_size()
    {
        return n_beam * n_channel * n_samples / nfft;
    }

    /**
     * @brief returns the size of the full Stokes high frequency resolution buffer
     *
     * @return std::size_t
     */
    std::size_t output_full_stokes_size()
    {
        // return n_beam * n_channel * n_samples * (1 - ((float)1/os_numerator) * (os_numerator - os_denominator)) / integration;
        return n_beam * n_channel * full_stokes_samples();
    }

    /**
     * @brief returns the size of the full Stokes high frequency resolution buffer
     *
     * @return std::size_t
     */
    std::size_t full_stokes_samples()
    {
        // return n_samples * (1 - ((float)1/os_numerator) * (os_numerator - os_denominator)) / integration;
        return spectrum_size() * nspectra();
    }

    /**
     * @brief returns the size of the weight buffer
     *
     * @return std::size_t
     */
    std::size_t weight_size()
    {
        return n_beam * n_channel * n_elements * n_pol;
    }

    /**
     * @brief returns the the right edge of the FFT
     *
     * @return std::size_t
     */
    std::size_t right()
    {
        return nfft - (os_numerator - os_denominator) / 2;
    }

    /**
     * @brief eturns the the left edge of the FFT
     *
     * @return std::size_t
     */
    std::size_t left()
    {
        return (os_numerator - os_denominator) / 2;
    }

    /**
     * @brief returns the number of batches in the input buffer
     *
     * @return std::size_t
     */
    std::size_t nbatch()
    {
        return n_samples / batch_size;
    }

    /**
     * @brief returns the the number raw voltage beams
     *
     * @return std::size_t
     */
    std::size_t raw_beams()
    {
        std::size_t rbeam;
        (n_beam > N_MAX_RBEAMS) ? rbeam = N_MAX_RBEAMS: rbeam = n_beam;
        return rbeam;
    }

    /**
     * @brief returns the number of channels in a single full Stokes spectrum
     *
     * @return std::size_t
     */
    std::size_t spectrum_size()
    {
        return nfft / os_numerator * os_denominator;
    }

    /**
     * @brief returns the number of spectra within the full Stokes buffer
     *
     * @return std::size_t
     */
    std::size_t nspectra()
    {
        return n_samples / (nfft * integration);
    }

    /**
     * @brief returns the oversample ratio
     *
     * @return std::size_t
     */
    std::size_t os_ratio()
    {
        return os_numerator / os_denominator;
    }

    /**
     * @brief Validates the configuration parameters
     *
     * @return true valid configuration
     * @return false invalid configuration
     */
    bool validate()
    {
        if(n_samples % SAMPLES_ITER)
            BOOST_LOG_TRIVIAL(error) << "ERROR: Number of samples must be multiple of 128"; return CONFIG_ERROR;
        if(integration % INTEGRATE_ITER)
            BOOST_LOG_TRIVIAL(error) << "ERROR: Integration value must be multiple of 4"; return CONFIG_ERROR;
        if(nfft != FFT32_POINTS)// && _nfft != FFT128_POINTS)
            BOOST_LOG_TRIVIAL(error) << "ERROR: Only 32- and 128-point FFTs are implemented"; return CONFIG_ERROR;
        if(integration * nfft > n_samples)
            BOOST_LOG_TRIVIAL(error) << "ERROR: Not enough samples ("<< n_samples << ") in batch to integrate "<< integration<< "x"<< nfft << " spectra."; return CONFIG_ERROR;
        // if(_nfft != FFT32_POINTS || _nfft != FFT128_POINTS){
        //     BOOST_LOG_TRIVIAL(error) << "ERROR: Only 32- and 128-point FFTs are implemented"; return CONFIG_ERROR;}
        return 0;
    }
};

}
}
