#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/complex.h>
#include <cuda.h>
#include <random>
#include <cmath>
#include <fstream>
#include <chrono>
#include <unordered_map>

#include <boost/program_options.hpp>

#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/cli_utils.hpp>
#include <psrdada_cpp/dada_input_stream.hpp>
#include <psrdada_cpp/dada_output_stream.hpp>
#include <psrdada_cpp/multilog.hpp>

#include "edd_paf/cuda_utils.cuh"
#include "edd_paf/beamformer/config.hpp"
#include "edd_paf/beamformer/pipeline.cuh"


const size_t SUCCESS = 0;
const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

using namespace psrdada_cpp;
namespace bf = edd_paf::beamformer;

template<typename T>
void launch(bf::PipelineConfig& conf, key_t in_key, key_t o_vol_key, key_t o_sti_key, key_t o_stf_key)
{
    MultiLog log("beamformer_cli");
    DadaOutputStream ovol(o_vol_key, log);
    DadaOutputStream osti(o_sti_key, log);
    DadaOutputStream ostf(o_stf_key, log);
    bf::Pipeline<DadaOutputStream, T> pipeline(conf, ovol, osti, ostf);
    DadaInputStream<decltype(pipeline)> input(in_key, log, pipeline);
    input.start();
}

int main(int argc, char** argv)
{
    try
    {
        // Variables to store command line options
        bf::PipelineConfig conf;
        int device_id;
        key_t in_key, o_vol_key, o_sti_key, o_stf_key;

        // Parse command line
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
        ("help,h", "Print help messages")
        ("in_key", po::value<std::string>()->required()
          ->notifier([&in_key](std::string key){in_key = string_to_key(key);}), "Input dada key")
        ("out_vol_key", po::value<std::string>()->required()
          ->notifier([&o_vol_key](std::string key){o_vol_key = string_to_key(key);}), "Output dada key for channelized voltage beams")
        ("out_sti_key", po::value<std::string>()->required()
          ->notifier([&o_sti_key](std::string key){o_sti_key = string_to_key(key);}), "Output dada key for high time resoltion stokes I beams")
        ("out_stf_key", po::value<std::string>()->required()
          ->notifier([&o_stf_key](std::string key){o_stf_key = string_to_key(key);}), "Output dada key for high frequency resoltion full stokes beams")
        ("ibatch", po::value<std::size_t>(&conf.batch_size)->default_value(4096), "Number of samples in an input batch (heap) - Set to 0 for unbatched data")
        ("obatch", po::value<std::size_t>(&conf.ovol_batch_size)->default_value(4096), "Number of samples in an output batch (heap) - Channelized voltage beam")
        ("samples", po::value<std::size_t>(&conf.n_samples)->default_value(262144), "Number of samples within one dada block")
        ("channels", po::value<std::size_t>(&conf.n_channel)->default_value(7), "Number of channels")
        ("elements", po::value<std::size_t>(&conf.n_elements)->default_value(36), "Number of elments")
        ("beams", po::value<std::size_t>(&conf.n_beam)->default_value(36), "Number of beams")
        ("num", po::value<std::size_t>(&conf.os_numerator)->default_value(32), "Oversample numerator. If data is not oversample, both num and dnom should be 1")
        ("dnom", po::value<std::size_t>(&conf.os_denominator)->default_value(27), "Oversample denominator. If data is not oversample, both num and dnom should be 1")
        ("nfft", po::value<std::size_t>(&conf.nfft)->default_value(32), "Number FFT size - currently only 32 supported")
        ("integration", po::value<std::size_t>(&conf.integration)->default_value(1), "Integration interval; must be multiple 2^n and smaller 32")
        ("device", po::value<int>(&device_id)->default_value(0), "ID of GPU device");

        po::variables_map vm;
        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if ( vm.count("help")  )
            {
                std::cout << "edd_paf -- The edd_paf Controller implementations" << std::endl
                << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        }
        catch(po::error& e)
        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }
        // edd_paf::CUDA_ERROR_CHECK(cudaSetDevice(device_id));
        launch<float2>(conf, in_key, o_vol_key, o_sti_key, o_stf_key);
    }
    catch(std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
            << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;
}
