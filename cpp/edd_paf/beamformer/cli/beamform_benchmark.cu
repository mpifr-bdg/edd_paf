#include <benchmark/benchmark.h>
#include <cuda.h>
#include <psrdada_cpp/testing_tools/streams.hpp>
#include <psrdada_cpp/raw_bytes.hpp>
#include <psrdada_cpp/cli_utils.hpp>
#include <psrdada_cpp/cuda_utils.hpp>
#include <psrdada_cpp/testing_tools/benchmark_tools.hpp>
#include <thrust/device_vector.h>
#include "edd_paf/beamformer/pipeline.cuh"

namespace psr = psrdada_cpp;
namespace bf = edd_paf::beamformer;

void set_counters(benchmark::State& state, bf::PipelineConfig& conf, std::size_t tot_size)
{
    state.counters["ibyte"] = conf.input_size() * sizeof(char2);
    state.counters["nsample"] = conf.n_samples;
    state.counters["nchannel"] = conf.n_channel;
    state.counters["nantenna"] = conf.n_elements;
    state.counters["nbeam"] = conf.n_beam;
    state.counters["integration"] = conf.integration;
    state.counters["used_bytes"] = tot_size;
}

static void BF_PIPELINE(benchmark::State& state) {
    bf::PipelineConfig conf({
        (std::size_t)state.range(0),
        (std::size_t)state.range(1),
        (std::size_t)state.range(2),
        (std::size_t)state.range(3),
        (std::size_t)state.range(4), 32});
    psr::testing_tools::DummyStream<char> voltage_handler(false);
    psr::testing_tools::DummyStream<char> power_handler(false);
    psr::testing_tools::DummyStream<char> stokes_handler(false);
    char* idata;
    cudaEvent_t start, stop;

    CUDA_ERROR_CHECK(cudaEventCreate(&start));
    CUDA_ERROR_CHECK(cudaEventCreate(&stop));
    float milliseconds = 0;
    std::vector<double> vec(0);
    try
    {
        bf::Pipeline<decltype(voltage_handler), float2>
            test_object(conf, voltage_handler, power_handler, stokes_handler);
        std::size_t buffer_size = conf.input_size() * sizeof(char2);
        CUDA_ERROR_CHECK(cudaMallocHost((void**)&idata, buffer_size));
        psr::RawBytes rawdata(idata, buffer_size, buffer_size);
        // Warm phase
        while(voltage_handler.call_count() < 5){
            test_object(rawdata);
        }
        for (auto _ : state){
            CUDA_ERROR_CHECK(cudaEventRecord(start));

            test_object(rawdata);

            CUDA_ERROR_CHECK(cudaEventRecord(stop));
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&milliseconds, start, stop);
            state.SetIterationTime(milliseconds / 1000);
            vec.push_back(buffer_size / (milliseconds / 1000));
        }
        state.counters["rate"] = benchmark::Counter(buffer_size,
            benchmark::Counter::kIsIterationInvariantRate,
            benchmark::Counter::OneK::kIs1024);
        set_counters(state, conf, test_object.total_size());
        psr::testing_tools::getStats(vec, state);
    }catch(std::exception& e){
        state.counters["rate"] = 0;
        set_counters(state, conf, 0);
        psr::testing_tools::getStats(vec, state);
        CUDA_ERROR_CHECK(cudaFreeHost(idata));
    }
}


static void BF_KERNEL(benchmark::State& state) {
    bf::PipelineConfig conf({(std::size_t)state.range(0),
        (std::size_t)state.range(1),
        (std::size_t)state.range(2),
        (std::size_t)state.range(3),
        (std::size_t)state.range(4),
        32});

    std::size_t total_size = (conf.input_size() + conf.weight_size()) * sizeof(char2)
        + conf.output_raw_voltage_size() * sizeof(int2)
        + conf.output_stokes_i_size() * sizeof(float)
        + conf.output_full_stokes_size() * sizeof(float4);

    cudaStream_t stream;
    cudaEvent_t start, stop;
    float milliseconds = 0;
    std::vector<double> vec(0);
    CUDA_ERROR_CHECK(cudaStreamCreate(&stream));
    CUDA_ERROR_CHECK(cudaEventCreate(&start));
    CUDA_ERROR_CHECK(cudaEventCreate(&stop));
    try{
        thrust::device_vector<char2> idata(conf.input_size());
        thrust::device_vector<char2> wdata(conf.weight_size());
        thrust::device_vector<int2> odata_vol(conf.output_raw_voltage_size());
        thrust::device_vector<float> odata_sti(conf.output_stokes_i_size());
        thrust::device_vector<float4> odata_stf(conf.output_full_stokes_size());
        bf::Beamformer<float2>test_object(
            conf.n_samples,
            conf.n_channel,
            conf.n_elements,
            conf.n_beam,
            conf.nfft,
            conf.os_numerator,
            conf.os_denominator,
            conf.integration);
        test_object.check_config();
        for (auto _ : state){
            CUDA_ERROR_CHECK(cudaEventRecord(start));

            test_object.process(
                thrust::raw_pointer_cast(idata.data()),
                thrust::raw_pointer_cast(wdata.data()),
                thrust::raw_pointer_cast(odata_vol.data()),
                thrust::raw_pointer_cast(odata_sti.data()),
                thrust::raw_pointer_cast(odata_stf.data()),
                stream);

            CUDA_ERROR_CHECK(cudaEventRecord(stop));
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&milliseconds, start, stop);
            state.SetIterationTime(milliseconds / 1000);
            vec.push_back(conf.input_size() * sizeof(char2) / (milliseconds / 1000));
        }
        state.counters["rate"] = benchmark::Counter(conf.input_size() * sizeof(char2),
            benchmark::Counter::kIsIterationInvariantRate,
            benchmark::Counter::OneK::kIs1024);
        set_counters(state, conf, total_size);
        psr::testing_tools::getStats(vec, state);
    }catch(std::exception& e){
        state.counters["rate"] = 0;
        set_counters(state, conf, total_size);
        psr::testing_tools::getStats(vec, state);
    }
}

BENCHMARK(BF_PIPELINE)
    ->ArgsProduct({
        benchmark::CreateRange(32768, 32768, 1),
        benchmark::CreateRange(16, 16, 1),
        benchmark::CreateRange(128, 1024, 4),
        benchmark::CreateRange(128, 1024, 4),
        benchmark::CreateRange(8, 64, 8)
    })->Iterations(10)->UseManualTime();

BENCHMARK(BF_KERNEL)->ArgsProduct({
        benchmark::CreateRange(32768, 32768, 1),
        benchmark::CreateRange(16, 16, 1),
        benchmark::CreateRange(128, 1024, 2),
        benchmark::CreateRange(128, 1024, 2),
        benchmark::CreateRange(8, 64, 8)
    })->Iterations(50)->UseManualTime();

int main(int argc, char** argv) {
    char arg0_default[] = "benchmark";
    char* args_default = arg0_default;
    psr::set_log_level("INFO");
    if (!argv) {
      argc = 1;
      argv = &args_default;
    }
    ::benchmark::Initialize(&argc, argv);
    if (::benchmark::ReportUnrecognizedArguments(argc, argv)) return 1;
    ::benchmark::RunSpecifiedBenchmarks();
    ::benchmark::Shutdown();
    return 0;
}
