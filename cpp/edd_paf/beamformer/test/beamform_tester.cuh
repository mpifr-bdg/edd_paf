#pragma once

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <cuda_fp16.h>
#include <random>
#include <cmath>
#include <gtest/gtest.h>
#include <fstream>
#include <iostream>

#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/cuda_utils.hpp>

#include "edd_paf/cuda_utils.cuh"
#include "edd_paf/beamformer/pipeline.cuh"
#include "edd_paf/beamformer/beamformer.cuh"

namespace edd_paf{
namespace beamformer{
namespace test{


/**
 * @brief
 *
 * @param conf
 * @param in
 * @param weights
 * @param ovoltage
 * @param ostokesi
 * @param ostokesf
 */
void cpu_beamform(
    PipelineConfig conf,
    thrust::host_vector<char2>& in,
    thrust::host_vector<char2>& weights,
    thrust::host_vector<int2>& ovoltage,
    thrust::host_vector<float>& ostokesi,
    thrust::host_vector<float4>& ostokesf);

/**
 * @brief
 *
 * @param conf
 * @param in
 * @param weights
 * @param ovoltage
 * @param ostokesi
 * @param ostokesf
 */
void gpu_beamform(
    PipelineConfig conf,
    thrust::host_vector<char2>& in,
    thrust::host_vector<char2>& weights,
    thrust::host_vector<int2>& ovoltage,
    thrust::host_vector<float>& ostokesi,
    thrust::host_vector<float4>& ostokesf);

/**
 * @brief
 *
 * @param cpu
 * @param gpu
 */
void compare_voltage(
    const thrust::host_vector<int2> cpu,
    const thrust::host_vector<int2> gpu);

/**
 * @brief
 *
 * @param cpu
 * @param gpu
 * @param tol
 */
void compare_stokesi(
    const thrust::host_vector<float> cpu,
    const thrust::host_vector<float> gpu,
    const float tol);

/**
 * @brief
 *
 * @param cpu
 * @param gpu
 * @param tol
 */
void compare_stokesf(
    const thrust::host_vector<float4> cpu,
    const thrust::host_vector<float4> gpu,
    const float tol);


/**
 * @brief
 *
 */
class BeamformTester : public ::testing::TestWithParam<PipelineConfig> {

protected:
  void SetUp() override;
  void TearDown() override;
public:
    /**
     * @brief Construct a new Beamform Tester object
     *
     */
    BeamformTester();

    /**
     * @brief Destroy the Beamform Tester object
     *
     */
    ~BeamformTester();

    /**
     * @brief
     *
     * @param tol
     */
    void reference_test(float tol=0.0001);

    /**
     * @brief
     *
     */
    void analytical_test();

    /**
     * @brief
     *
     * @tparam T
     * @param vec
     * @param fname
     */
    template<typename T>
    void write_vector2disk(thrust::host_vector<T> vec, std::string fname);


    // thrust::host_vector<float2> convert_vec(thrust::host_vector<__half2> in);
    // thrust::host_vector<float2> convert_vec(thrust::host_vector<int16_t> in);


private:

    PipelineConfig conf;
    std::size_t _left;
    std::size_t _right;
    bool _expected_err = 0;
    thrust::host_vector<char2> input;
    thrust::host_vector<char2> weights;
    thrust::host_vector<int2> cpu_ovol;
    thrust::host_vector<int2> gpu_ovol;
    thrust::host_vector<float4> cpu_ostf;
    thrust::host_vector<float4> gpu_ostf;
    thrust::host_vector<float> cpu_osti;
    thrust::host_vector<float> gpu_osti;

};

} // namespace beamforming
} // namespace edd_paf
} // namespace test

#include "edd_paf/beamformer/test/src/beamform_tester.cu"