#include <gtest/gtest.h>

#include "edd_paf/beamformer/test/beamform_tester.cuh"
#include "edd_paf/beamformer/test/pipeline_tester.cuh"
#include "edd_paf/beamformer/test/io_tester.cuh"

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
