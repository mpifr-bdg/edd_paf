#ifndef EDD_PAF_BEAMFORMER_TEST_PIPELINE_TESTER_CU
#define EDD_PAF_BEAMFORMER_TEST_PIPELINE_TESTER_CU

namespace psr = psrdada_cpp;
namespace psr_testing = psr::testing_tools;

namespace edd_paf {
namespace beamformer {
namespace test {

const int header_block_size = 4096;

void PipelineTester::test_header_no_sample_clock()
{
    std::size_t sample_clock, sample_clock_start;
    char* header = new char[header_block_size]; std::memset(header, 0, header_block_size);
    psr::RawBytes raw_header(header, header_block_size, header_block_size);
    Pipeline<decltype(consumer_vol), float2> pipeline(_config, consumer_vol, consumer_sti, consumer_stf);
    pipeline.init(raw_header);

    ASSERT_EQ(ascii_header_get(consumer_vol.header_ptr(), "SAMPLE_CLOCK_START", "%lu", &sample_clock_start), 1);
    ASSERT_EQ(ascii_header_get(consumer_vol.header_ptr(), "SAMPLE_CLOCK", "%lu", &sample_clock), 1);
    ASSERT_EQ(sample_clock_start, 0);
    ASSERT_EQ(sample_clock, 0);
    ASSERT_EQ(ascii_header_get(consumer_sti.header_ptr(), "SAMPLE_CLOCK_START", "%lu", &sample_clock_start), 1);
    ASSERT_EQ(ascii_header_get(consumer_sti.header_ptr(), "SAMPLE_CLOCK", "%lu", &sample_clock), 1);
    ASSERT_EQ(sample_clock_start, 0);
    ASSERT_EQ(sample_clock, 0);
    ASSERT_EQ(ascii_header_get(consumer_stf.header_ptr(), "SAMPLE_CLOCK_START", "%lu", &sample_clock_start), 1);
    ASSERT_EQ(ascii_header_get(consumer_stf.header_ptr(), "SAMPLE_CLOCK", "%lu", &sample_clock), 1);
    ASSERT_EQ(sample_clock_start, 0);
    ASSERT_EQ(sample_clock, 0);
}
void PipelineTester::test_header()
{
    std::size_t sample_clock_exp = 3200000000;
    std::size_t sample_clock_start_exp = 8192;
    std::size_t sample_clock, sample_clock_start;
    char* header = new char[header_block_size]; std::memset(header, 0, header_block_size);
    ascii_header_set(header, "SAMPLE_CLOCK", "%lu", sample_clock_exp);
    ascii_header_set(header, "SAMPLE_CLOCK_START", "%lu", sample_clock_start_exp);
    Pipeline<decltype(consumer_vol), float2> pipeline(_config, consumer_vol, consumer_sti, consumer_stf);
    psr::RawBytes raw_header(header, header_block_size, header_block_size);
    pipeline.init(raw_header);

    ASSERT_EQ(ascii_header_get(consumer_vol.header_ptr(), "SAMPLE_CLOCK_START", "%lu", &sample_clock_start), 1);
    ASSERT_EQ(ascii_header_get(consumer_vol.header_ptr(), "SAMPLE_CLOCK", "%lu", &sample_clock), 1);
    ASSERT_EQ(sample_clock_start, sample_clock_start_exp);
    ASSERT_EQ(sample_clock, sample_clock_exp);
    ASSERT_EQ(ascii_header_get(consumer_sti.header_ptr(), "SAMPLE_CLOCK_START", "%lu", &sample_clock_start), 1);
    ASSERT_EQ(ascii_header_get(consumer_sti.header_ptr(), "SAMPLE_CLOCK", "%lu", &sample_clock), 1);
    ASSERT_EQ(sample_clock_start, sample_clock_start_exp / _config.nfft);
    ASSERT_EQ(sample_clock, sample_clock_exp / _config.nfft);
    ASSERT_EQ(ascii_header_get(consumer_stf.header_ptr(), "SAMPLE_CLOCK_START", "%lu", &sample_clock_start), 1);
    ASSERT_EQ(ascii_header_get(consumer_stf.header_ptr(), "SAMPLE_CLOCK", "%lu", &sample_clock), 1);
    ASSERT_EQ(sample_clock_start, sample_clock_start_exp / (_config.os_ratio() * _config.integration));
    ASSERT_EQ(sample_clock, sample_clock_exp / (_config.os_ratio() * _config.integration));
}

void PipelineTester::test_streaming(std::size_t nbuffers)
{
    // Create a stream of
    cudaStream_t tmp_stream;
    CUDA_ERROR_CHECK(cudaStreamCreate(&tmp_stream));
    // Generate test vectors
    thrust::host_vector<char2> idata(_config.input_size() * nbuffers);
    thrust::host_vector<char2> unbatch(_config.input_size());
    thrust::host_vector<char2> wdata(_config.weight_size());
    thrust::host_vector<int2> odata_vol_cpu(_config.output_raw_voltage_size());
    thrust::host_vector<int2> odata_vol_cpu_batch(_config.output_raw_voltage_size());
    thrust::host_vector<int2> odata_vol_gpu(_config.output_raw_voltage_size());
    thrust::host_vector<float> odata_sti_gpu(_config.output_stokes_i_size());
    thrust::host_vector<float4> odata_stf_gpu(_config.output_full_stokes_size());
    fill_random_cplx<char2>(idata, 0x7F);
    fill_random_cplx<char2>(wdata, 0x7F);

    // Write data to the pipeline
    InterprocessMemory<char2> smem_weights(wdata.size(), "SharedMemoryWeights", true);
    smem_weights.writing();
    std::vector<char2> swdata(wdata.begin(), wdata.end());
    // Create the pipeline instance
    Pipeline<decltype(consumer_vol), float2> pipeline(_config, consumer_vol, consumer_sti, consumer_stf);
    smem_weights.set_data(swdata);
    while(!pipeline.weight_updates()){usleep(100000);}
    // Run the pipeline in a streaming fashion
    for(std::size_t i = 0; i < nbuffers; i++){
        // Create a data block and pass it to the pipeline under test
        psrdada_cpp::RawBytes block(reinterpret_cast<char *>(&idata[i * _config.input_size()]),
            _config.input_size() * sizeof(char2),
            _config.input_size() * sizeof(char2));
        ASSERT_FALSE(pipeline(block));

        // When the consumers received the first data block, we can compare data
        if(consumer_vol.call_count() > 1)
        {
            // Reset the CPU buffers, otherwise data would be added up
            thrust::host_vector<float> odata_sti_cpu(_config.output_stokes_i_size());
            thrust::host_vector<float4> odata_stf_cpu(_config.output_full_stokes_size());

            // Copy the GPU output
            memcpy(odata_vol_gpu.data(), consumer_vol.data_ptr(), odata_vol_gpu.size() * sizeof(int2));
            memcpy(odata_sti_gpu.data(), consumer_sti.data_ptr(), odata_sti_gpu.size() * sizeof(float));
            memcpy(odata_stf_gpu.data(), consumer_stf.data_ptr(), odata_stf_gpu.size() * sizeof(float4));

            // unbatch data from TFPET -> FPET - ugly
            // We use the 2D memcpy from CUDA
            thrust::host_vector<char2> in(
                idata.begin() + (consumer_vol.call_count()-1) * _config.input_size(),
                idata.begin() + (consumer_vol.call_count()) * _config.input_size());

            psrdada_cpp::RawBytes tmp_block(
                reinterpret_cast<char *>(in.data()),
                _config.input_size() * sizeof(char2),
                _config.input_size() * sizeof(char2));

            copy_input(
                thrust::raw_pointer_cast(unbatch.data()),
                tmp_block.ptr(),
                _config, tmp_stream, cudaMemcpyHostToHost);

            CUDA_ERROR_CHECK(cudaStreamSynchronize(tmp_stream));

            cpu_beamform(_config, unbatch, wdata, odata_vol_cpu, odata_sti_cpu, odata_stf_cpu);

            copy_output_voltage<int2>(
                thrust::raw_pointer_cast(odata_vol_cpu_batch.data()),
                thrust::raw_pointer_cast(odata_vol_cpu.data()),
                _config, tmp_stream, cudaMemcpyHostToHost);
            CUDA_ERROR_CHECK(cudaStreamSynchronize(tmp_stream));
            compare_voltage(odata_vol_cpu_batch, odata_vol_gpu);
            compare_stokesi(odata_sti_cpu, odata_sti_gpu, 0.01);
            compare_stokesf(odata_stf_cpu, odata_stf_gpu, 0.01);
        }
    }
    smem_weights.stop();
}


TEST_P(PipelineTester, test_header_no_sample_clock)
{
    std::cout << std::endl
        << "----------------------------" << std::endl
        << " Testing BF-Pipeline init() " << std::endl
        << "----------------------------" << std::endl;
    test_header_no_sample_clock();
}
TEST_P(PipelineTester, test_header)
{
    std::cout << std::endl
        << "----------------------------" << std::endl
        << " Testing BF-Pipeline init() " << std::endl
        << "----------------------------" << std::endl;
    test_header();
}
TEST_P(PipelineTester, test_streaming_float)
{
    if(!sufficientComputeCap(7, 5)){return;}
    std::cout << std::endl
        << "--------------------------------" << std::endl
        << " Testing BF-Pipeline operator() " << std::endl
        << "--------------------------------" << std::endl;
    test_streaming();
}

INSTANTIATE_TEST_SUITE_P(PipelineTesterInstantiation, PipelineTester, ::testing::Values(
	// device ID |  samples |   channels |  elements |  beam |  integration |   nfft |  batch_size| ovol_batch_size
    PipelineConfig{ 128,        1,          128,        128,    4,              32,     64,        128},
    PipelineConfig{ 256,        2,          512,        96,     4,              32,     128,        256},
    PipelineConfig{ 1024,       4,          128,        64,     8,              32,     64,         64},
    PipelineConfig{ 512,        8,          128,        32,     16,             32,     256,        256},
    PipelineConfig{ 8192,       1,          64,         128,    32,             32,     2048,       2048}, // fails sometimes
    PipelineConfig{ 2048,       4,          32,         64,     16,             32,     512,        512},
    PipelineConfig{ 768,        12,         256,        32,     24,             32,     128,        128},
    PipelineConfig{ 4096,       1,          16,         16,     64,             32,     1024,       1024}, // fails
    PipelineConfig{ 65536,      1,          32,         16,     32,             32,     4096,       4096}, // fails
    PipelineConfig{ 2048,       4,          32,         64,     16,             32,     512,        512, 1, 1},
    PipelineConfig{ 768,        12,         256,        32,     24,             32,     128,        128, 1, 1},
    PipelineConfig{ 4096,       1,          16,         16,     64,             32,     1024,       1024, 1, 1} // fails
    // PipelineConfig{ 65536,      1,          128,       128,     32,             32,     4096,       4096}
));

}
}
}

#endif // EDD_PAF_BEAMFORMER_TEST_PIPELINE_TESTER_CU