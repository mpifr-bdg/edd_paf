#ifndef EDD_PAF_BEAMFORMER_TEST_IO_TESTER_CU
#define EDD_PAF_BEAMFORMER_TEST_IO_TESTER_CU

namespace psr_testing = psrdada_cpp::testing_tools;

namespace edd_paf {
namespace beamformer {
namespace test {

void IOTester::test_copy_input()
{

    thrust::host_vector<char2> expect(_config.input_size());
    thrust::host_vector<char2> actual(_config.input_size());
    fill_random_cplx<char2>(expect);
    std::cout << "Filled vector, transpose" << std::endl;
    copy_input(
        thrust::raw_pointer_cast(actual.data()),
        reinterpret_cast<char*>(thrust::raw_pointer_cast(expect.data())),
        _config, stream, cudaMemcpyHostToHost);
    CUDA_ERROR_CHECK(cudaStreamSynchronize(stream));
    std::cout << "Transposed data" << std::endl;

    int i_et = _config.n_elements * _config.batch_size;
    int i_pet = _config.n_pol * i_et;
    int i_fpet = _config.n_channel * i_pet;
    int o_et = _config.n_elements * _config.n_samples;
    int o_pet = _config.n_pol * o_et;
    int exp_idx, act_idx;
    for(std::size_t t1 = 0; t1 < _config.n_samples/_config.batch_size; t1++)
    {
        for (std::size_t f = 0; f < _config.n_channel; f++)
        {
            for(std::size_t p = 0; p < _config.n_pol; p++)
            {
                for(std::size_t a = 0; a < _config.n_elements; a++)
                {
                    for(std::size_t t2 = 0; t2 < _config.batch_size; t2++)
                    {
                        exp_idx = t1 * i_fpet + f * i_pet + p * i_et + a * _config.batch_size + t2;
                        act_idx = f * o_pet + p * o_et + a * _config.n_samples + t1 * _config.batch_size + t2;
                        ASSERT_TRUE(expect[exp_idx].x == actual[act_idx].x || expect[exp_idx].y == actual[act_idx].y)
                            << "Voltage: CPU and GPU result is unequal for element " << exp_idx
                            << "\n  CPU result: " << std::to_string(expect[exp_idx].x)  << "+ i*" << std::to_string(expect[exp_idx].y)
                            << "\n  GPU result: " << std::to_string(actual[act_idx].x) << "+ i*" << std::to_string(actual[act_idx].y);
                    }
                }
            }
        }
    }

}

void IOTester::test_copy_output_voltage()
{
    thrust::host_vector<int2> expect(_config.output_raw_voltage_size());
    thrust::host_vector<int2> actual(_config.output_raw_voltage_size());
    fill_random_cplx<int2>(expect);

    copy_output_voltage<int2>(
        thrust::raw_pointer_cast(actual.data()),
        thrust::raw_pointer_cast(expect.data()),
        _config, stream, cudaMemcpyHostToHost);
    CUDA_ERROR_CHECK(cudaStreamSynchronize(stream));

    // Transposing from B-F-P-T to T-B-F-P-T
    for(std::size_t t1 = 0; t1 < _config.n_samples; t1+=_config.ovol_batch_size){
        for(std::size_t b = 0; b < _config.raw_beams(); b++){
            for(std::size_t f = 0; f < _config.n_channel; f++){
                for(std::size_t p = 0; p < _config.n_pol; p++){
                    for(std::size_t t2 = 0; t2 < _config.ovol_batch_size; t2++){

                        std::size_t exp_idx = b * _config.n_channel * _config.n_pol * _config.n_samples
                            + f * _config.n_pol * _config.n_samples + p * _config.n_samples + t1 + t2;

                        std::size_t act_idx = t1 * _config.raw_beams() * _config.n_channel * _config.n_pol
                            + b * _config.n_channel * _config.n_pol * _config.ovol_batch_size
                            + f * _config.n_pol * _config.ovol_batch_size + p * _config.ovol_batch_size + t2;

                        ASSERT_TRUE(expect[exp_idx].x == actual[act_idx].x || expect[exp_idx].y == actual[act_idx].y)
                            << "Voltage: CPU and GPU result is unequal for element " << exp_idx << " vs. " << act_idx
                            << "\n  CPU result: " << std::to_string(expect[exp_idx].x)  << "+ i*" << std::to_string(expect[exp_idx].y)
                            << "\n  GPU result: " << std::to_string(actual[act_idx].x) << "+ i*" << std::to_string(actual[act_idx].y);
                    }
                }
            }
        }
    }
}

void IOTester::test_copy_output_stokesi()
{
    thrust::host_vector<float> expect(_config.output_stokes_i_size());
    thrust::host_vector<float> actual(_config.output_stokes_i_size());
    fill_random<float>(expect);

    copy_output_stokesi<float>(
        thrust::raw_pointer_cast(actual.data()),
        thrust::raw_pointer_cast(expect.data()),
        _config, stream, cudaMemcpyHostToHost);
    CUDA_ERROR_CHECK(cudaStreamSynchronize(stream));

    for(std::size_t i = 0; i < expect.size(); i++)
    {
        ASSERT_TRUE(expect[i] == actual[i])
            << "Voltage: CPU and GPU result is unequal for element " << i
            << "\n  CPU result: " << std::to_string(expect[i])
            << "\n  GPU result: " << std::to_string(actual[i]);
    }
}

void IOTester::test_copy_output_stokesf()
{
    thrust::host_vector<float4> expect(_config.output_full_stokes_size());
    thrust::host_vector<float4> actual(_config.output_full_stokes_size());
    fill_random_stokes<float4>(expect);

    copy_output_stokesf<float4>(
        thrust::raw_pointer_cast(actual.data()),
        thrust::raw_pointer_cast(expect.data()),
        _config, stream, cudaMemcpyHostToHost);
    CUDA_ERROR_CHECK(cudaStreamSynchronize(stream));
    std::cout << "sychronized" << std::endl;

    // // Transposing from B-F-T-F-S to T-B-F-S
    // for(std::size_t t = 0; t < _config.nspectra(); t++){
    //     for(std::size_t b = 0; b < _config.n_beam; b++){
    //         for(std::size_t f = 0; f < _config.n_channel; f++){
    //             for(std::size_t f2 = 0; f2 < _config.spectrum_size(); f2++){

    //                 std::size_t exp_idx = b * _config.n_channel * _config.nspectra() * _config.spectrum_size()
    //                     + f * _config.nspectra() * _config.spectrum_size()
    //                     + t * _config.spectrum_size() + f2;

    //                 std::size_t act_idx = t * _config.n_beam * _config.n_channel * _config.spectrum_size()
    //                     + b * _config.n_channel * _config.spectrum_size()
    //                     + f * _config.spectrum_size() + f2;

    //                 ASSERT_TRUE((expect[exp_idx].x == actual[act_idx].x && expect[exp_idx].x == actual[act_idx].x)
    //                 || (expect[exp_idx].y == actual[act_idx].y && expect[exp_idx].y == actual[act_idx].y)
    //                 || (expect[exp_idx].z == actual[act_idx].z && expect[exp_idx].z == actual[act_idx].z)
    //                 || (expect[exp_idx].w == actual[act_idx].w && expect[exp_idx].w == actual[act_idx].w))
    //                     << "Full Stokes: CPU and GPU result is unequal for element " << std::to_string(exp_idx) << " and " << std::to_string(exp_idx) << std::endl
    //                     << "  CPU result: I=" << std::to_string(expect[exp_idx].x) << " Q=" << std::to_string(expect[exp_idx].y)
    //                     << " U="<< std::to_string(expect[exp_idx].z) << " V=" << std::to_string(expect[exp_idx].w) << std::endl
    //                     << "  GPU result: I=" << std::to_string(actual[act_idx].x) << " Q=" << std::to_string(actual[act_idx].y)
    //                     << " U=" << std::to_string(actual[act_idx].z) << " V=" << std::to_string(actual[act_idx].w) << std::endl;

    //             }
    //         }
    //     }
    // }
}

// TEST_P(IOTester, test_copy_input){
//     std::cout << std::endl
//         << "----------------------" << std::endl
//         << " Testing copy_input() " << std::endl
//         << "----------------------" << std::endl;
//     test_copy_input();
// }

// TEST_P(IOTester, test_copy_output_voltage){
//     std::cout << std::endl
//         << "-------------------------------" << std::endl
//         << " Testing copy_output_voltage() " << std::endl
//         << "-------------------------------" << std::endl;
//     test_copy_output_voltage();
// }

TEST_P(IOTester, test_copy_output_stokesi){
    std::cout << std::endl
        << "------------------------------------" << std::endl
        << " Testing test_copy_output_stokesi() " << std::endl
        << "------------------------------------" << std::endl;
    test_copy_output_stokesi();
}

// TEST_P(IOTester, test_copy_output_stokesf){
//     std::cout << std::endl
//         << "------------------------------------" << std::endl
//         << " Testing test_copy_output_stokesf() " << std::endl
//         << "------------------------------------" << std::endl;
//     test_copy_output_stokesf();
// }

INSTANTIATE_TEST_SUITE_P(IOTesterInstantiation, IOTester, ::testing::Values(
	// device ID |  samples |   channels |  elements |  beam |  integration |   nfft |  batch_size| ovol_batch_size
    PipelineConfig{ 128,        1,          128,        128,    4,              32,     64,         128},
    PipelineConfig{ 256,        2,          512,        96,     4,              32,     128,        256},
    PipelineConfig{ 1024,       4,          128,        64,     8,              32,     64,         64},
    PipelineConfig{ 512,        8,          128,        32,     16,             32,     256,        256},
    PipelineConfig{ 8192,       1,          64,         128,    32,             32,     2048,       2048}, // fails sometimes
    PipelineConfig{ 2048,       4,          32,         64,     16,             32,     512,        512},
    PipelineConfig{ 768,        12,         256,        32,     24,             32,     128,        128},
    PipelineConfig{ 4096,       1,          16,         16,     64,             32,     1024,       1024}, // fails
    PipelineConfig{ 65536,      1,          32,         16,     32,             32,     4096,       4096}, // fails
    PipelineConfig{ 65536,      1,          128,       128,     32,             32,     4096,       4096}
));

}
}
}

#endif