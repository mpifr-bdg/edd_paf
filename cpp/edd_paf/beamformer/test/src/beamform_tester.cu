#ifndef EDD_PAF_BEAMFORMER_TEST_BEAMFORM_TESTER
#define EDD_PAF_BEAMFORMER_TEST_BEAMFORM_TESTER

namespace edd_paf{
namespace beamformer{
namespace test{


void gpu_beamform(
    PipelineConfig conf,
    thrust::host_vector<char2>& in,
    thrust::host_vector<char2>& weights,
    thrust::host_vector<int2>& ovoltage,
    thrust::host_vector<float>& ostokesi,
    thrust::host_vector<float4>& ostokesf)
{
    BOOST_LOG_TRIVIAL(debug) << "Calculating GPU results... " << std::flush;
    thrust::device_vector<char2> dev_input = in;
    thrust::device_vector<char2> dev_weights = weights;
    thrust::device_vector<int2> dev_ovoltage(ovoltage.size());
    thrust::device_vector<float>  dev_ostokesi(ostokesi.size());
    thrust::device_vector<float4> dev_ostokesf(ostokesf.size());

    cudaStream_t stream;
    CUDA_ERROR_CHECK(cudaStreamCreate(&stream));

    Beamformer<float2> beamformer(
        conf.n_samples,
        conf.n_channel,
        conf.n_elements,
        conf.n_beam,
        conf.nfft,
        conf.os_numerator,
        conf.os_denominator,
        conf.integration);
    if(beamformer.check_config()){return;}

    beamformer.process(
        thrust::raw_pointer_cast(dev_input.data()),
        thrust::raw_pointer_cast(dev_weights.data()),
        thrust::raw_pointer_cast(dev_ovoltage.data()),
        thrust::raw_pointer_cast(dev_ostokesi.data()),
        thrust::raw_pointer_cast(dev_ostokesf.data()),
        stream
    );

    CUDA_ERROR_CHECK(cudaDeviceSynchronize());
    CUDA_ERROR_CHECK(cudaStreamDestroy(stream));
    BOOST_LOG_TRIVIAL(debug) << "Copying GPU results to host... " << std::flush;
    // Copy device 2 host (output)
    ovoltage = dev_ovoltage;
    ostokesi = dev_ostokesi;
    ostokesf = dev_ostokesf;
    BOOST_LOG_TRIVIAL(debug) << "GPU processing done" << std::flush;
}

void cpu_beamform(
    PipelineConfig conf,
    thrust::host_vector<char2>& in,
    thrust::host_vector<char2>& weights,
    thrust::host_vector<int2>& ovoltage,
    thrust::host_vector<float >& ostokesi,
    thrust::host_vector<float4>& ostokesf)
{
    BOOST_LOG_TRIVIAL(debug) << "Calculating CPU results... " << std::flush;
  	int ou_idx_vol = 0, ou_idx_full = 0, ou_idx_stokesi = 0, in_idx = 0, wg_idx = 0;
    int2 voltage = {0,0};
    thrust::host_vector<int2> ovol_inter(
        conf.n_beam * conf.n_channel * conf.n_samples * conf.n_pol);

    // Do cGEMM
    for(std::size_t b = 0; b < conf.n_beam; b++){
        for(std::size_t f = 0; f < conf.n_channel; f++){
            for(std::size_t t = 0; t < conf.n_samples; t++){
                for(std::size_t p = 0; p < conf.n_pol; p++){
                    ou_idx_vol = b * conf.n_channel * conf.n_samples * conf.n_pol
                        + f * conf.n_samples * conf.n_pol
                        + p * conf.n_samples + t;
                    voltage = {0,0};
                    for(std::size_t e = 0; e < conf.n_elements; e++){
                        in_idx = f * conf.n_pol * conf.n_samples * conf.n_elements
                            + p * conf.n_samples * conf.n_elements
                            + e * conf.n_samples  + t;
                        wg_idx = f * conf.n_pol * conf.n_beam * conf.n_elements
                            + p * conf.n_beam * conf.n_elements
                            + b * conf.n_elements + e;
                        voltage = cmadd(in[in_idx], weights[wg_idx], voltage);
                    }
                    if(b < N_MAX_RBEAMS){
                        ovoltage[ou_idx_vol].x = voltage.x / (float)conf.n_elements;
                        ovoltage[ou_idx_vol].y = voltage.y / (float)conf.n_elements;
                    }
                    ovol_inter[ou_idx_vol].x = voltage.x / (float)conf.n_elements;
                    ovol_inter[ou_idx_vol].y = voltage.y / (float)conf.n_elements;
                }
            }
  		}
  	}
    BOOST_LOG_TRIVIAL(debug) << "CPU beamforming done... ";
    BOOST_LOG_TRIVIAL(debug) << "Channelizing, detection and integration";
    // Do FFT
    int ou_idx_x;
    int ou_idx_y;
    thrust::host_vector<float2> ovoltage_fft(ovol_inter.size());
    thrust::host_vector<float4> full_stokes(conf.nfft);
    for(std::size_t b = 0; b < conf.n_beam; b++){
        for(std::size_t f = 0; f < conf.n_channel; f++){
            for(std::size_t p = 0; p < conf.n_pol; p++){
                for(std::size_t t = 0; t < conf.n_samples; t+=conf.nfft){
                    ou_idx_vol = b * conf.n_channel * conf.n_samples * conf.n_pol
                        + f * conf.n_samples * conf.n_pol
                        + p * conf.n_samples + t;
                    dft(&ovol_inter[ou_idx_vol], &ovoltage_fft[ou_idx_vol], conf.nfft);
                }
            }
            // Downsample and integrate
            int spec_cnt = 0;
            for(std::size_t t = 0; t < conf.n_samples; t+=conf.nfft)
            {
                if( (t / conf.nfft) % conf.integration == 0)
                {
                    // ou_idx_full = b * conf.n_channel * conf.full_stokes_samples()
                    //     + f * conf.full_stokes_samples()
                    //     + conf.spectrum_size() * spec_cnt;
                    // spec_cnt++;
                    ou_idx_full = spec_cnt * conf.n_beam * conf.n_channel * conf.spectrum_size()
                        + b * conf.n_channel * conf.spectrum_size()
                        + f * conf.spectrum_size();
                    spec_cnt++;
                }
                ou_idx_stokesi = b * conf.n_channel * conf.n_samples/conf.nfft
                    + f * conf.n_samples / conf.nfft
                    + t/conf.nfft;
                for(std::size_t f2 = 0; f2 < conf.nfft; f2++)
                {
                    ou_idx_x = b * conf.n_channel * conf.n_samples * conf.n_pol
                        + f * conf.n_samples * conf.n_pol + t + f2;
                    ou_idx_y = b * conf.n_channel * conf.n_samples * conf.n_pol
                        + f * conf.n_samples * conf.n_pol + conf.n_samples + t + f2;
                    if(f2 > conf.left() && f2 < conf.right())
                    {
                        full_stokes[f2] = stokes_iquv(ovoltage_fft[ou_idx_x], ovoltage_fft[ou_idx_y]);
                        ostokesi[ou_idx_stokesi] += full_stokes[f2].x / conf.nfft;
                        ostokesf[ou_idx_full + f2 - conf.left() - 1] = add_iquv(
                            ostokesf[ou_idx_full + f2 - conf.left() - 1], full_stokes[f2]);
                        // ostokesf[ou_idx_full + tt - conf.left() - 1] = div_iquv(add_iquv(ostokesf[ou_idx_full + tt - conf.left() - 1], full_stokes[tt]), conf.integration);
                    }
                }
            }
        }
    }
    BOOST_LOG_TRIVIAL(debug) << "CPU reference done... ";
}

void compare_voltage(
    const thrust::host_vector<int2> cpu,
    const thrust::host_vector<int2> gpu)
{
    BOOST_LOG_TRIVIAL(debug) << "Comparing results ..";
    // Check each element of CPU and GPU implementation
	for(std::size_t i = 0; i < gpu.size(); i++)
	{
        ASSERT_TRUE(cpu[i].x == gpu[i].x && cpu[i].y == gpu[i].y)
            << "Voltage: CPU and GPU result is unequal for element " << i
            << "\n  CPU result: " << cpu[i].x << "+ i*" << cpu[i].y
            << "\n  GPU result: " << gpu[i].x << "+ i*" << gpu[i].y;
    }
}

void compare_stokesi(
    const thrust::host_vector<float> cpu,
    const thrust::host_vector<float> gpu,
    const float tol)
{
    ASSERT_EQ(cpu.size(), gpu.size())
		<< "Host (" << cpu.size()/(1024*1024) << "MiB) and device ("
        << gpu.size()/(1024*1024) << "MiB) vector sizes not equal";
    BOOST_LOG_TRIVIAL(debug) << "Comparing results ..";
    float mean = std::accumulate(cpu.begin(), cpu.end(), 0.0) / cpu.size();
	for(std::size_t i = 0; i < gpu.size(); i++)
	{
        ASSERT_NEAR(cpu[i], gpu[i], mean * tol)
            << "Stokes I: CPU and GPU result is unequal for element " << i;
    }
}



void compare_stokesf(
  const thrust::host_vector<float4> cpu,
  const thrust::host_vector<float4> gpu,
  const float tol)
{
    // Check if vectors have the same size
    ASSERT_EQ(cpu.size(), gpu.size())
		<< "Host (" << cpu.size()/(1024*1024) << "MiB) and device ("
        << gpu.size()/(1024*1024) << "MiB) vector sizes not equal";
    float4 mean;
    for(const auto& val : cpu)
    {
        mean.x += std::fabs(val.x) / cpu.size();
        mean.y += std::fabs(val.y) / cpu.size();
        mean.z += std::fabs(val.z) / cpu.size();
        mean.w += std::fabs(val.w) / cpu.size();
    }
    for(std::size_t i = 0; i < gpu.size(); i++)
	{
        std::string emsg = "Full Stokes: CPU and GPU result is unequal for element " + std::to_string(i);
        ASSERT_NEAR(cpu[i].x, gpu[i].x, std::fabs(mean.x * 0.001)) << emsg;
        ASSERT_NEAR(cpu[i].y, gpu[i].y, std::fabs(mean.y * 0.001)) << emsg;
        ASSERT_NEAR(cpu[i].z, gpu[i].z, std::fabs(mean.z * 0.001)) << emsg;
        ASSERT_NEAR(cpu[i].w, gpu[i].w, std::fabs(mean.w * 0.001)) << emsg;
    }
}

BeamformTester::BeamformTester()
  : ::testing::TestWithParam<PipelineConfig>(),
  conf(GetParam()) // GetParam() is inherited from base TestWithParam
{
    BOOST_LOG_TRIVIAL(debug) << "Creating instance of BeamformTester";
	CUDA_ERROR_CHECK(cudaSetDevice(getDeviceHighestComputeCap()));
}


BeamformTester::~BeamformTester()
{
  BOOST_LOG_TRIVIAL(debug) << "Destroying instance of BeamformTester";
}


void BeamformTester::SetUp()
{
    BOOST_LOG_TRIVIAL(debug) << "BeamformTester::SetUp()" << std::endl;
    // Calulate memory size for input, weights and output
    std::size_t input_size = conf.input_size();
    std::size_t weight_size =  conf.weight_size();
    std::size_t ovol_size = conf.output_raw_voltage_size();
    std::size_t osti_size = conf.output_stokes_i_size();
    std::size_t ostf_size = conf.output_full_stokes_size();
    std::size_t required_mem_out = ovol_size * sizeof(int2)
        + osti_size * sizeof(float)
        + ostf_size * sizeof(float4);
    std::size_t required_mem = input_size * sizeof(char2)
        + weight_size * sizeof(char2)
        + required_mem_out;
    BOOST_LOG_TRIVIAL(debug) << "Required memory for input:    " << std::to_string(input_size * sizeof(char2) / (1024)) << "\tkiB";
    BOOST_LOG_TRIVIAL(debug) << "Required memory for weights:  " << std::to_string(weight_size * sizeof(char2) / (1024)) << "\tkiB";
    BOOST_LOG_TRIVIAL(debug) << "Required memory for ovoltage: " << std::to_string(ovol_size * sizeof(int2) / (1024)) << "\tkiB";
    BOOST_LOG_TRIVIAL(debug) << "Required memory for ostokesi: " << std::to_string(osti_size * sizeof(float) / (1024)) << "\tkiB";
    BOOST_LOG_TRIVIAL(debug) << "Required memory for ostokesf: " << std::to_string(ostf_size * sizeof(float4) / (1024)) << "\tkiB";
    BOOST_LOG_TRIVIAL(debug) << "Required device memory:       " << std::to_string(required_mem / (1024*1024)) << "\tMiB";
    BOOST_LOG_TRIVIAL(debug) << "Required host memory:         " << std::to_string((required_mem + required_mem_out) / (1024*1024)) << "\tMiB";

    input.resize(input_size);
    weights.resize(weight_size);
    cpu_ovol.resize(ovol_size);
    gpu_ovol.resize(ovol_size);
    cpu_ostf.resize(ostf_size);
    gpu_ostf.resize(ostf_size);
    cpu_osti.resize(osti_size);
    gpu_osti.resize(osti_size);
}


void BeamformTester::TearDown()
{
    BOOST_LOG_TRIVIAL(debug) << "TearDown()" << std::endl;
}

void BeamformTester::analytical_test()
{
    int et  = conf.n_samples * conf.n_elements;
    int pet = conf.n_pol * et;
    int be  = conf.n_beam * conf.n_elements;
    int pbe = conf.n_pol * be;
    int idx;
    // Generate a rectangle function for each element
    for(std::size_t f = 0; f < conf.n_channel; f++){
        for(std::size_t p = 0; p < conf.n_pol; p++){
            for(std::size_t e = 0; e < conf.n_elements; e++){
                for(std::size_t t = 0; t < conf.n_samples; t++){
                    idx = f * pet + p * et + e * conf.n_samples  + t;
                    (t > e) ? input[idx] = {127,0}: input[idx] = {0,0};
                }
            }
        }
    }
    // Generate test weights
    for(std::size_t f = 0; f < conf.n_channel; f++){
        for(std::size_t p = 0; p < conf.n_pol; p++){
            for(std::size_t b = 0; b < conf.n_beam; b++){
                for(std::size_t e = 0; e < conf.n_elements; e++){
                    idx = f * pbe + p * be + b * conf.n_elements  + e;
                    (b == e) ? weights[idx] = {1,0} : weights[idx] = {0,0};
                }
            }
        }
    }
	// launches CUDA kernel
	gpu_beamform(conf, input, weights, gpu_ovol, gpu_osti, gpu_ostf);
    // launches cpu beamforming
    cpu_beamform(conf, input, weights, cpu_ovol, cpu_osti, cpu_ostf);

    thrust::host_vector<int2> gpu_vol = gpu_ovol;
    thrust::host_vector<float> gpu_sti = gpu_osti;
    thrust::host_vector<char2> input_f = input;

    write_vector2disk(input_f, "input.dat");
    write_vector2disk(gpu_vol, "gpu_rawbeams.dat");
    write_vector2disk(cpu_ovol, "cpu_rawbeams.dat");
    write_vector2disk(gpu_sti, "gpu_stibeams.dat");
    write_vector2disk(cpu_osti, "cpu_stibeams.dat");
}

void BeamformTester::reference_test(float tol)
{
    // Generate test samples / normal distributed noise for input signal
    fill_random_cplx<char2>(input);
    fill_random_cplx<char2>(weights);
	// launches CUDA kernel
	gpu_beamform(conf, input, weights, gpu_ovol, gpu_osti, gpu_ostf);

    if(_expected_err == 0)
    {
        // launches cpu beamforming
        cpu_beamform(conf, input, weights, cpu_ovol, cpu_osti, cpu_ostf);
        compare_voltage(cpu_ovol, gpu_ovol);
        compare_stokesi(cpu_osti, gpu_osti, tol);
        compare_stokesf(cpu_ostf, gpu_ostf, tol);
    }
}

template<typename T>
void BeamformTester::write_vector2disk(thrust::host_vector<T> vec, std::string fname)
{
  std::ofstream ofile(fname, std::ios::binary);
  ofile.write(
    reinterpret_cast<char*>(thrust::raw_pointer_cast(vec.data())),
    sizeof(T)*vec.size()
  );
  ofile.close();
}


TEST_P(BeamformTester, BeamformerReferenceTest)
{
    if(!sufficientComputeCap(7, 5)){return;}
    std::cout << std::endl
        << "-----------------------------------------------------" << std::endl
        << " Testing beamforming against reference implementation" << std::endl
        << "-----------------------------------------------------" << std::endl << std::endl;
    reference_test(0.001);
}

// TEST_P(BeamformTester, BeamformerAnalyticalTests)
// {
//   std::cout << std::endl
//     << "--------------------------------------------------" << std::endl
//     << " Testing beamforming with half precision (16bit)  " << std::endl
//     << "--------------------------------------------------" << std::endl << std::endl;
//   analytical_test();
// }

INSTANTIATE_TEST_SUITE_P(BeamformerTesterInstantiation, BeamformTester, ::testing::Values(

	// device ID |  samples |   channels |  elements |  beam |  integration |   nfft |  batch_size| ovol_batch |    os_num| os_dnum
    PipelineConfig{ 128,        1,          128,        128,    4,              32,     128 ,       128 ,           32,     27},
    PipelineConfig{ 256,        2,          512,        96,     4,              32,     256 ,       256 ,           32,     27},
    PipelineConfig{ 1024,       4,          128,        64,     8,              32,     64  ,       64  ,           32,     27},
    PipelineConfig{ 512,        8,          128,        32,     16,             32,     256 ,       256 ,           32,     27},
    PipelineConfig{ 8192,       1,          64,         128,    32,             32,     2048,       2048,           32,     27},
    PipelineConfig{ 2048,       4,          32,         64,     16,             32,     512 ,       512 ,           32,     27},
    PipelineConfig{ 768,        12,         256,        32,     24,             32,     128 ,       128 ,           32,     27},
    PipelineConfig{ 4096,       1,          16,         16,     64,             32,     1024,       1024,           32,     27},
    PipelineConfig{ 768,        12,         256,        32,     24,             32,     128 ,       128 ,           1,       1},
    PipelineConfig{ 512,        8,          128,        32,     16,             32,     256 ,       256 ,           1,       1},
    PipelineConfig{ 8192,       1,          64,         128,    32,             32,     2048,       2048,           1,       1},
    PipelineConfig{ 4096,       1,          16,         16,     64,             32,     1024,       1024,           1,       1}
));


} // namespace test
} // namespace beamforming
} // namespace edd_paf

#endif // EDD_PAF_BEAMFORMER_TEST_BEAMFORM_TESTER