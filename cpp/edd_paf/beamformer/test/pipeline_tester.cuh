#pragma once

#include <gtest/gtest.h>
#include <ctime>
#include <random>
#include <string>
#include <cstring>
#include <psrdada_cpp/testing_tools/streams.hpp>
#include <psrdada_cpp/testing_tools/tools.cuh>

#include "edd_paf/memory.cuh"
#include "edd_paf/cuda_utils.cuh"
#include "edd_paf/beamformer/pipeline.cuh"
#include "edd_paf/beamformer/test/beamform_tester.cuh"

namespace psr_testing = psrdada_cpp::testing_tools;

namespace edd_paf {
namespace beamformer {
namespace test {


class PipelineTester : public ::testing::TestWithParam<PipelineConfig>
{
protected:
    void SetUp(){}
    void TearDown(){}
public:
    PipelineTester() : ::testing::TestWithParam<PipelineConfig>(),
        _config(GetParam()){};
    void test_header_no_sample_clock();
    void test_header();
    void test_streaming(std::size_t nbuffers=16);

private:
    PipelineConfig _config;
    psr_testing::DummyStream<char> consumer_vol;
    psr_testing::DummyStream<char> consumer_sti;
    psr_testing::DummyStream<char> consumer_stf;
};


}
}
}

#include "edd_paf/beamformer/test/src/pipeline_tester.cu"