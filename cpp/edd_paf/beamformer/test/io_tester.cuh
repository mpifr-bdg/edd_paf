#pragma once

#include <gtest/gtest.h>
#include <ctime>
#include <random>
#include <string>
#include <cstring>
#include <psrdada_cpp/testing_tools/streams.hpp>
#include <psrdada_cpp/testing_tools/tools.cuh>

#include "edd_paf/cuda_utils.cuh"
#include "edd_paf/beamformer/config.hpp"
#include "edd_paf/beamformer/io.cuh"

namespace edd_paf {
namespace beamformer {
namespace test {


class IOTester : public ::testing::TestWithParam<PipelineConfig>
{
protected:
    void SetUp(){
        CUDA_ERROR_CHECK(cudaStreamCreate(&stream));
    }
    void TearDown(){
        CUDA_ERROR_CHECK(cudaStreamDestroy(stream));
    }
public:
    IOTester() : ::testing::TestWithParam<PipelineConfig>(),
        _config(GetParam()){};
    void test_copy_input();
    void test_copy_output_voltage();
    void test_copy_output_stokesi();
    void test_copy_output_stokesf();

private:
    PipelineConfig _config;
    cudaStream_t stream;
};


}
}
}

#include "edd_paf/beamformer/test/src/io_tester.cu"