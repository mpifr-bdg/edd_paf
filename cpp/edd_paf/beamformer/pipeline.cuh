/*
* pipeline.cuh
* Author: Niclas Esser <nesser@mpifr-bonn.mpg.de>
* Description:
*  This files consists of a single class (Pipeline<HandlerType, ComputeType, ResultType>)
*  and a configuration structure (PipelineConfig).
*  An object of Pipeline is used to access data from psrdada buffers, unpacks them,
*  performs beamforming and writes the results back to another psrdada buffer.
*/
#pragma once

#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <cuda.h>
#include <cstring>

#include <ascii_header.h> // psrdada
#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/cuda_utils.hpp>
#include <psrdada_cpp/raw_bytes.hpp>
#include <psrdada_cpp/double_device_buffer.cuh>
#include <psrdada_cpp/double_host_buffer.cuh>

#include "edd_paf/cuda_utils.cuh"
#include "edd_paf/memory.cuh"
#include "edd_paf/beamformer/config.hpp"
#include "edd_paf/beamformer/io.cuh"
#include "edd_paf/beamformer/beamformer.cuh"

namespace psr = psrdada_cpp;

namespace edd_paf{
namespace beamformer{

template<class HandlerType, class ComputeType>
class Pipeline{
// Internal type defintions
private:
    typedef SharedPipelineBuffer<char2> WeightType;  // Type for beam weights
    typedef psr::DoubleBuffer<thrust::device_vector<char2>> InputType;    // Type for unpacked raw input data (voltage)
    typedef psr::DoubleBuffer<thrust::device_vector<int2>> VoltageBeam;// Type for beamfored output data
    typedef psr::DoubleBuffer<thrust::device_vector<float>> StokesIBeam;// Type for beamfored output data
    typedef psr::DoubleBuffer<thrust::device_vector<float4>> StokesFBeam;// Type for beamfored output data

public:
	/**
	* @brief	Constructs an object of Pipeline
	*
    * @param	PipelineConfig conf  Pipeline configuration containing all necessary parameters (declaration can be found in Types.cuh)
    * @param	HandlerType	handler_vol  Object for handling raw voltage beams
    * @param	HandlerType	handler_sti  Object for handling power beams high temporal resolution
    * @param	HandlerType	handler_stf  Object for handling full Stokes beams high spectral resolution
	*
	* @detail	Initializes the pipeline enviroment including device memory and processor objects.
	*/
	Pipeline(PipelineConfig& conf,
        HandlerType &handler_vol,
        HandlerType &handler_sti,
        HandlerType &handler_stf);


    /**
     * @brief	Deconstructs an object of Pipeline
     *
     * @details	Destroys all objects and allocated memory
     */
	~Pipeline();


    /**
     * @brief      Initialise the pipeline with a DADA header block
     *
     * @param      header  A RawBytes object wrapping the DADA header block
     */
	void init(psrdada_cpp::RawBytes &header_block);


    /**
     * @brief       Process the data in a DADA data buffer
     *
     * @param       data  A RawBytes object wrapping the DADA data block
     *
     * @details     The expected input order is T-F-P-E-T, inner T is the batch_size, outer T is n_samples / batch_size
     *              The expected weight order is F-P-B-E
     *              The output order of voltage data is T-B-F-P-T, inner T is the batch_size, outer T is n_samples / batch_size
     *              The output order of Stokes I data is T-B-F-T, inner T is the batch_size, outer T is n_samples / batch_size
     *              The output order of full Stokes is T-B-F-T-S, inner T is length of the spectrum, outer T is the number of spectrums
     */
    bool operator()(psrdada_cpp::RawBytes &dada_block);

    /**
     * @brief total memory size in bytes required on the GPU
     *
     * @return std::size_t
     */
    std::size_t total_size(){return _total_size;}

    /**
     * @brief returns number of weight updates
     *
     */
    std::size_t weight_updates(){return _weight_buffer->updates();}

private:

    HandlerType &_handler_vol;
    HandlerType &_handler_sti;
	HandlerType &_handler_stf;
	PipelineConfig& _conf;

    // Object to perform beamforming on GPU
    Beamformer<ComputeType>* _beamformer = nullptr;
    // input buffer on the GPU -> raw data is copied and transposed from pinned memory (RawBytes object)
	InputType _input_buffer;
    // Beam weights, updated through shared memory from an external process
	WeightType* _weight_buffer = nullptr;
    // Output GPU buffer containing channelized raw voltage beams -> number of raw voltage beams is limited to MAX_RAW_BEAMS
    VoltageBeam _ovoltage_buffer;
    // Output GPU buffer containing high time resolution Power / Stokes I beams -> rate reduced by 1:nfft
    StokesIBeam _ostokesi_buffer;
    // Output GPU buffer containing high spectral resiltion full Stokes beams -> rate is reduced by OS_ratio * integrateion
    StokesFBeam _ostokesf_buffer;
    // Host to device cuda stream (used for async copys)
	cudaStream_t _h2d_stream;
    // Processing stream
	cudaStream_t _prc_stream;
    // Device to host cuda stream (used for async copys)
	cudaStream_t _d2h_stream;

    // Internal dada block counter
    std::size_t _call_cnt = 0;
    // Total memory size on the GPU
    std::size_t _total_size = 0;
#ifdef DEBUG
  // Time measurement variables (debugging only)
	cudaEvent_t start, stop;
	float ms;
#endif
};


} // namespace beamforming
} // namespace edd_paf

#include "edd_paf/beamformer/src/pipeline.cu"