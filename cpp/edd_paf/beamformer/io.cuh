
/**
 * @file io.cuh
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-03-14
 *
 * @copyright Copyright (c) 2024
 *
 */
#pragma once

#include <functional>
#include <thrust/device_vector.h>
#include <cuda.h>
#include <psrdada_cpp/dada_output_stream.hpp>
#include <psrdada_cpp/cuda_utils.hpp>

#include "edd_paf/beamformer/config.hpp"

namespace edd_paf{
namespace beamformer{


/**
 * @brief Copies the input block.
 *          The copy unbatches the data from T-F-P-E-T -> F-P-E-T
 * @param dst destination pointer
 * @param src source pointer
 * @param conf the pipeline config
 * @param stream the cuda stream
 * @param kind the memory copy kind defaults cudaMemcpyHostToDevice
 */
void copy_input(
    char2* dst,
    char* src,
    PipelineConfig& conf,
    cudaStream_t& stream,
    cudaMemcpyKind kind=cudaMemcpyHostToDevice);


/**
 * @brief Copies and batches the beamformed raw voltage data
 *           batches from B-F-P-T -> T-B-F-P-T
 * @tparam T the type of the pointer
 * @param dst destination pointer
 * @param src source pointer
 * @param conf the pipeline config
 * @param stream the cuda stream
 * @param kind the memory copy kind defaults cudaMemcpyDeviceToHost
 */
template<typename T>
void copy_output_voltage(
    T* dst,
    T* src,
    PipelineConfig& conf,
    cudaStream_t& stream,
    cudaMemcpyKind kind=cudaMemcpyDeviceToHost);

/**
 * @brief Copies the beamformed high temporal resolution Stokes I data
 *          without batching. Heap size is dictated by the number of input samples (N / nfft)
 * @tparam T the type of the pointer
 * @param dst destination pointer
 * @param src source pointer
 * @param conf the pipeline config
 * @param stream the cuda stream
 * @param kind the memory copy kind defaults cudaMemcpyDeviceToHost
 */
template<typename T>
void copy_output_stokesi(
    T* dst,
    T* src,
    PipelineConfig& conf,
    cudaStream_t& stream,
    cudaMemcpyKind kind=cudaMemcpyDeviceToHost);


/**
 * @brief Copies the beamformed high frequency resolution full Stokes data
 *          transposes from B-F-T-F-S to T-B-F-S
 * @tparam T the type of the pointer
 * @param dst destination pointer
 * @param src source pointer
 * @param conf the pipeline config
 * @param stream the cuda stream
 * @param kind the memory copy kind defaults cudaMemcpyDeviceToHost
 */
template<typename T>
void copy_output_stokesf(
    T* dst,
    T* src,
    PipelineConfig& conf,
    cudaStream_t& stream,
    cudaMemcpyKind kind=cudaMemcpyDeviceToHost);



/**
 * @brief Wrapper for copying data
 *    The template DadaOutputStream::operator(RawBytes&, CopyEngine) calls the operator() of this class
 *
 * @tparam T the type of the data pointers
 */
template<typename T>
class CopyBeamformEngine : public psrdada_cpp::AbstractCopyEngine{
public:
    using CopyFunc = std::function<void(T*, T*, PipelineConfig&, cudaStream_t&, cudaMemcpyKind)>;

    /**
     * @brief Construct a new CopyBeamformEngine object
     *
     * @param conf The Pipeline configuration
     * @param stream The stream
     * @param func The function to copy with - must have the signature void(T*, T*, PipelineConfig&, cudaStream_t&, cudaMemcpyKind)
     * @param kind Copy directions defaults to cudaMemcpyDeviceToHost
     */
    CopyBeamformEngine(
        PipelineConfig& conf,
        cudaStream_t& stream,
        CopyFunc func,
        cudaMemcpyKind kind=cudaMemcpyDeviceToHost)
        : _conf(conf), _stream(stream), _func(func), _kind(kind)
    {}

    /**
     * @brief Wrapper around CopyFunc
     *
     * @param dst pointer to the destination
     * @param src pointer to the source
     * @param nbytes number of bytes to copy - here ignored/not used
     */
    void operator()(void* dst, void* src, std::size_t nbytes)
    {
        _func(reinterpret_cast<T*>(dst),
              reinterpret_cast<T*>(src),
              _conf, _stream, _kind);
    }


    /**
     * @brief Empty function - Sync happens in the pipeline, as copies depend on the same stream
     *
     */
    void sync(){return;}
private:
    PipelineConfig _conf;
    cudaStream_t _stream;
    cudaMemcpyKind _kind;
    CopyFunc _func;
};

}
}

#include "edd_paf/beamformer/src/io.cu"