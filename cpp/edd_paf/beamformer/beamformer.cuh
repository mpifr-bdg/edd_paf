/*
* beamformer.cuh
* Author: Niclas Esser <nesser@mpifr-bonn.mpg.de>
* Description:
*  This file consists of a single class (Beamformer<ComputeType>). An object of Beamformer
*  can be used o either perform a Stokes I detection or raw voltage beamforming
*  on a GPU.
*  Both beamforming kernels expect the same dataproduct (linear aligned in device memory)
*    Input:  F-P-T-E
*    Weight: F-P-B-E
*    Output: F-T-B-P (voltage beams)
*    Output: F-T-B   (Stokes I beams)
*/
#pragma once

#include <cuda.h>
#include <cuda_fp16.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <cmath>

#include <psrdada_cpp/common.hpp>

#include "edd_paf/cuda_utils.cuh"

namespace edd_paf{
namespace beamformer{

// Constants for beamform kernels
#define N_WARPS       8
#define WARP_SIZE     32
#define N_THREAD      (N_WARPS * WARP_SIZE)
#define N_MAX_RBEAMS  16
#define TC_K          16
#define TC_M          16
#define TC_N          16
#define N_POL         2
#define SCALE_FACTOR  1
#define N_BEAMS_BLOCK 16
#define FFT32_STAGES  5
#define FFT32_POINTS  32
#define FFT128_POINTS 128
#define INTEGRATE_ITER 4
#define SAMPLES_ITER  128

#define CONFIG_ERROR  3
/**
* @brief 	GPU kernel to perform raw voltage beamforming
*
* @detail Template type T has to be etiher T=float2 or T=__half2
*
* @param	T* idata       pointer to input memory (format: F-P-T-E)
* @param	T* wdata       pointer to beam weight memory (format: F-P-B-E)
* @param	T* odata	     pointer to output memory (format: F-T-B)
* @param	int time	     Width of time dimension (T)
* @param	int elem       Number of elements (E)
* @param	int beam       Number of beams (B)
*/
__global__ void beamform(
  const char2* idata,
  const char2* wdata,
  const float2* twiddle,
  int2* odata_vol,
  float* odata_sti,
  float4* odata_stf,
  const unsigned n_time,
  const unsigned n_elem,
  const unsigned n_beam,
  const unsigned integrate,
  const unsigned os_numerator,
  const unsigned os_denominator);


template<class ComputeType>
class Beamformer{

// Internal typedefintions
private:
  typedef decltype(ComputeType::x) real; // Just necessary for twiddle factors (Datatype of FFT)
  typedef decltype(ComputeType::y) imag; // Just necessary for twiddle factors (Datatype of FFT)

// Public functions
public:

/**
  * @brief constructs an object of Beamformer<ComputeType> (ComputeType=float2)
  *
  * @param sample
  * @param channel          Number of samples to process in on kernel launch (at least 128)
  * @param element          Number of channels to process in on kernel launch (no restrictions)
  * @param beam             Number of elements to process in on kernel launch (no restrictions)
  * @param nfft             Number of beams to process in on kernel launch (at least 16)
  * @param os_numerator     Oversample numerator    -> set to 1 for critical sampled
  * @param os_denominator   Oversample denominator  -> set to 1 for critical sampled
  * @param integration      Samples to be integrated, has to be a multiple of 4
  */
  Beamformer(
    std::size_t sample,
    std::size_t channel,
    std::size_t element,
    std::size_t beam,
    std::size_t nfft,
    std::size_t os_numerator,
    std::size_t os_denominator,
    std::size_t integration);

  /**
  * @brief  deconstructs an object of Beamformer<ComputeType> (ComputeType=float2 or ComputeType=__half2)
  */
  ~Beamformer();

  /**
  * @brief 	Launches voltage beamforming GPU kernel
  *
  * @param	char2* input       pointer to input memory (format: F-P-T-E)
  * @param	char2* weights     pointer to beam weight memory (format: F-P-B-E)
  * @param	int2* output	     pointer to output memory (format: F-T-B-P)
  * @param	float* output	     pointer to output memory (format: F-T-B-P)
  * @param	float4* output	     pointer to output memory (format: F-T-B-P)
  * @param  cudaStream_t of cudaStream_t to allow parallel copy + processing (has to be created and destroyed elsewhere)
  */
  void process(
      const char2* input,
      const char2* weights,
      int2* ovoltage,
      float* ostokesi,
      float4* ostokesf,
      const cudaStream_t& stream);

    /**
  	* @brief	Prints the block and grid layout of a kernel (used for debugging purposes)
  	*/
  	void print_layout();

    /**
  	* @brief	Prints the block and grid layout of a kernel (used for debugging purposes)
  	*/
    int check_config();
// Private functions
private:
    void __twid();
    ComputeType __get_tw(int i, int n);
// Private attributes
private:
  thrust::device_vector<float2> _twiddle;
  dim3 grid;
  dim3 block;
  std::size_t _sample;
  std::size_t _channel;
  std::size_t _element;
  std::size_t _beam;
  std::size_t _nfft;
  std::size_t _stages;
  std::size_t _os_numerator;
  std::size_t _os_denominator;
  std::size_t _integration;
};


} // namespace beamforming
} // namespace edd_paf

#include "edd_paf/beamformer/src/kernels.cu"
#include "edd_paf/beamformer/src/beamformer.cu"