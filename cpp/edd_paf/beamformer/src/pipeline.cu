#ifndef EDD_PAF_BEAMFORMER_PIPELINE_CU
#define EDD_PAF_BEAMFORMER_PIPELINE_CU

namespace edd_paf{
namespace beamformer{


template<class HandlerType, class ComputeType>
Pipeline<HandlerType, ComputeType>::Pipeline(
    PipelineConfig& _conf,
    HandlerType &handler_vol,
    HandlerType &handler_sti,
    HandlerType &handler_stf
):
    _conf(_conf),
    _handler_vol(handler_vol),
    _handler_sti(handler_sti),
    _handler_stf(handler_stf)
{
	// Check if passed template types are valid
	if(!(std::is_same<ComputeType, float2>::value))
	// && !(std::is_same<ComputeType,__half2>::value))
	{
		BOOST_LOG_TRIVIAL(error) << "PipelineError: Template type not supported";
		exit(1);
	}
#ifdef DEBUG
	// Cuda events for profiling
  	CUDA_ERROR_CHECK( cudaEventCreate(&start) );
	CUDA_ERROR_CHECK( cudaEventCreate(&stop) );
#endif
	// Calculate buffer sizes
    std::size_t free, total;
    std::size_t input_size = _conf.input_size();
    std::size_t weight_size = _conf.weight_size();
    std::size_t ovoltage_size = _conf.output_raw_voltage_size();
    std::size_t ostokesi_size = _conf.output_stokes_i_size();
    std::size_t ostokesf_size = _conf.output_full_stokes_size();

	_total_size = (input_size * sizeof(char2)
		+ weight_size 	* sizeof(char2)
		+ ovoltage_size * sizeof(int2)
		+ ostokesi_size * sizeof(float)
		+ ostokesf_size * sizeof(float4)) * 2; // Multiplied by two, since each buffer is double buffer

	BOOST_LOG_TRIVIAL(debug) << "Required memory for unpacked:\t 2x"
        << std::to_string(input_size * sizeof(char2) / (1024)) << "\tkiB";
    BOOST_LOG_TRIVIAL(debug) << "Required memory for weights:\t 2x"
        << std::to_string(weight_size * sizeof(char2) / (1024)) << "\tkiB";
    BOOST_LOG_TRIVIAL(debug) << "Required memory for ovoltage:\t 2x"
        << std::to_string(ovoltage_size * sizeof(int2) / (1024)) << "\tkiB";
    BOOST_LOG_TRIVIAL(debug) << "Required memory for ostokesi:\t 2x"
        << std::to_string(ostokesi_size * sizeof(float) / (1024)) << "\tkiB";
    BOOST_LOG_TRIVIAL(debug) << "Required memory for ostokesf:\t 2x"
        << std::to_string(ostokesf_size * sizeof(float4) / (1024)) << "\tkiB";
	BOOST_LOG_TRIVIAL(info) << "Required total device memory: " << _total_size / 1024 << "\tkiB";

    CUDA_ERROR_CHECK(cudaMemGetInfo(&free, &total));
    if(_total_size > free)
    {
        std::string estr = "Requested " + std::to_string(_total_size / (1024*1024))
            + " MiB memory, but exceeds available memory: " + std::to_string(free / (1024*1024))
            + " MiB -> total: " + std::to_string(total / (1024*1024)) + " MiB";
        throw std::runtime_error(estr);
    }

	// Instantiate buffers
	_weight_buffer = new WeightType(weight_size);
	_input_buffer.resize(input_size, {0x00, 0x00});
	_ovoltage_buffer.resize(ovoltage_size, {0,0});
	_ostokesi_buffer.resize(ostokesi_size, .0);
	_ostokesf_buffer.resize(ostokesf_size, {0, 0, 0, 0});
    _weight_buffer->start(); // Start the IPC to transfer weights


    // Instantiate beamformer object -> the processor wrapped into this pipeline
	_beamformer = new Beamformer<ComputeType>(
        _conf.n_samples,
		_conf.n_channel,
		_conf.n_elements,
		_conf.n_beam,
		_conf.nfft,
		_conf.os_numerator,
		_conf.os_denominator,
		_conf.integration
	);
	if(_beamformer->check_config()){exit(CONFIG_ERROR);}

	// Create streams
	CUDA_ERROR_CHECK(cudaStreamCreate(&_h2d_stream));
	CUDA_ERROR_CHECK(cudaStreamCreate(&_prc_stream));
	CUDA_ERROR_CHECK(cudaStreamCreate(&_d2h_stream));

    // Debug print of the configuration
	_conf.print();
}


template<class HandlerType, class ComputeType>
Pipeline<HandlerType, ComputeType>::~Pipeline()
{

#ifdef DEBUG
    CUDA_ERROR_CHECK( cudaEventDestroy(start) );
    CUDA_ERROR_CHECK( cudaEventDestroy(stop) );
#endif
    if(_h2d_stream)
    {
        CUDA_ERROR_CHECK(cudaStreamDestroy(_h2d_stream));
    }
    if(_prc_stream)
    {
        CUDA_ERROR_CHECK(cudaStreamDestroy(_prc_stream));
    }
    if(_d2h_stream)
    {
        CUDA_ERROR_CHECK(cudaStreamDestroy(_d2h_stream));
    }
}



template<class HandlerType, class ComputeType>
void Pipeline<HandlerType, ComputeType>::init(psrdada_cpp::RawBytes &header_block)
{
    std::size_t bytes = header_block.total_bytes();
    std::size_t sample_clock_start, sample_clock;
    if (ascii_header_get(header_block.ptr(), "SAMPLE_CLOCK", "%lu", &sample_clock) != 1){
        BOOST_LOG_TRIVIAL(warning) << "No or multiple SAMPLE_CLOCK in header stream! Setting SAMPLE_CLOCK to 0";
        sample_clock = 0;
    }
    if (ascii_header_get(header_block.ptr(), "SAMPLE_CLOCK_START", "%lu", &sample_clock_start) != 1){
        BOOST_LOG_TRIVIAL(warning) << "No or multiple SAMPLE_CLOCK_START in header stream! Setting SAMPLE_CLOCK_START to 0";
        sample_clock_start = 0;
    }
    // sample_clock_start += sample_clock * _conf.n_samples; // The first block is ignored
    char* header_vol = new char[bytes];
    char* header_sti = new char[bytes];
    char* header_stf = new char[bytes];

    std::memcpy(header_vol, header_block.ptr(), bytes);
    std::memcpy(header_sti, header_block.ptr(), bytes);
    std::memcpy(header_stf, header_block.ptr(), bytes);

	psrdada_cpp::RawBytes raw_header_vol(header_vol, bytes, bytes);
	psrdada_cpp::RawBytes raw_header_sti(header_sti, bytes, bytes);
	psrdada_cpp::RawBytes raw_header_stf(header_stf, bytes, bytes);

    ascii_header_set(header_vol, "SAMPLE_CLOCK", "%lu", sample_clock);
    ascii_header_set(header_sti, "SAMPLE_CLOCK", "%lu", sample_clock / _conf.nfft);
    ascii_header_set(header_stf, "SAMPLE_CLOCK", "%lu", sample_clock / (_conf.os_ratio() * _conf.integration));

    ascii_header_set(header_vol, "SAMPLE_CLOCK_START", "%lu", sample_clock_start);
    ascii_header_set(header_sti, "SAMPLE_CLOCK_START", "%lu", sample_clock_start / _conf.nfft);
    ascii_header_set(header_stf, "SAMPLE_CLOCK_START", "%lu", sample_clock_start / (_conf.os_ratio() * _conf.integration));

    _handler_vol.init(raw_header_vol);
    _handler_sti.init(raw_header_sti);
    _handler_stf.init(raw_header_stf);
}



template<class HandlerType, class ComputeType>
bool Pipeline<HandlerType, ComputeType>::operator()(psrdada_cpp::RawBytes &dada_input)
{
	_call_cnt += 1;
	BOOST_LOG_TRIVIAL(debug) << "Processing "<< _call_cnt << " dada block..";
	if(dada_input.used_bytes() > _input_buffer.size() * sizeof(char2))
	{
		BOOST_LOG_TRIVIAL(error) << "Unexpected size: "<< dada_input.used_bytes()
            << " byte, expected " << _input_buffer.size() * sizeof(char2) << " byte";
		CUDA_ERROR_CHECK(cudaDeviceSynchronize());
		return true;
	}

#ifdef DEBUG
	CUDA_ERROR_CHECK( cudaEventRecord(start,0) );
#endif

    _input_buffer.swap();

    CUDA_ERROR_CHECK(cudaStreamSynchronize(_h2d_stream));
    copy_input(
        _input_buffer.b_ptr(), dada_input.ptr(),
        _conf, _h2d_stream, cudaMemcpyHostToDevice);
	BOOST_LOG_TRIVIAL(debug) << "Copied dada block from host to device";

    if(_call_cnt == 1){return false;}

    _ovoltage_buffer.swap();
    _ostokesi_buffer.swap();
    _ostokesf_buffer.swap();

    CUDA_ERROR_CHECK(cudaStreamSynchronize(_prc_stream));
	_beamformer->process(
		_input_buffer.a_ptr(),
		_weight_buffer->a_ptr(),
		_ovoltage_buffer.b_ptr(),
		_ostokesi_buffer.b_ptr(),
		_ostokesf_buffer.b_ptr(),
        _prc_stream);

	BOOST_LOG_TRIVIAL(debug) << "Beamformed data";

	if(_call_cnt == 2){return false;}

    CUDA_ERROR_CHECK(cudaStreamSynchronize(_d2h_stream));

	// Wrap output into RawBytes objects here;
	psrdada_cpp::RawBytes raw_ovoltage(static_cast<char*>(
		(void*)_ovoltage_buffer.a_ptr()),
		_ovoltage_buffer.size() * sizeof(int2),
		_ovoltage_buffer.size() * sizeof(int2), true);

    psrdada_cpp::RawBytes raw_ostokesi(static_cast<char*>(
		(void*)_ostokesi_buffer.a_ptr()),
		_ostokesi_buffer.size() * sizeof(float),
		_ostokesi_buffer.size() * sizeof(float), true);

	psrdada_cpp::RawBytes raw_ostokesf(static_cast<char*>(
		(void*)_ostokesf_buffer.a_ptr()),
		_ostokesf_buffer.size() * sizeof(float4),
		_ostokesf_buffer.size() * sizeof(float4), true);

    CopyBeamformEngine<int2> voltage_copy_engine(_conf, _d2h_stream, copy_output_voltage<int2>);
    CopyBeamformEngine<float> stokesi_copy_engine(_conf, _d2h_stream, copy_output_stokesi<float>);
    CopyBeamformEngine<float4> stokesf_copy_engine(_conf, _d2h_stream, copy_output_stokesf<float4>);

    // The first output is ignored, because it is not reliable
	// if(_call_cnt == 3){return false;}

	_handler_vol(raw_ovoltage, voltage_copy_engine);
	_handler_sti(raw_ostokesi, stokesi_copy_engine);
	_handler_stf(raw_ostokesf, stokesf_copy_engine);

	BOOST_LOG_TRIVIAL(debug) << "Processed "<< _call_cnt << " dada block..";

#ifdef DEBUG
	CUDA_ERROR_CHECK(cudaEventRecord(stop, 0));
	CUDA_ERROR_CHECK(cudaEventSynchronize(stop));
	CUDA_ERROR_CHECK(cudaEventElapsedTime(&ms, start, stop));
	BOOST_LOG_TRIVIAL(info) << "Took " << ms << " ms to process stream";
#endif
	return false;
}

} // namespace beamforming
} // namespace edd_paf

#endif