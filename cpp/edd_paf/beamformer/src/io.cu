#ifndef EDD_PAF_BEAMFORMER_IO_CU
#define EDD_PAF_BEAMFORMER_IO_CU

namespace edd_paf{
namespace beamformer{


void copy_input(
    char2* dst,
    char* src,
    PipelineConfig& conf,
    cudaStream_t& stream,
    cudaMemcpyKind kind)
{
    if(conf.batch_size == 0)
    {
        CUDA_ERROR_CHECK(cudaMemcpyAsync(
            static_cast<void*>(dst),
            static_cast<void*>(src),
            conf.input_size() * sizeof(char2),
            kind, stream));
        return;
    }
    std::size_t width = conf.batch_size * sizeof(char2);
    std::size_t height = conf.n_channel * conf.n_pol * conf.n_elements;
    std::size_t dpitch = conf.n_samples * sizeof(char2);
    std::size_t spitch = width;
    for(std::size_t batch = 0; batch < conf.n_samples; batch+=conf.batch_size)
    {
        CUDA_ERROR_CHECK(cudaMemcpy2DAsync(
            static_cast<void*>(dst + batch), dpitch,
            static_cast<void*>(src + batch * height * sizeof(char2)), spitch,
            width, height, kind, stream));
    }
}


template<typename T>
void copy_output_voltage(
    T* dst,
    T* src,
    PipelineConfig& conf,
    cudaStream_t& stream,
    cudaMemcpyKind kind)
{
    if(conf.ovol_batch_size == 0)
    {
        CUDA_ERROR_CHECK(cudaMemcpyAsync(
            static_cast<void*>(dst),
            static_cast<void*>(src),
            conf.output_raw_voltage_size() * sizeof(T),
            kind, stream));
        return;
    }
    std::size_t width = conf.ovol_batch_size * sizeof(T);
    std::size_t height = conf.n_channel * conf.n_pol * conf.raw_beams();
    std::size_t dpitch = width;
    std::size_t spitch = conf.n_samples * sizeof(T);
    for(std::size_t batch = 0; batch < conf.n_samples; batch+=conf.ovol_batch_size)
    {
        CUDA_ERROR_CHECK(cudaMemcpy2DAsync(
            static_cast<void*>(dst + batch  * height), dpitch,
            static_cast<void*>(src + batch), spitch,
            width, height, kind, stream));
    }
}


template<typename T>
void copy_output_stokesi(
    T* dst,
    T* src,
    PipelineConfig& conf,
    cudaStream_t& stream,
    cudaMemcpyKind kind)
{
    CUDA_ERROR_CHECK(cudaMemcpyAsync(
        static_cast<void*>(dst),
        static_cast<void*>(src),
        conf.output_stokes_i_size() * sizeof(T),
        kind, stream));
}


template<typename T>
void copy_output_stokesf(
    T* dst,
    T* src,
    PipelineConfig& conf,
    cudaStream_t& stream,
    cudaMemcpyKind kind)
{
    CUDA_ERROR_CHECK(cudaMemcpyAsync(
        static_cast<void*>(dst),
        static_cast<void*>(src),
        conf.output_full_stokes_size() * sizeof(float4),
        kind, stream));
}

}
}

#endif