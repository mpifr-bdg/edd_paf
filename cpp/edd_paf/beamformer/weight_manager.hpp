#pragma once

#include <vector>
#include <string>
#include <unistd.h>
#include <random>
#include <cmath>
#include <complex>
#include <fstream>
#include <iostream>

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>

#include <sw/redis++/redis++.h>

#include <psrdada_cpp/common.hpp>

#include "edd_paf/beamformer/queue_header.hpp"

namespace psr = psrdada_cpp;
namespace red = sw::redis;
namespace bip = boost::interprocess;


namespace edd_paf {
namespace beamformer {


struct WeightManagerConfig{
   std::size_t n_channel;
   std::size_t n_elements;
   std::size_t n_beam;
   std::string host;
   std::size_t port;
   int scale;
   std::string redis_channel;
   const std::size_t n_pol = 2;
   void print()
   {
     std::cout << "Pipeline interface configuration" << std::endl;
     std::cout << "n_channel: " << n_channel << std::endl;
     std::cout << "n_elements: " << n_elements << std::endl;
     std::cout << "n_pol: " << n_pol << std::endl;
     std::cout << "n_beam: " << n_beam << std::endl;
   }
};


template<class ComputeType, class InputType>
class WeightManager
{
public:
  WeightManager(WeightManagerConfig& config, psr::MultiLog& logger, red::Subscriber& subscriber);
  ~WeightManager();
  void start();

private:
  void parse_weights(std::string channel, std::string msg);
  void convert();

private:
  WeightManagerConfig& conf;
  psr::MultiLog& log;
  red::Subscriber& sub;

  bip::shared_memory_object smem;
  bip::mapped_region region;
  void* smem_addr = nullptr;
  ComputeType* smem_weights = nullptr;
  QueueHeader *qheader;

  std::string smem_name = "SharedMemoryWeights";
  red::StringView sub_channel;

  std::vector<ComputeType> vect_weights;
  std::vector<char> raw_weights;
  std::size_t expected_bytes;
  std::size_t update_cnt = 0;
  bool updated = false;
  bool quit = false;
};

} //namespace beamforming
} //namespace edd_paf

#include "edd_paf/beamformer/src/weight_manager.cpp"
