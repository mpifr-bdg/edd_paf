#pragma once

#include <vector>
#include <string>
#include <thread>

#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/cuda_utils.hpp>
#include <psrdada_cpp/double_device_buffer.cuh>
#include <psrdada_cpp/double_host_buffer.cuh>

#include "edd_paf/cuda_utils.cuh"

namespace edd_paf{

namespace bip = boost::interprocess;

/**
* @brief    Header stored in POSIX shared memory to allow IPC communication
*/
struct QueueHeader
{
    QueueHeader() : data_in(false), stop(false){}
    // Mutex to protect access to the queue
    bip::interprocess_mutex mutex;
    // Condition to wait when the queue is empty
    bip::interprocess_condition ready_to_read;
    // Condition to wait when the queue is full
    bip::interprocess_condition ready_to_write;
    // Is there any payload?
    bool data_in;
    // Stop flag, will force the owner to remove the shared memory
    bool stop;
};

/**
 * @brief InterprocessMemory provides an POSIX shared memory interface
 * @details It uses a QueueHeader object to allow the interprocess communication in memory
 *
 * @tparam T: The type of the data in the memory
 */
template<class T>
class InterprocessMemory
{
public:
    /**
     * @brief Construct a new Interprocess Memory object
     *
     * @param size the size of the vector in memory. Actual bytesize of the memory is size * sizeof(T) + sizeof(QueueHeader)
     * @param shared_key The shared memory key to open or create
     * @param force_Creation If true, forces the creation of the shared memory
     */
    InterprocessMemory(std::size_t size, std::string shared_key="SharedMemoryWeights", bool force_creation=false);

    /**
     * @brief Destroy the Interprocess Memory object
     *
     */
    ~InterprocessMemory();

    /**
     * @brief Starts a thread which writes to the shared memory
     * @details Data is transferred whenever the set_data()-method was called.
     *          Note that only one thread per object can run.
     */
    void writing();


    /**
     * @brief Starts a thread which reads from the shared memory
     * @details Acces to the read data is done with the get_data() method.
     *          Note that only one thread per object can run.
     */
    void reading();

    /**
     * @brief Stops the running thread and notifys the opponent thread to stop, too.
     *
     */
    void stop();

    /**
     * @brief Set the data which should get written to the shared memory
     *
     * @param vec The vector / data to write to
     */
    void set_data(std::vector<T> vec);

    /**
     * @brief Get the data from the shared memory.
     *
     * @return std::vector<T>
     */
    std::vector<T> get_data(){return _data;};

    /**
     * @brief Number of registered updates
     *
     * @return std::size_t number of updates
     */
    std::size_t updates(){return _update_cnt.load();}

    /**
     * @brief Returns true if the thread is running, false otherwise
     *
     * @return true
     * @return false
     */
    bool is_active(){return _active.load();}

private:
    void run_writing();
    void run_reading();
    QueueHeader* qheader;
    bip::shared_memory_object _smem;
    bip::mapped_region region;
    std::thread* _thread = nullptr;
    std::string _smem_name;
    std::size_t _bytes;
    std::vector<T> _data;
    std::atomic<unsigned int>  _update_cnt = 0;
    std::atomic<bool> _active = false;
    std::atomic<bool> _updated = false;
    bool _owner = false;
    void* smem_addr = nullptr;
    T* _smem_data = nullptr;
};


/**
* @brief  Class providing an interface to update GPU buffers through a POSIX based shared memory
*/
template<class T>
class SharedPipelineBuffer : public psrdada_cpp::DoubleBuffer<thrust::device_vector<T>>
{
public:
    typedef T type;
public:

    /**
    * @brief	Instantiates an object of PipelineBuffer
    *
    * @param	std::size_t  Number of items in buffer
    * @param	std::string  Name of the POSIX shared memory
    *
    * @details Allocates twice the size in device memory as double device buffer.
    *         It also launches a boost::thread to create, read and write from shared
    *         memory.
    */
    SharedPipelineBuffer(std::size_t size, std::string smem_name="SharedMemoryWeights");

    /**
    * @brief	Destroys an object of PipelineBuffer
    */
    ~SharedPipelineBuffer();

    /**
    * @brief	Creates, read, write and removes a POSIX shared memory space
    *
    * @detail This function is a temporary solution to update beam weights on-the-fly
    *         while the pipeline is operating. In the future a clean interface will be created
    *         that provides addtional monitoring informations (e.g. power level) besides the
    *         beam weight updating mechanism.
    */
    void start();

    void run();

    void stop();

    bool is_active(){return _active;}
    std::size_t updates(){return _update_cnt;}

private:
    std::size_t _bytes;
    std::size_t _update_cnt = 0;
    std::thread *_handle_thread;
    InterprocessMemory<T>* _reader = nullptr;
    bool _active = false;
};

} // namespace edd_paf

#include "edd_paf/src/memory.cu"