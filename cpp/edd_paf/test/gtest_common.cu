#include <gtest/gtest.h>
#include <edd_paf/test/test_memory.cuh>

int main(int argc, char **argv) {
 ::testing::InitGoogleTest(&argc, argv);

 return RUN_ALL_TESTS();
}
