#pragma once
#include <gtest/gtest.h>
#include <vector>
#include <complex>

#include <psrdada_cpp/testing_tools/tools.cuh>

#include "edd_paf/memory.cuh"


namespace psr_testing = psrdada_cpp::testing_tools;
namespace edd_paf {
namespace beamformer {
namespace test {

TEST(test_start_stop, start_stop)
{
    InterprocessMemory<char> dummy(1000, "dummy_shared_memory");
    dummy.writing();
    ASSERT_TRUE(dummy.is_active());
    dummy.stop();
    ASSERT_FALSE(dummy.is_active());
    dummy.writing();
    ASSERT_TRUE(dummy.is_active());
    dummy.stop();
    ASSERT_FALSE(dummy.is_active());
}

TEST(test_fail_multiple_starts, fail_multiple_starts)
{
    InterprocessMemory<char> dummy(1000, "dummy_shared_memory");
    dummy.writing();
    ASSERT_TRUE(dummy.is_active());
    EXPECT_THROW(dummy.writing(), std::runtime_error);
}

TEST(test_data_readable_before_reader_started, data_readable_before_reader_started)
{
    std::size_t size = 1000;
    InterprocessMemory<float> writer(size, "dummy_shared_memory");
    InterprocessMemory<float> reader(size, "dummy_shared_memory");
    thrust::host_vector<float> hvec = psr_testing::random_vector<float>(0, 64, size);
    std::vector<float> expect_vec(hvec.begin(), hvec.end());
    writer.writing();
    writer.set_data(expect_vec);
    reader.reading();
    while(reader.updates() == 0){usleep(1000);}
    for(std::size_t i = 0; i < size; i+=1)
    {
        ASSERT_EQ(writer.get_data()[i], reader.get_data()[i]);
    }
}

template<typename T>
void test_data_transfer(std::size_t size)
{
    std::string smem_key = "dummy_shared_memory";
    InterprocessMemory<T> writer(size, smem_key);
    InterprocessMemory<T> reader(size, smem_key);
    writer.writing();
    reader.reading();
    for(std::size_t updates = 0; updates < 16; updates++)
    {
        thrust::host_vector<T> hvec = psr_testing::random_vector<T>(0, 64, size);
        std::vector<T> expect_vec(hvec.begin(), hvec.end());
        writer.set_data(expect_vec);
        // Wait for the buffer to update
        while(reader.updates() == updates)
        {
            usleep(10000);
        }
        std::vector<T> actual_vec = reader.get_data();
        // Proof actual is equal to expected
        for(std::size_t i = 0; i < actual_vec.size(); i+=1)
        {
            if constexpr (psr_testing::is_complex<T>::value){
                ASSERT_EQ(actual_vec[i].real(), expect_vec[i].real());
                ASSERT_EQ(actual_vec[i].imag(), expect_vec[i].imag());
            }else{
                ASSERT_EQ(actual_vec[i], expect_vec[i]);
            }
        }
    }
    writer.stop();
}

TEST(test_data_transfer_char, test_data_transfer)
{
    test_data_transfer<std::complex<float>>(1337);
}
TEST(test_data_transfer_int2, test_data_transfer)
{
    test_data_transfer<std::complex<int>>(7777);
}
TEST(test_data_transfer_int, test_data_transfer)
{
    test_data_transfer<int>(7777);
}
TEST(test_data_transfer_float, test_data_transfer)
{
    test_data_transfer<float>(13370);
}


template<typename T>
void test_data_transfer_h2d_d2h(std::size_t size)
{
    std::string smem_key = "dummy_shared_memory";
    InterprocessMemory<T> writer(size, smem_key);
    SharedPipelineBuffer<T> buffer(size, smem_key);
    writer.writing();
    buffer.start();
    for(std::size_t updates = 0; updates < 16; updates++)
    {
        thrust::host_vector<T> hvec = psr_testing::random_vector<T>(0, 64, size);
        std::vector<T> expect_vec(hvec.begin(), hvec.end());
        writer.set_data(expect_vec);
        // Wait for the buffer to update
        while(buffer.updates() == updates)
        {
            usleep(1000);
        }
        // Transfer data form GPU back to Host
        thrust::host_vector<T> actual_vec = buffer.a();
        // Proof actual is equal to expected
        for(std::size_t i = 0; i < actual_vec.size(); i+=1)
        {
            if constexpr (psr_testing::is_complex<T>::value){
                ASSERT_EQ(actual_vec[i].real(), expect_vec[i].real());
                ASSERT_EQ(actual_vec[i].imag(), expect_vec[i].imag());
            }else{
                ASSERT_EQ(actual_vec[i], expect_vec[i]);
            }
        }
    }
    writer.stop();
    buffer.stop();
}

TEST(test_data_transfer_h2d_d2h_float, test_data_transfer_h2d_d2h)
{
    test_data_transfer_h2d_d2h<float>(13370);
}

}
}
}