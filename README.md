# The EDD Phased Array Feed (PAF) plugin
The EDD (Effelsberg Direct Digitization) is a modular concept for radioastronomy systems, including receivers, time distribution, digitization, packetization and data processing in the backend.

This repository is a plugin for the EDD backend software stack allowing to operate common PAF backend components

## Requirements
- CUDA 11.0 or later
- C++ libs: psrdada, psrdada_cpp, boost
- python3: mpikat and all it's dependencies

The plugin provides a Dockerfile in `roles/edd_paf/templates/`. The built Docker image resolves all dependencies and requirements

# Contact
Author: Niclas Esser - <nesser@mpifr-bonn.mpg.de>
