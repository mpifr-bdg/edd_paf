import numpy as np
import subprocess
import pandas as pd
import argparse
import os
from argparse import RawTextHelpFormatter

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
    parser.add_argument('--begin', action='store', default=128, dest="start", help="Number of samples to begin profiling ()")
    parser.add_argument('--stop','-s', action='store', default=8192, dest="stop", help="Number of samples to stop profiling")
    parser.add_argument('--elements','-e', action='store', default=128, dest="elements", help="Number of samples to stop profiling")
    parser.add_argument('--beams','-b', action='store', default=128, dest="beams", help="Number of samples to stop profiling")
    parser.add_argument('--channels','-c', action='store', default=16, dest="channels", help="Number of samples to stop profiling")
    parser.add_argument('--integration','-i', action='store', default=4, dest="inte", help="Number of spectras to be integrated")
    parser.add_argument('--runs','-r', action='store', default=5, dest="runs", help="Number of runs per configuration")
    parser.add_argument('--device','-d', action='store', default=0, dest="device", help="CUDA device id of")
    parser.add_argument('--plot','-p', action='store', default=False, dest="plot", help="Plots the results")
    parser.add_argument('--dir', action='store', default="/tmp/results/", dest="dir", help="Directory to store results")
    parser.add_argument('--exec', action='store', default="beamform_profiling", dest="exec", help="Executable file './profile'")

    start = int(parser.parse_args().start)
    stop = int(parser.parse_args().stop)
    elem = int(parser.parse_args().elements)
    beam = int(parser.parse_args().beams)
    chan = int(parser.parse_args().channels)
    inte = int(parser.parse_args().inte)
    runs = int(parser.parse_args().runs)
    device = int(parser.parse_args().device)
    plot = int(parser.parse_args().plot)
    dir = parser.parse_args().dir

    if not os.path.exists(dir):
        os.mkdir(dir)

    sizes = np.arange(start, stop, 128)
    bw_ch = 2e9 / 1024 * 32/27
    # elem = 256
    # beam = 256
    # chan = 16
    t_p_s = sizes * 1/(bw_ch)
    ih_bw = np.full(len(sizes), bw_ch * 2 * chan * elem * 2)/1e9
    ii_bw = np.full(len(sizes), bw_ch * 2 * chan * elem)/1e9

    # Profile kernels in subprocess
    exe = parser.parse_args().exec
    exe += " -r " + str(runs)
    exe += " -e " + str(elem)
    exe += " -b " + str(beam)
    exe += " -c " + str(chan)
    exe += " -i " + str(inte)
    exe += " --outdir " + dir
    for size in sizes:
        print(exe + " -s " + str(size))
        subprocess.call(exe + " -s " + str(size), shell=True)


    files = [f for f in os.listdir(dir) if '.csv' in f]
    if len(files) == 0:
        print("No csv-files found in " + dir)
        exit()
    else:
        print("Found files: " + str(files))
    avg_time = np.zeros((len(files), len(sizes)))
    avg_throughput = np.zeros((len(files), len(sizes)))
    avg_bandwidth = np.zeros((len(files), len(sizes)))
    kname = []
    for i, f in enumerate(files):
        table = pd.read_csv(dir + f, sep=",", engine='python')
        avg_time[i] = table['avg_time'].to_numpy()[:len(sizes)]
        avg_throughput[i] = table['avg_throughput'].to_numpy()[:len(sizes)]
        avg_bandwidth[i] = table['input_avg_bandwidth'].to_numpy()[:len(sizes)]
        dev_name = table["devicename"].to_numpy()[0]
        kname.append(table["kernelname"].to_numpy()[0])

    # Plot
    import matplotlib
    if not plot:
        matplotlib.use('Agg')
    import matplotlib.pyplot as plot

    fig = plot.figure(figsize=(18,12))
    for i in range(len(files)):
        ax1 = plot.subplot(131)
        ax1.plot(sizes, avg_time[i], label=files[i].split('.')[0])
        if i == len(files)-1:
            ax1.plot(sizes, t_p_s*1000, label="Required", linestyle='-.')
        ax1.set_yscale('log')
        ax1.set_title("Average time")
        ax1.set_ylabel("Time [ms]")
        ax1.set_xlabel("N -> [Samples] \nElements = {}\nBeams={}\nChannels={}".format(elem,beam,chan))
        ax1.legend()

        ax2 = plot.subplot(132)
        # if i == 0:
        #     ax2.plot(sizes/chan, 7*elem*beam*sizes/(t_p_s*1e9), label="Required", linestyle='-.')
        ax2.plot(sizes, avg_throughput[i])
        ax2.set_title("Theoretical Throughput")
        ax2.set_ylabel("Throughput [GOPS]")
        ax2.set_xlabel("N -> [Samples] \nElements: 2x{}\nBeams: 2x{}\nChannels: {}".format(elem,beam,chan))

        ax3 = plot.subplot(133)
        # if i == 0:
        #     ax3.plot(sizes/chan, ih_bw, label="Required", linestyle='-.')
        ax3.plot(sizes, avg_bandwidth[i])
        ax3.set_title("Input Bandwidth")
        ax3.set_ylabel("Bandwidth [GB/s]")
        ax3.set_xlabel("N -> [Samples] \nElements: 2x{}\nBeams: 2x{}\nChannels: {}".format(elem,beam,chan))

    fig.suptitle("GPU Beamform Benchmarks on {} (runs = {})".format(dev_name, runs))
    plot.savefig(dir + dev_name + ".png")
    if plot:
        plot.show()
