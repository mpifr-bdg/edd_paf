"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:

The acm_file.py implements the ACMFile class to store array covariance matrices (ACMs)
in HDF5 format. ACMs are the outcome of the correlator pipeline.
"""
import h5py
import numpy as np
from mpikat.core import logger


_LOG = logger.getLogger("mpikat.eddpaf.acm_file")


# pylint: disable=E1101
class ACMFile(h5py.File):
    """_summary_

    Args:
        h5py (_type_): _description_
    """
    def __init__(self, fname, mode="w"):
        """_summary_

        Args:
            fname (_type_): _description_
            mode (str, optional): _description_. Defaults to "w".
        """
        super().__init__(fname, mode)
        self.prefix = "ACM"
        self.attrs = {}

    def create(self):
        """_summary_
        """
        self.attrs['n_samples'] = 0
        self.attrs['azimuth_start'] = 0
        self.attrs['azimuth_stop'] = 0
        self.attrs['elevation_start'] = 0
        self.attrs['elevation_stop'] = 0
        self.attrs['dec_start'] = 0
        self.attrs['dec_stop'] = 0
        self.attrs['ra_start'] = 0
        self.attrs['ra_stop'] = 0
        self.attrs['on_source'] = False
        # self.create_dataset('ACMdata', (N_PFB_CHANNELS, N_ELEMENTS, N_ELEMENTS), dtype=np.complex64)
        # self.create_dataset('ACMstatus', (N_PFB_CHANNELS, N_ELEMENTS), dtype='int32')
        # self.create_dataset('skyFrequency', (N_PFB_CHANNELS,), dtype='double')

    def write_dataset(self, name: str, array: np.ndarray):
        """_summary_

        Args:
            name (_type_): _description_
            array (_type_): _description_
        """
        _LOG.info("Wrinting dataset %s %s", name, array)

    def write_attribtue(self, name: str, value: any):
        """_summary_

        Args:
            name (_type_): _description_
            value (_type_): _description_
        """
        _LOG.info("Wrinting attribute %s with value %s", name, value)
