"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:
The corr_stream.py file defines the correlator data stream
"""
import tempfile
import numpy as np
from mpikat.core import logger
from mpikat.core.data_stream import DataStream, edd_spead

_LOG = logger.getLogger("mpikat.eddpaf.data_streams.corr_stream")


class CorrelatorDataStreamFormat(DataStream):
    """
    Format description of the CorrelatorDataStreamFormat
    """
    samples_per_heap = 4096
    data_type = np.dtype([
        ('real', np.int32),
        ('imag', np.int32)
    ])
    itemsize = data_type.itemsize

    data_items = [
        edd_spead(5632, "timestamp"),
        edd_spead(5633, "channel"),
        edd_spead(5634, "data", "Payload data of spead heap", (samples_per_heap,), data_type),
    ]

    stream_meta = {
        "type":"CorrelatorDataStreamFormat:1",
        "description":"The CorrelatorDataStreamFormat stream 1",
        "ip":"239.0.255.1",
        "nic":"127.0.0.1",
        "port":8125,
        "name": "default",
        "sync_time":0,
        "sample_rate":2e6,
        "center_freq":1e6,
        "samples_per_heap":samples_per_heap,
        "channel_id": 0
    }


class CorrelatorSpeadHandler():
    """
    Class for handling spead heaps in the DigitalDownConverter:1 format
    """
    def __init__(self):
        """
        Constructor
        """
        self.format = CorrelatorDataStreamFormat()
        self.item_group = self.format.create_item_group()

    def __call__(self, heap):
        """
        Callback function
        """
        return self.item_group.update(heap)

def corr_mksend_header(desc: dict) -> str:
    """_summary_

    Args:
        desc (dict): _description_

    Returns:
        str: _description_
    """
    tmp = tempfile.NamedTemporaryFile()
    with open(tmp.name, "wb") as f:
        for key, val in desc.items():
            f.write(f"{key.upper()} {val}")
    return tmp.name

def corr_mksend_cmd(desc: dict) -> str:
    """Create the mksend command

    Args:
        desc (dict): stream description

    Returns:
        str: the command to run mksend
    """
    fname = corr_mksend_cmd(desc)
    return f"mksend --header {fname}"
