"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:

The data_streams module provides data stream descriptions which are registered EDD
DataStreamRegistry.
"""

from mpikat.core.data_stream import DataStreamRegistry
# pylint: disable=C0301
from eddpaf.data_streams.beam_stream import VoltageBeamDataStreamFormat, FullStokesBeamDataStreamFormat, PowerBeamDataStreamFormat
from eddpaf.data_streams.chan_stream import ChannelizerDataStreamFormat, channelizer_mkrecv_cmd, channelizer_mkrecv_header
from eddpaf.data_streams.corr_stream import CorrelatorDataStreamFormat, CorrelatorSpeadHandler, corr_mksend_cmd
# pylint: enable=C0301
__all__ = [
    "ChannelizerDataStreamFormat",
    "VoltageBeamDataStreamFormat",
    "FullStokesBeamDataStreamFormat",
    "PowerBeamDataStreamFormat",
    "CorrelatorDataStreamFormat",
    "CorrelatorSpeadHandler",
    "channelizer_mkrecv_cmd",
    "channelizer_mkrecv_header",
    "corr_mksend_cmd"
]

DataStreamRegistry.register(ChannelizerDataStreamFormat)
DataStreamRegistry.register(VoltageBeamDataStreamFormat)
DataStreamRegistry.register(FullStokesBeamDataStreamFormat)
DataStreamRegistry.register(PowerBeamDataStreamFormat)
DataStreamRegistry.register(CorrelatorDataStreamFormat)
