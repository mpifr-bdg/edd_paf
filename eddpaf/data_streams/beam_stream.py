"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:
The beam_stream.py file defines the three different beam data stream
"""
import numpy as np
from mpikat.core import logger
from mpikat.core.data_stream import DataStream, edd_spead

_LOG = logger.getLogger("mpikat.eddpaf.data_streams.beam_stream")


class VoltageBeamDataStreamFormat(DataStream):
    """
    Format description of the VoltageBeamDataStreamFormat
    """
    samples_per_heap = 2048
    data_type = np.dtype([
        ('real', np.int32),
        ('imag', np.int32)
    ])
    data_items = [
        edd_spead(5632, "timestamp"),
        edd_spead(5633, "channel"),
        edd_spead(5634, "polarization"),
        edd_spead(5635, "beam"),
        edd_spead(5636, "data", "Payload data of spead heap", (samples_per_heap,), data_type),
    ]

#pylint: disable=R0801
    stream_meta = {
        "type":"VoltageBeamDataStreamFormat:1",
        "description":"The VoltageBeamDataStreamFormat stream 1",
        "ip":"239.0.255.1",
        "nic":"127.0.0.1",
        "port":8125,
        "name": "default",
        "sync_time":0,
        "sample_rate":2e6,
        "center_freq":1e6,
        "samples_per_heap":samples_per_heap,
        "polarization":0,
        "channel_id": 0,
        "bit_depth":data_type.itemsize*8
    }
#pylint: enable=R0801
    @property
    def name(self) -> str:
        """Name of the VoltageBeamDataStreamFormat

        Returns:
            str: always returns 'VoltageBeamDataStreamFormat'
        """
        return self.stream_meta["type"]
    @property
    def nbytes(self) -> int:
        """Number of payload bytes per heap

        Returns:
            int: The number of bytes
        """
        return self.stream_meta["samples_per_heap"] * self.data_type.itemsize

class FullStokesBeamDataStreamFormat(DataStream):
    """
    Format description of the FullStokesBeamDataStreamFormat
    """
    samples_per_heap = 2048
    data_type = np.dtype([
        ('i', np.float32),
        ('q', np.float32),
        ('u', np.float32),
        ('v', np.float32)
    ])
    data_items = [
        edd_spead(5632, "timestamp"),
        edd_spead(5633, "channel"),
        edd_spead(5634, "stokes"),
        edd_spead(5635, "beam"),
        edd_spead(5636, "data", "Payload data of spead heap", (samples_per_heap,), data_type),
    ]

    stream_meta = {
        "type":"FullStokesBeamDataStreamFormat:1",
        "description":"The FullStokesBeamDataStreamFormat stream 1",
        "ip":"239.0.255.1",
        "nic":"127.0.0.1",
        "port":8125,
        "name": "default",
        "sync_time":0,
        "sample_rate":2e6,
        "center_freq":1e6,
        "samples_per_heap":samples_per_heap,
        "polarization":0,
        "channel_id": 0,
        "bit_depth":np.dtype(data_type).itemsize*8
    }

    @property
    def name(self) -> str:
        """Name of the FullStokesBeamDataStreamFormat

        Returns:
            str: always returns 'FullStokesBeamDataStreamFormat'
        """
        return self.stream_meta["type"]

    @property
    def nbytes(self) -> int:
        """Number of payload bytes per heap

        Returns:
            int: The number of bytes
        """
        return self.stream_meta["samples_per_heap"] * self.stream_meta["bit_depth"] / 8

class PowerBeamDataStreamFormat(DataStream):
    """
    Format description of the PowerBeamDataStreamFormat
    """
    samples_per_heap = 2048
    data_type = np.float32
    data_items = [
        edd_spead(5632, "timestamp"),
        edd_spead(5633, "channel"),
        edd_spead(5634, "beam"),
        edd_spead(5635, "data", "Payload data of spead heap", (samples_per_heap,), data_type),
    ]

    stream_meta = {
        "type":"PowerBeamDataStreamFormat:1",
        "description":"The PowerBeamDataStreamFormat stream 1",
        "ip":"239.0.255.1",
        "nic":"127.0.0.1",
        "port":8125,
        "name": "default",
        "sync_time":0,
        "sample_rate":2e6,
        "center_freq":1e6,
        "samples_per_heap":samples_per_heap,
        "polarization":0,
        "channel_id": 0,
        "bit_depth":np.dtype(data_type).itemsize*8
    }

    @property
    def name(self) -> str:
        """Name of the PowerBeamDataStreamFormat

        Returns:
            str: always returns 'PowerBeamDataStreamFormat'
        """
        return self.stream_meta["type"]

    @property
    def nbytes(self) -> int:
        """Number of payload bytes per heap

        Returns:
            int: The number of bytes
        """
        return self.stream_meta["samples_per_heap"] * self.stream_meta["bit_depth"] / 8
