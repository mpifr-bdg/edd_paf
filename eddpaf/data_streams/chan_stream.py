"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:
The chan_stream.py file defines the channelizer data stream
"""
import tempfile
import numpy as np
from mpikat.core import logger
from mpikat.core.data_stream import DataStream, edd_spead

_LOG = logger.getLogger("mpikat.eddpaf.data_streams.chan_stream")


class ChannelizerDataStreamFormat(DataStream):
    """
    Format description of the ChannelizerDataStreamFormat
    """
    samples_per_heap = 16384
    data_type = np.dtype([
        ('real', np.int8),
        ('imag', np.int8)
    ])
    itemsize = data_type.itemsize
    nbytes = samples_per_heap * itemsize
    channels = 1

    data_items = [
        edd_spead(5632, "timestamp"),
        edd_spead(5633, "order_vector"),
        edd_spead(5634, "clipping_cnt"),
        edd_spead(5635, "scale_factor"),
        edd_spead(5636, "reserved"),
        edd_spead(5637, "data", "Payload data of spead heap", (samples_per_heap,), data_type),
    ]

    stream_meta = {
        "type":"ChannelizerDataStreamFormat:1",
        "description":"The ChannelizerDataStreamFormat stream 1",
        "ip":"225.0.0.0",
        "nic":"127.0.0.1",
        "port":7148,
        "name": "default",
        "sync_time":0,
        "sample_rate": 3.125e6 * data_type.itemsize,
        "center_freq":1e6,
        "samples_per_heap":samples_per_heap,
        "channel_id": 0,
        "bit_depth": data_type.itemsize * 8
    }
    @property
    def name(self):
        """
        The name of the stream
        """
        return self.stream_meta["type"]

_mkrecv_header = """
HEADER DADA
HDR_VERSION 1.0
HDR_SIZE 4096
NBIT 16
DADA_VERSION 1.0
SAMPLE_CLOCK_START unset
PACKET_SIZE 8400
IBV_VECTOR -1
IBV_MAX_POLL 10
BUFFER_SIZE 16777216
SAMPLE_CLOCK {sample_rate}
DADA_NSLOTS 256
SLOTS_SKIP 0
NINDICES 3              # Number of ALL items (indexer, lists etc.) except the payload
NHEAPS 128
SCI_LIST unset          # Important: If there are no SCIs it must be 'unset' or NOT specified


IDX1_ITEM 0             # Time index
IDX1_STEP 16384
IDX1_INDEX 1

IDX2_ITEM 1             # Channel index
IDX2_LIST {channel_list}
IDX2_INDEX 3            # Third dimension
IDX2_MASK 0xffff0000

IDX3_ITEM 1             # Element index
IDX3_LIST {element_start}:{element_stop}:{element_step}
IDX3_INDEX 2
IDX3_MASK 0x0000ffff
"""

def channelizer_mkrecv_header(desc: dict) -> str:
    """_summary_

    Args:
        desc (dict): _description_

    Returns:
        str: _description_
    """
    with tempfile.NamedTemporaryFile(delete=False, mode="w") as f:
        _LOG.debug("Creating mksend header: %s", f.name)
        f.write(_mkrecv_header.format(**desc))
        name = f.name
    return name

def channelizer_mkrecv_cmd(desc: dict) -> str:
    """_summary_

    Args:
        desc (dict): _description_

    Returns:
        str: _description_
    """
    header_file = channelizer_mkrecv_header(desc)
    if "idx1_modulo" not in desc:
        idx1_modulo = 0 # np.lcm(ChannelizerDataStreamFormat.samples_per_heap,
            #int(desc["sample_rate"]))
    else:
        idx1_modulo = desc["idx1_modulo"]
    cmd = []
    if "physcpu" in desc:
        cmd.append(f'taskset -c {desc["physcpu"]} ')
    cmd.append('mkrecv_v4 --quiet')
    cmd.append(f'--header {header_file}')
    cmd.append(f'--mst {desc["mst"]}')
    cmd.append(f'--heap-size {ChannelizerDataStreamFormat.samples_per_heap\
        * ChannelizerDataStreamFormat.itemsize}')
    cmd.append(f'--idx1-step {ChannelizerDataStreamFormat.samples_per_heap}')
    cmd.append(f'--idx1-modulo {idx1_modulo}')
    cmd.append(f'--dada-key {desc["key"]}')
    cmd.append(f'--sync-epoch {desc["sync_time"]}')
    cmd.append(f'--sample-clock {desc["sample_rate"]}')
    cmd.append(f'--ibv-if {desc["nic"]}')
    cmd.append(f'--port {desc["port"]}')
    cmd.append(f'{desc["ip"]}')
    return ' '.join(cmd)
