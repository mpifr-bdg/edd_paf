"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:

The eddpaf module provides a set of sub-modules to process data streams of an EDD compatible
PAF receiver. The module implements real-time processing pipelines for beamforming, correlation,
weight generation, calibration and backend infrastructure.
"""
import datetime
import pynvml

def folder_string(timestamp: float, prefix: str="/mnt", suffix: str="") -> str:
    """_summary_

    Args:
        timestamp (float): _description_
        prefix (str, optional): _description_. Defaults to "/mnt".
        suffix (str, optional): _description_. Defaults to "".

    Returns:
        str: _description_
    """
    time = datetime.datetime.fromtimestamp(timestamp)
    return f"{prefix}/{time.year}/{time.month}/{time.day}/{time.hour}/{time.minute}/{suffix}"

def sanitize_dictionary(conf: dict, default: dict) -> dict:
    """sanitizes a dictionary, meaning that param conf is filled with missing
    keys from param "default"

    Args:
        conf (dict): actual dictionary
        default (dict): dictionary with desired keys and default values

    Returns:
        dict: sanitzied dictionary
    """

    if not isinstance(conf, dict):
        conf = {}
    for key, val in default.items():
        if key not in conf:
            conf[key] = val
    return conf


def viable_cuda_devices(devices: list, major: int, minor: int) -> list:
    """_summary_

    Args:
        devices (list): _description_
        major (int): _description_
        minor (int): _description_

    Returns:
        list: _description_
    """
    pynvml.nvmlInit()
    viable: list = []
    for dev in devices:
        handle = pynvml.nvmlDeviceGetHandleByIndex(int(dev))
        maj, mi = pynvml.nvmlDeviceGetCudaComputeCapability(handle)
        if (major == maj and minor >= mi) or major < maj:
            viable.append(int(dev))
    return viable
