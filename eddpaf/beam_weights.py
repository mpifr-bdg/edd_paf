"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:

The beam_weights.py implements the BeamWeightFile class to store beam weights
in HDF5 format. Beam weights are used to form beams with the beamform pipeline.
"""
import h5py
import numpy as np
# import matplotlib.pyplot as mplt

from mpikat.core import logger

DEFAULT_DTYPE = np.dtype([('real', np.int16), ('imag', np.int16)])

_LOG = logger.getLogger("mpika.eddpaf.beam_weights")

# pylint: disable=E1101
class BeamWeightFile(h5py.File):

    """
    Class for reading/writing '.wt.hdf5' weights files for the ASKAP beamformer.  It should not
    be possible to create a file that will not upload to a properly configured ASKAP digital
    backend (with up to 8 beamformer chassis).
    """
    def __init__(self, fname, mode="w"):
        """_summary_

        Args:
            fname (_type_): _description_
            mode (str, optional): _description_. Defaults to "w".
        """
        super().__init__(fname, mode)
        self.attrs = {}

    def create(self, nbeam: int=128, nchannel:int =1024, nelement_x: int=256, nelement_y: int=256,
               dtype: np.dtype=DEFAULT_DTYPE):
        """
        Initialise emtpy `wt.hdf5` file according to version 1 of the schema (fixed precision
        weights).

        Args:
            nbeam (int, optional): _description_. Defaults to 128.
            nchannel (int, optional): _description_. Defaults to 1024.
            nelement_x (int, optional): _description_. Defaults to 256.
            nelement_y (int, optional): _description_. Defaults to 256.
            dtype (np.dtype, optional): _description_. Defaults to DEFAULT_DTYPE.
        """
        _LOG.info("Creating beam weight file")
        nelement = nelement_x + nelement_y

        self.create_dataset('beams', data=np.zeros(nbeam), maxshape=(nbeam,), dtype=np.int64)
        self.create_dataset('freq', data=np.zeros(nchannel), maxshape=(nchannel,), dtype=np.int64)
        self.create_dataset('eig_x', data=np.zeros((nbeam, nchannel, nelement_x), np.float64),
            maxshape=(nbeam, nchannel, nelement_x), dtype=np.float64,
            chunks=(nbeam, nchannel, nelement_x))
        self.create_dataset('eig_y', data=np.zeros((nbeam, nchannel, nelement_y), np.float64),
            maxshape=(nbeam, nchannel, nelement), dtype=np.float64,
            chunks=(nbeam, nchannel, nelement_y))
        self.create_dataset('wgt_x', data=np.zeros((nbeam, nchannel, nelement), dtype),
            maxshape=(nbeam, nchannel, nelement), dtype=dtype,
            chunks=(nbeam, nchannel, nelement))
        self.create_dataset('wgt_y', data=np.zeros((nbeam, nchannel, nelement), dtype),
            maxshape=(nbeam, nchannel, nelement), dtype=dtype,
            chunks=(nbeam, nchannel, nelement))
        self.create_dataset('pts_x', data=np.zeros((nbeam, nchannel, nelement), np.int64),
            maxshape=(nbeam, nchannel, nelement), dtype=np.int64,
            chunks=(nbeam, nchannel, nelement))
        self.create_dataset('pts_y', data=np.zeros((nbeam, nchannel, nelement), np.int64),
            maxshape=(nbeam, nchannel, nelement), dtype=np.int64,
            chunks=(nbeam, nchannel, nelement))
        self.create_dataset('y_factor_x', data=np.zeros((nbeam, nchannel), np.float64),
            dtype=np.float64, chunks=(nbeam, nchannel))
        self.create_dataset('y_factor_y', data=np.zeros((nbeam, nchannel), np.float64),
            dtype=np.float64, chunks=(nbeam, nchannel))

    def get_element_ports(self):
        """_summary_

        Returns:
            _type_: _description_
        """
        pts_x = np.asarray(self["pts_x"])
        pts_y = np.asarray(self["pts_y"])
        return np.asarray([pts_x, pts_y])

    def select_strongest(self, nelements):
        """_summary_

        Args:
            nelements (_type_): _description_

        Returns:
            _type_: _description_
        """
        weights_x = self["wgt_x"][:,:,0:nelements]
        weights_y = self["wgt_y"][:,:,0:nelements]
        return np.asarray([weights_x, weights_y])

    # def plot_weights(self, outdir="", elements=[], freq=[], show=False):
    #     if elements != []:
    #         self.n_ports_max_x = len(elements)
    #         nelement_y = len(elements)
    #     weights_x = np.zeros((nbeam, nchannel, self.n_ports_max_x), dtype=np.complex64)
    #     weights_y = np.zeros((nbeam, nchannel, nelement_y), dtype=np.complex64)
    #     pts_x = np.asarray(self['pts_x'])
    #     pts_y = np.asarray(self['pts_y'])
    #     weights_from_file_x = np.asarray(self["wgt_x"]).view(np.int16)\
    #         .astype(np.float32).view(np.complex64)
    #     weights_from_file_y = np.asarray(self["wgt_y"]).view(np.int16)\
    #         .astype(np.float32).view(np.complex64)

    #     for beam in range(nbeam):
    #         freq_list = np.arange(0,nchannel,int(nchannel*0.2)).tolist()
    #         if elements == [] or freq == []:
    #             for f in range(nchannel):
    #                     for e in range(0,nelement):
    #                         weights_x[beam,f,pts_x[beam,f,e]-1] = weights_from_file_x[beam,f,e]\
    #                             /self.MAXINT_14BIT_SIGNED
    #                         weights_y[beam,f,pts_y[beam,f,e]-self.n_ports_max_x-1] = \
    #                             weights_from_file_y[beam,f,e]/self.MAXINT_14BIT_SIGNED
    #         else:
    #             weights_x = self.get_weights_by_freq_and_elementid(freq, elements)[0]\
    #                 .view(np.int16).astype(np.float32).view(np.complex64)
    #             weights_y = self.get_weights_by_freq_and_elementid(freq, elements)[1]\
    #                 .view(np.int16).astype(np.float32).view(np.complex64)
    #             freq_list = freq.tolist()

    #         sub = [0,0,0,0]

    #         f = mplt.figure(num=beam, figsize=(12,12))
    #         sub[0] = mplt.subplot(2,2,1)
    #         sub[0].set_title("X-pol weights")
    #         sub[0].set_ylabel("Elements")
    #         sub[0].set_xlabel("Frequency [MHz]")
    #         sub[0].set_xticks(freq_list)
    #         sub[0].set_xticklabels(self["freq"][freq_list])

    #         sub[1] = mplt.subplot(2,2,2)
    #         sub[1].set_title("Y-pol weights")
    #         sub[1].set_ylabel("Elements")
    #         sub[1].set_xlabel("Frequency [MHz]")
    #         sub[1].set_xticks(freq_list)
    #         sub[1].set_xticklabels(self["freq"][freq_list])

    #         sub[2] = mplt.subplot(2,2,3)
    #         sub[2].set_ylim(.5, 5)
    #         sub[2].set_xlim(np.min(self["freq"])-0.5, np.max(self["freq"])+0.5)
    #         sub[2].plot(self["freq"], self["y_factor_x"][beam], lw=1, color="black")
    #         sub[2].set_ylabel('y-factor', fontsize=10)
    #         sub[2].set_xlabel('Frequency [MHz]', fontsize=10)

    #         sub[3] = mplt.subplot(2,2,4)
    #         sub[3].set_ylim(.5, 5)
    #         sub[3].set_xlim(np.min(self["freq"])-0.5, np.max(self["freq"])+0.5)
    #         sub[3].plot(self["freq"], self["y_factor_y"][beam], lw=1, color="black")
    #         sub[3].set_ylabel('y-factor', fontsize=10)
    #         sub[3].set_xlabel('Frequency [MHz]', fontsize=10)

    #         sub[0].imshow(np.abs(weights_x[beam]).transpose(), aspect='auto')
    #         sub[1].imshow(np.abs(weights_y[beam]).transpose(), aspect='auto')
    #         mplt.tight_layout()
    #         if outdir:
    #             mplt.savefig(outdir + "weights_beam{}.png".format(beam))
    #         if show:
    #             mplt.show()
    #         else:
    #             mplt.close(f)
