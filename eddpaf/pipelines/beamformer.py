"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:
The Beamformer is a pipeline that processes channelized voltage streams of PAF elements into beams.

Configuration Settings:
    - n_beams: Number of beams to produce
"""

# Common used modules
import json
import asyncio

from aiokatcp.connection import FailReply

from mpikat.core.edd_pipeline_aio import launchPipelineServer, state_change
from mpikat.core import logger as logging
from mpikat.utils.core_manager import CoreManager
from mpikat.utils import dada_tools as dada

from eddpaf.pipelines.cbf_base import CBFBase
from eddpaf.data_streams import ChannelizerDataStreamFormat as FStreamFormat
from eddpaf.data_streams import VoltageBeamDataStreamFormat as VStreamFormat
from eddpaf.data_streams import FullStokesBeamDataStreamFormat as SStreamFormat
from eddpaf.data_streams import PowerBeamDataStreamFormat as PStreamFormat



_LOG = logging.getLogger("mpikat.eddpaf.pipelines.Beamformer")

# CONSTANTS
MAX_RAW_BEAMS = 16
N_POL = 2

#pylint: disable=R0801
# The dictionary below is the default configuration of this EDD pipeline
_DEFAULT_CONFIG = {
    # Pipeline identifiers
    "id": "Beamformer", # Id of pipeline (automatically set by ansible)
    "type": "Beamformer",
    # General parameters used for calculation and configuration
    "n_heap_groups":16,
    "n_beams" : 128, # Number of beams that should be processed
    "nfft" : 32, # Size of 2nd FFT
    "numerator" : 32, # Oversample numerator
    "denumerator" : 27, # Oversample denumerator
    "integration": 64, # Accumulation / integration used for Stokes I beamformer to integrate
    "n_capture_threads":16,
    "element_start":0,
    "element_stop":256,
    "element_step":1,
    "weight_scale":1,
    "input_type":"network",
    "output_type" : "network", # Possible options: "network", "disk", "null"
    "output_directory" : "/mnt", # Location for all data dumps
    "nonfatal_numacheck":False,
    # Input stream configuration
    "input_data_streams":
    [
        {
            "format" : "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.0",
            "port" : 7148
        },
        {
            "format" : "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.1",
            "port" : 7148
        },
        {
            "format" : "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.2",
            "port" : 7148
        },
        {
            "format" : "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.3",
            "port" : 7148
        },
        {
            "format" : "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.4",
            "port" : 7148
        },
        {
            "format" : "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.5",
            "port" : 7148
        },
        {
            "format" : "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.6",
            "port" : 7148
        },
        {
            "format" : "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.7",
            "port" : 7148
        },
        {
            "format" : "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.8",
            "port" : 7148
        },
        {
            "format" : "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.9",
            "port" : 7148
        },
        {
            "format" : "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.10",
            "port" : 7148
        },
        {
            "format" : "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.11",
            "port" : 7148
        }
    ],
    # Output stream configuration
    "output_data_streams":
    {
        "voltage_beam_00":
        {
            "format" : "VoltageBeamDataStreamFormat:1",
            "ip": "auto",
            "port" : 8125
        },
        "stokes_i_beams_000":
        {
            "format" : "PowerBeamDataStreamFormat:1",
            "ip": "auto",
            "port" : 8125
        },
        "full_stokes_beams_000":
        {
            "format" : "FullStokesBeamDataStreamFormat:1",
            "ip": "auto",
            "port" : 8125
        }
    }
}
#pylint: enable=R0801

class Beamformer(CBFBase):
    """
    @brief An EDDPipeline to process beams from raw voltages of the current PAF in Effelsberg
    """
    VERSION_INFO = ("mpikat-edd-api", 0, 1)
    BUILD_INFO = ("mpikat-edd-implementation", 0, 1, "rc1")

    def __init__(self, ip: str, port: int, loop: asyncio.AbstractEventLoop=None):
        super().__init__(ip, port, default_config=_DEFAULT_CONFIG, loop=loop)

    # ------------------------------ #
    # Methods to setup KATCP sensors #
    # ------------------------------ #
    # def setup_sensors(self):
    #     """
    #     Setup monitoring sensors
    #     """
    #     super().setup_sensors()
        # self._mkrecv_sensor = MkrecvSensors(self._config["id"] + "_input_stream")
        # for sensor in self._mkrecv_sensor.sensors.values():
        #     self.sensors.add(sensor)

    #------------------------- #
    # EDD State change methods #
    #------------------------- #
#pylint: disable=R0915,R0914
    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        @brief   Configure beamform pipeline

        @param   config_json    A JSON dictionary object containing configuration information
        """
        await super().configure()
        if self._config["n_beams"]%16:
            raise FailReply("key 'n_beams' must be a multiple of 16")
        sample_rate = self._config["input_data_streams"][0]["sample_rate"]
        #---------------------------------------------#
        # Calculations based on latest configurations #
        #---------------------------------------------#
        # General calculations
        # Calculate params for input raw voltage
        input_heap_sz = FStreamFormat.samples_per_heap
        n_samples_per_heap_input = input_heap_sz // FStreamFormat.stream_meta["bit_depth"] * 8
        input_num_heaps = self.n_element * self.n_samples * self.n_channel\
            / n_samples_per_heap_input
        input_buffer_sz = int(input_heap_sz * input_num_heaps)
        input_data_rate = self.n_channel \
            * self.n_element \
            * float(sample_rate) \
            * FStreamFormat.stream_meta["bit_depth"] / 8
        # Calculate params for voltage output
        output_voltage_heap_size = VStreamFormat.samples_per_heap
        n_samples_per_heap_voltage = output_voltage_heap_size\
            // (VStreamFormat.stream_meta["bit_depth"]*N_POL) * 8
        output_voltage_num_heaps = MAX_RAW_BEAMS * self.n_samples * self.n_channel\
            / n_samples_per_heap_voltage
        output_voltage_buffer_sz = int(output_voltage_heap_size * output_voltage_num_heaps)
        output_voltage_data_rate = self.n_channel \
            * MAX_RAW_BEAMS * N_POL \
            * sample_rate \
            * self.n_channel \
            * VStreamFormat.stream_meta["bit_depth"] / 8
        # Calculate params for stokes I output
        output_stokesi_heap_size = PStreamFormat.samples_per_heap
        n_samples_per_heap_stokesi = output_stokesi_heap_size\
            // PStreamFormat.stream_meta["bit_depth"] * 8
        output_stokesi_num_heaps = self._config["n_beams"] * self.n_samples * self.n_channel\
            / (n_samples_per_heap_stokesi * self._config["nfft"])
        output_stokesi_buffer_sz = int(output_stokesi_heap_size * output_stokesi_num_heaps)
        output_stokesi_data_rate = self.n_channel \
            * self._config["n_beams"] \
            * sample_rate \
            * self.n_channel \
            * PStreamFormat.stream_meta["bit_depth"] \
            * self._config["denumerator"] \
            / (8*self._config["numerator"]*self._config["nfft"])
        # Calculate params for full stokes output
        output_stokesf_heap_size = SStreamFormat.samples_per_heap
        n_samples_per_heap_stokesf = output_stokesf_heap_size //\
            SStreamFormat.stream_meta["bit_depth"] * 8\
            # * (1 - 1/self._config["numerator"]\
            # * (self._config["numerator"] - self._config["denumerator"]))
        output_stokesf_num_heaps = self._config["n_beams"] * self.n_samples * self.n_channel\
            / (n_samples_per_heap_stokesf * self._config["integration"])
        output_stokesf_buffer_sz = int(self.n_samples \
             * self._config["n_beams"] \
             * self.n_channel \
             * (1 - 1/self._config["numerator"] \
                * (self._config["numerator"] - self._config["denumerator"])) \
             * SStreamFormat.stream_meta["bit_depth"] \
             / (self._config["integration"] * 8))

        output_stokesf_data_rate = self.n_channel \
            * self._config["n_beams"] \
            * SStreamFormat.stream_meta["bit_depth"] \
            * sample_rate \
            * self.n_channel \
            * self._config["denumerator"] \
            / (8*self._config["integration"]*self._config["numerator"])
        # create dada buffers
        self._dada_buffers.append(dada.DadaBuffer(input_buffer_sz, n_slots=32,
                                                  node_id=self._numa_node, pin=True, lock=True))
        self._dada_buffers.append(dada.DadaBuffer(output_voltage_buffer_sz, n_slots=32,
                                                  node_id=self._numa_node, pin=True, lock=True))
        self._dada_buffers.append(dada.DadaBuffer(output_stokesi_buffer_sz, n_slots=32,
                                                  node_id=self._numa_node, pin=True, lock=True))
        self._dada_buffers.append(dada.DadaBuffer(output_stokesf_buffer_sz, n_slots=32,
                                                  node_id=self._numa_node, pin=True, lock=True))
        buffer_tasks = []
        for buf in self._dada_buffers:
            buffer_tasks.append(asyncio.create_task(buf.create()))

        _LOG.info('\nParameters for input stream:\n\
            DADA key:              %s \n\
            Heap size:             %d byte\n\
            Samples per heap:      %d \n\
            Heaps per block:       %d\n\
            Total buffer size:     %d byte\n\
            Datarate:              %.2f Bytes/s',
                self._dada_buffers[0].key,
                input_heap_sz,
                n_samples_per_heap_input,
                input_num_heaps,
                input_buffer_sz,
                input_data_rate)
        _LOG.info('\nParameters for output voltage stream:\n\
            DADA key:              %s \n\
            Heap size:             %d byte\n\
            Samples per heap:      %d \n\
            Heaps per block:       %d \n\
            Total buffer size:     %d byte\n\
            Datarate:              %.2f Bytes/s',
                self._dada_buffers[1].key,
                output_voltage_heap_size,
                n_samples_per_heap_voltage,
                output_voltage_num_heaps,
                output_voltage_buffer_sz,
                output_voltage_data_rate)
        _LOG.info('\nParameters for output stokes I stream (high temporal res.):\n\
            DADA key:              %s \n\
            Heap size:             %d byte\n\
            Samples per heap:      %d \n\
            Heaps per block:       %d\n\
            Total buffer size:     %d byte\n\
            Datarate:              %.2f Bytes/s',
                self._dada_buffers[2].key,
                output_stokesi_heap_size,
                n_samples_per_heap_stokesi,
                output_stokesi_num_heaps,
                output_stokesi_buffer_sz,
                output_stokesi_data_rate)
        _LOG.info('\nParameters for output full stokes stream (high frequency res.):\n\
            DADA key:              %s \n\
            Heap size:             %d byte\n\
            Samples per heap:      %d \n\
            Heaps per block:       %d\n\
            Total buffer size:     %d byte\n\
            Datarate:              %.2f Bytes/s',
                self._dada_buffers[3].key,
                output_stokesf_heap_size,
                n_samples_per_heap_stokesf,
                output_stokesf_num_heaps,
                output_stokesf_buffer_sz,
                output_stokesf_data_rate)

        self._update_output_voltage_streams()
        # # specify all subprocesses
        self._core_manager = CoreManager(self._numa_node)
        self._core_manager.add_task("capture_proc", self._config["n_capture_threads"]+1,
                                    prefere_isolated=True)
        self._core_manager.add_task("processing", 2)
        self._core_manager.add_task("weightupdater", 1)
        self._core_manager.add_task("mksend_voltage", 1)
        self._core_manager.add_task("mksend_full_stokes", 1)
        self._core_manager.add_task("mksend_stokes_i", 1)

        await asyncio.gather(*buffer_tasks)
        self.add_buffer_sensor(self._dada_buffers[0].key)
        self.add_buffer_sensor(self._dada_buffers[1].key)
        self.add_buffer_sensor(self._dada_buffers[2].key)
        self.add_buffer_sensor(self._dada_buffers[3].key)
        self.start_buffer_monitoring()
        _LOG.debug("Ringbuffers created")
        # Launch processing process
        print(self.cmd_beamform())
        self.start_process(self.cmd_beamform(),
                           env={"CUDA_VISIBLE_DEVICES": str(self._cuda_device)})
        # self.start_process(self.cmd_weight_interface())
        self.start_process(self.cmd_output(VStreamFormat.name, self._dada_buffers[1].key,
                                           "mksend_voltage"))
        self.start_process(self.cmd_output(SStreamFormat.name, self._dada_buffers[2].key,
                                           "mksend_full_stokes"))
        self.start_process(self.cmd_output(PStreamFormat.name, self._dada_buffers[3].key,
                                           "mksend_stokes_i"))
        self._configUpdated()
        _LOG.debug("Final config: %s", json.dumps(self._config, indent=4))
#pylint: enable=R0915,R0914

    @state_change(target="streaming", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        await super().capture_start()

    @state_change(target="configured", allowed=["streaming"], intermediate="capture_stopping")
    async def capture_stop(self):
        await super().capture_stop()

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        await super().deconfigure()

    # -------------------------------------------- #
    # Methods for launching and handling processes #
    # -------------------------------------------- #
    def cmd_beamform(self):
        """_summary_

        Returns:
            _type_: _description_
        """
        return f"taskset -c {self._core_manager.get_coresstr('processing')} beamformer_cli \
                --in_key {self._dada_buffers[0].key} \
                --out_vol_key {self._dada_buffers[1].key} \
                --out_sti_key {self._dada_buffers[2].key} \
                --out_stf_key {self._dada_buffers[3].key} \
                --samples {self.n_samples} \
                --channels {self.n_channel} \
                --elements {self.n_element // 2} \
                --beams {self._config['n_beams']} \
                --integration {self._config['integration']}".replace("\n","")

    def cmd_weight_interface(self):
        """_summary_

        Returns:
            _type_: _description_
        """
        return f"taskset -c {self._core_manager.get_coresstr('weightupdater')} weightupdater_cli \
                --redis_channel {self._config['id']} \
                --channels {self.n_channel} \
                --elements {self.n_element // 2} \
                --beams {self._config['n_beams']} \
                --scale {self._config['weight_scale']} \
                --host {self._config['data_store']['ip']} \
                --port {self._config['data_store']['port']}"

    def cmd_output(self, name, key, process_name):
        """_summary_

        Args:
            name (_type_): _description_
            key (_type_): _description_
            process_name (_type_): _description_

        Returns:
            _type_: _description_
        """

        _LOG.info("Creating out handle command for stream %s", name)
        # Network transmission via mksend / spead
        if self._config["output_type"] == "network":
            cmd = process_name
        # Data to disk
        elif self._config["output_type"] == "disk":
            cmd = dada.DbDisk(key, self._config["output_directory"]).cmd
        # No recording or transmission
        elif self._config["output_type"] == "null":
            cmd = dada.DbNull(key).cmd
        else:
            _LOG.error("output_type not known which is %s", self._config["output_type"])
            return ""
        return cmd


    def _update_output_voltage_streams(self):
        for i in range(MAX_RAW_BEAMS):
            stream_name = f"voltage_beam_{i:02d}"
            if stream_name in self._config["output_data_streams"]:
                continue
            self._config["output_data_streams"][stream_name] = {
                "format" : "CRYO_PAF_CBF_VOLTAGE_BEAM:1",
                "ip": f"226.0.1.0{i:02d}",
                "port" : 17101,
                "bit_depth": VStreamFormat.stream_meta["bit_depth"]
            }

if __name__ == "__main__":
    asyncio.run(launchPipelineServer(Beamformer))
