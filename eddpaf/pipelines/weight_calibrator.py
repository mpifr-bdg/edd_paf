"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:
TBD

Configuration Settings:
TBD
"""
import asyncio
# from aiokatcp.sensor import Sensor
# from aiokatcp.connection import FailReply

from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change
# from mpikat.core.datastore import EDDDataStore
from mpikat.core import logger as logging

# from eddpaf.beam_weights import BeamWeightFile

_LOG = logging.getLogger("mpikat.eddpaf.pipelines.WeightCalibrator")


_DEFAULT_CONFIG = {
    "id": "WeightCalibrator",
    "type": "WeightCalibrator",
    "output_data_streams": [],
    "input_data_streams": [],
    "data_store" :
    {
        "host" : "pacifix6",
        "port" : 6379
    },
    "input_type":"network",
    "output_type":"disk",
    "output_directory":"/mnt/"
}

class WeightCalibrator(EDDPipeline):
    """Computes the beam weights

    Args:
        EDDPipeline (_type_): _description_
    """
    def __init__(self, ip: str, port: int, loop: asyncio.AbstractEventLoop=None):
        """_summary_

        Args:
            ip (str): _description_
            port (int): _description_
            loop (asyncio.AbstractEventLoop, optional): _description_. Defaults to None.
        """
        super().__init__(ip, port, default_config=dict(_DEFAULT_CONFIG), loop=loop)
        self._beamformer_configs = []
        self._package_cnt = 0

    # def setup_sensors(self):
    #     """Setup monitoring sensors
    #     """
    #     super().setup_sensors()

    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        _LOG.info("Configuring WeightCalibrator")

    @state_change(target="ready", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        _LOG.info("WeightCalibrator starts capturing")

    @state_change(target="set", allowed=["ready"], intermediate="measurement_preparing")
    async def measurement_prepare(self, config_json={}):
        _LOG.info("WeightCalibrator prepares measurement")

    @state_change(target="measuring", allowed=["set"], intermediate="measurement_starting")
    async def measurement_start(self):
        _LOG.info("WeightCalibrator starts measuring")

    @state_change(target="ready", allowed=["measuring", "set"], ignored=["ready"],
                  intermediate="measurement_stopping")
    async def measurement_stop(self):
        _LOG.info("WeightCalibrator stops measuring")

    @state_change(target="idle", allowed=["ready", "set"], intermediate="capture_stopping")
    async def capture_stop(self):
        _LOG.info("WeightCalibrator  stops capturing")

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        _LOG.info("WeightCalibrator Correlator")

if __name__ == "__main__":
    launchPipelineServer(WeightCalibrator)
