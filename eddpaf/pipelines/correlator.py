"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:
The Correlator is a pipeline that processes channelized voltage streams of PAF elements into ACMs.

Configuration Settings:
    TBD
"""
import asyncio
import os

from mpikat.core.edd_pipeline_aio import launchPipelineServer, state_change
from mpikat.core import logger as logging
from mpikat.utils import dada_tools as dada

from eddpaf import folder_string
from eddpaf.pipelines.cbf_base import CBFBase
from eddpaf.data_streams import ChannelizerDataStreamFormat as FStream
from eddpaf.data_streams import CorrelatorDataStreamFormat as XStream
from eddpaf.data_streams import corr_mksend_cmd

_LOG = logging.getLogger("mpikat.eddpaf.pipelines.correlator")


#pylint: disable=R0801
_DEFAULT_CONFIG = {
    "id": "Correlator",
    "type": "Correlator",
    # General parameters used for calculation and configuration
    "n_heap_groups":16,
    "element_start":0,
    "element_stop":8,
    "element_step":1,
    "input_data_streams": [
        {
            "format": "ChannelizerDataStreamFormat:1",
            "ip": "225.0.0.0+255",
            "port" : 7148
        }
    ],
    "output_data_streams": {
        "acm_stream_channel_0":{
            "format" : "CorrelatorDataStreamFormat:1",
            "ip": "auto",
            "port" : 7148
        }
    },
    # User inputs
    "nonfatal_numacheck":False,
    "input_type":"network",
    "output_type":"disk",
    "output_directory":"/mnt",
    "n_accumulate": 262144,
    "n_capture_threads":16
}
#pylint: enable=R0801


class Correlator(CBFBase):
    """_summary_
    """
    def __init__(self, ip: str, port: int, loop: asyncio.AbstractEventLoop=None):
        super().__init__(ip, port, default_config=_DEFAULT_CONFIG, loop=loop)

    # def setup_sensors(self):
    #     """Setup monitoring sensors
    #     """
    #     super().setup_sensors()

    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        await super().configure()
        n_accumul = int(self._config["n_accumulate"])
        if n_accumul % FStream.samples_per_heap:
            _LOG.error("Accumlations must be multiple of samples per heap!")
            raise ValueError("Accumlations must be multiple of samples per heap!")
        buffer_tasks = []

        input_buffer_sz = self.n_element * self.n_channel * n_accumul * FStream.itemsize
        output_buffer_sz = self.n_element * self.n_element * self.n_channel * XStream.itemsize
        self._dada_buffers.append(dada.DadaBuffer(input_buffer_sz, n_slots=32,
                                                  node_id=self._numa_node, lock=True, pin=True))
        self._dada_buffers.append(dada.DadaBuffer(output_buffer_sz, n_slots=32,
                                                  node_id=self._numa_node, lock=True, pin=True))
        for buf in self._dada_buffers:
            buffer_tasks.append(asyncio.create_task(buf.create()))
        await asyncio.gather(*buffer_tasks)
        # Reserve cores
        self._core_manager.add_task("output_proc", 2,  prefere_isolated=False)
        self._core_manager.add_task("processing_proc", 1,  prefere_isolated=False)
        self._core_manager.add_task("capture_proc", self._config["n_capture_threads"]+1,
                                    prefere_isolated=True)
        # Start Processes
        self.start_process(self.cmd_output())
        self.start_process(self.cmd_correlator(), env={"LD_LIBRARY_PATH":"/usr/local/lib/"})

        self.add_mkrecv_sensor(self._dada_buffers[0].key)
        self.add_buffer_sensor(self._dada_buffers[0].key)
        self.add_buffer_sensor(self._dada_buffers[1].key)
        self.start_buffer_monitoring()

    @state_change(target="streaming", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        await super().capture_start()

    @state_change(target="idle", allowed=["streaming"], intermediate="capture_stopping")
    async def capture_stop(self):
        await super().capture_stop()

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        await super().deconfigure()

    def cmd_output(self) -> str:
        """Start output process
        """
        desc = dict(self._config["output_data_streams"]) # ToDo: Parse informations for mksend
        desc["physcpu"] = self._core_manager.get_coresstr("output_proc")
        if self._config["output_type"] == "network":
            s = corr_mksend_cmd(desc)
        elif self._config["output_type"] == "disk":
            ofpath = folder_string(self._configure_ts, prefix=self._config["output_directory"])
            _LOG.info("Writing output to %s", ofpath)
            if not os.path.isdir(ofpath):
                os.makedirs(ofpath)
            s = dada.DbDisk(self._dada_buffers[1].key,
                            os.path.join(self._config["output_directory"], ofpath)).cmd
        elif self._config["output_type"] == "null":
            s = dada.DbNull(self._dada_buffers[1].key).cmd
        else:
            _LOG.error("output_type not known which is %s", self._config["output_type"])
            return ""
        _LOG.info("Output command: %s", s)
        return s

    def cmd_correlator(self) -> str:
        """
        Get the correlator command
        """
        return f'numactl --cpunodebind {self._numa_node} correlator_cli \
            --in_key {self._dada_buffers[0].key} \
            --out_key {self._dada_buffers[1].key} \
            --sample {self._config["n_accumulate"]} \
            --channel {len(self._ip_list)} \
            --element {self.n_element//2} \
            --npol {2} \
            --nacc {self._config["n_accumulate"]} \
            --nbit {8}'

if __name__ == "__main__":
    asyncio.run(launchPipelineServer(Correlator))
