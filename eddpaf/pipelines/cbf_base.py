"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:
The CBFBase is a pipeline sharing common members and functioniality between beamformer and
correlator

"""
import asyncio
import time
from typing import List, Set

from aiokatcp.sensor import Sensor
from aiokatcp.connection import FailReply

from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer
from mpikat.core import logger as logging
from mpikat.utils.process_monitor import SubprocessMonitor
from mpikat.utils.sensor_watchdog import SensorWatchdog, conditional_update
from mpikat.utils.mk_tools.mksensor import MkrecvSensors
from mpikat.utils.process_tools import ManagedProcessPool, ManagedProcess
from mpikat.utils import dada_tools as dada
from mpikat.utils import numa
from mpikat.utils.core_manager import CoreManager

from eddpaf import viable_cuda_devices, sanitize_dictionary
from eddpaf.data_streams import ChannelizerDataStreamFormat as FStream
from eddpaf.data_streams import channelizer_mkrecv_cmd, channelizer_mkrecv_header

_LOG = logging.getLogger("mpikat.eddpaf.pipelines.correlator")

class CBFBase(EDDPipeline):
    """
    The CBFBase is a pipeline sharing common members and functioniality between beamformer and
    correlator
    """

    CUDA_MAJOR: int = 7
    CUDA_MINOR: int = 5
    DATA_FORMATS = [FStream.stream_meta]

    def __init__(self, ip: str, port: int, default_config: dict,
                 loop: asyncio.AbstractEventLoop=None):
        """_summary_

        Args:
            ip (str): _description_
            port (int): _description_
            default_config (dict): _description_
            loop (asyncio.AbstractEventLoop, optional): _description_. Defaults to None.
        """
        super().__init__(ip, port, default_config=default_config, loop=loop)
        self._dada_buffers: List[dada.DadaBuffer] = []
        self._buffer_sensors: dict = {}
        self._watchdog: SensorWatchdog = None
        self._numa_node: int = None
        self._configure_ts: int = None
        self._sp_monitor: SubprocessMonitor = None
        self._capture_proc: ManagedProcess = None
        self._process_pool: ManagedProcessPool = ManagedProcessPool()
        self._core_manager: CoreManager = None
        self._ip_list: Set[str] = set()
        self._channel_list: Set[int] = set()
        self._cuda_device: int = None
        self.fastest_nic: str = None
        self.nic_params: dict = None
        self.n_element: int = 0
        self.n_channel: int = 0
        self.n_samples: int = 0


    # def setup_sensors(self):
    #     """Setup monitoring sensors
    #     """
    #     super().setup_sensors()

    async def configure(self):
        _LOG.info("%s configuring", self._config["id"])
        self._ip_list = set()
        self._channel_list = set()
        self._set_numa_node()
        self._validate_input_data_stream()
        self._validate_num_threads()
        self._core_manager = CoreManager(self._numa_node)
        self._configure_ts = time.time()
        self._sp_monitor = SubprocessMonitor()
        self.n_element = len(range(self._config["element_start"],
                                   self._config["element_stop"],
                                   self._config["element_step"]))
        self.n_channel = len(self._config["input_data_streams"])  * FStream.channels
        self.n_samples = self._config["n_heap_groups"] * FStream.samples_per_heap

    async def capture_start(self):
        _LOG.info("%s starts capturing", self._config["id"])
        self._capture_proc = self.start_process(self.cmd_capture())
        self._watchdog = SensorWatchdog(
            self._buffer_sensors[self._dada_buffers[0].key]["buffer_total_write"],
            30, self.watchdog_error)
        self._watchdog.start()

    async def capture_stop(self):
        _LOG.info("%s stops capturing", self._config["id"])
        self._stop_capture()

    async def deconfigure(self):
        _LOG.info("%s deconfiguring", self._config["id"])
        self._stop_capture()
        if self._sp_monitor is not None:
            self._sp_monitor.stop()
        self._process_pool.terminate(threaded=True)
        self._process_pool.clean()
        # Destroy dada buffers
        destroy_tasks = []
        for buffer in self._dada_buffers:
            destroy_tasks.append(asyncio.create_task(buffer.destroy()))
        await asyncio.gather(*destroy_tasks)
        # Reset attributes
        self._dada_buffers = []
        self._core_manager = None
        self._numa_node = None
        self._watchdog = None

    def _stop_capture(self):
        """
        """
        if isinstance(self._watchdog, SensorWatchdog):
            self._watchdog.stop_event.set()
        if isinstance(self._capture_proc, ManagedProcess) and self._capture_proc.is_alive():
            self._capture_proc.terminate()
        self._watchdog = None
        self._capture_proc = None


    def start_process(self, cmd: str, env=None) -> ManagedProcess:
        proc = ManagedProcess(cmd, env=env)
        self._sp_monitor.add(proc, self._subprocess_error)
        self._process_pool.add(proc)
        return proc

    def _validate_num_threads(self):
        """
        """
        ncores = len(numa.getInfo()[self._numa_node]["isolated_cores"])
        if self._config["input_type"] == "dummy":
            self._config["n_capture_threads"] = 1
        if self._config["n_capture_threads"] > ncores:
            self._config["n_capture_threads"] = ncores
            _LOG.warning("Too many threads requested changing to %d", ncores)
        if self._config["n_capture_threads"] > len(self._ip_list):
            self._config["n_capture_threads"] = len(self._ip_list)
            _LOG.warning("More threads requested as subscribed multicast addresses")

    def _set_numa_node(self):
        """ Sets the NUMA node and gathers its information
        """
        # Get NUMA node and its charachteristics
        for node_id, desc in numa.getInfo().items():
            viable_dev = viable_cuda_devices(desc["gpus"], self.CUDA_MAJOR, self.CUDA_MINOR)
            # remove numa nodes with missing capabilities
            if len(desc['net_devices']) < 1:
                _LOG.debug("Not enough nics on numa node %d", node_id)
                continue
            if len(viable_dev) < 1:
                _LOG.debug("No viable CUDA device on numa node %d.", node_id)
                continue
            self._numa_node = node_id
            self._cuda_device = viable_dev[0]
            break

        if self._numa_node is None:
            if self._config["nonfatal_numacheck"] is False:
                _LOG.warning("No numa node available, but 'nonfatal_numacheck' enabled")
                self._numa_node = '0'
            else:
                _LOG.error("No numa node available to process data!")
                raise FailReply("No numa node available to process data!")
        self.fastest_nic, self.nic_params = numa.getFastestNic(self._numa_node)
        _LOG.info("Selected node %s. Will receive data on NIC %s [ %s ] @ %f Mbit/s",
                  self._numa_node, self.fastest_nic, self.nic_params["ip"], self.nic_params['speed'])

    def start_buffer_monitoring(self):
        """
        """
        for buf in self._dada_buffers:
            buf.get_monitor(callback=self.buffer_status_handle).start()

    def add_buffer_sensor(self, key: str):
        """
        add sensors for i/o buffers
        """
        if key not in self._buffer_sensors:
            self._buffer_sensors[key] = {}
        self._buffer_sensors[key]["buffer_fill_level"] = Sensor(
            float, f'buffer-fill-level-{key}',
            description=f"Fill level of the buffer {key}",
            initial_status=Sensor.Status.UNKNOWN)
        self._buffer_sensors[key]["buffer_total_write"] = Sensor(
            float, f"buffer-total-write-{key}",
            description=f"Total write of the buffer {key}",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._buffer_sensors[key]["buffer_fill_level"])
        self.sensors.add(self._buffer_sensors[key]["buffer_total_write"])
        self.mass_inform("interface-changed")

    def add_mkrecv_sensor(self, key: str):
        """
        Add sensors for monitoring mkrecv
        """
        if key not in self._buffer_sensors:
            self._buffer_sensors[key] = {}
        self._buffer_sensors[key]["mkrecv_sensors"] = MkrecvSensors(key)
        for s in self._buffer_sensors[key]["mkrecv_sensors"].sensors.values():
            self.sensors.add(s)
        self.mass_inform("interface-changed")


    def buffer_status_handle(self, status: dict):
        """
        Process a change in the DADA buffer status

        Args:
            status (dict): A dictionary containing information of the DADA buffer status
        """
        timestamp = time.time()
        key = status["key"]
        conditional_update(self._buffer_sensors[key]["buffer_fill_level"],
                           status['fraction-full'], timestamp=timestamp)
        conditional_update(self._buffer_sensors[key]["buffer_total_write"],
                           status['written'], timestamp=timestamp)


    def _validate_input_data_stream(self):
        # Validate input data streams
        for idx, istream in enumerate(self._config["input_data_streams"]):
            istream = sanitize_dictionary(istream, FStream.stream_meta)
            if istream["sample_rate"] != self._config["input_data_streams"][0]["sample_rate"]:
                _LOG.error("Unmatching sample_rate in stream %d", idx)
                raise FailReply(f"Unmatching sample_rate in stream {idx}")
            if istream["port"] != self._config["input_data_streams"][0]["port"]:
                _LOG.error("Unmatching port in stream %d", idx)
                raise FailReply(f"Unmatching port in stream {idx}")
            if istream["sync_time"] != self._config["input_data_streams"][0]["sync_time"]:
                _LOG.error("Unmatching sync_time in stream %d", idx)
                raise FailReply(f"Unmatching sync_time in stream {idx}")
            self._ip_list.add(istream["ip"])
            for chan in range(FStream.channels):
                self._channel_list.add(int(istream["channel_id"])+chan)

    def cmd_capture(self) -> str:
        """
        Get the capture command
        """
        magic_number = int(0x00010000)
        channel_ids = ",".join([str(chan * magic_number) for chan in set(self._channel_list)])
        desc = dict(self._config["input_data_streams"][0]) # is a copy because of dict()
        desc["key"] = self._dada_buffers[0].key
        desc["nic"] = self.nic_params["ip"] # Only viable for edd05
        desc["channel_list"] = channel_ids
        desc["element_start"] = self._config["element_start"]
        desc["element_stop"] = self._config["element_stop"]
        desc["element_step"] = self._config["element_step"]
        desc["mst"] = len(self._ip_list) // self._config["n_capture_threads"]
        desc["physcpu"] = self._core_manager.get_coresstr("capture_proc")
        if self._config["input_type"] == "network":
            s = channelizer_mkrecv_cmd(desc)
        elif self._config["input_type"] == "dummy":
            file_header = channelizer_mkrecv_header(desc)
            s = dada.JunkDb(self._dada_buffers[0].key,
                            header=file_header,
                            data_rate=desc["sample_rate"] * len(self._ip_list) * self.n_element,
                            duration=3600, charachter=1).cmd
        else:
            s = ""
        _LOG.debug("Capture command: %s", s)
        return s


if __name__ == "__main__":
    asyncio.run(launchPipelineServer(CBFBase))