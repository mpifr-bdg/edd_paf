"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:
The ComplexGainCalibrator receives outputs from correlator pipeline instances identifies
physical delays from the ACMs

Configuration Settings:
TBD
"""
import asyncio
from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change
from mpikat.core import logger as logging
# from mpikat.utils.spead_capture import SpeadCapture

# from eddpaf.data_streams import CorrelatorSpeadHandler

_LOG = logging.getLogger("mpikat.eddpaf.pipelines.ACMWriterPipeline")


_DEFAULT_CONFIG = {
    "id": "ComplexGainCalibrator",
    "type": "ComplexGainCalibrator",
    "input_data_streams": [],
    "data_store" :
    {
        "host" : "pacifix6",
        "port" : 6379
    },
    "input_type":"network",
    "output_type":"disk",
    "output_directory":"/mnt/"
}


class ComplexGainCalibrator(EDDPipeline):
    """ComplexGainCalibrator

    Args:
        EDDPipeline (_type_): _description_
    """

    def __init__(self, ip: str, port: int, loop: asyncio.AbstractEventLoop=None):
        """_summary_

        Args:
            ip (str): _description_
            port (int): _description_
            loop (asyncio.AbstractEventLoop, optional): _description_. Defaults to None.
        """
        super().__init__(ip, port, default_config=dict(_DEFAULT_CONFIG), loop=loop)

    # def setup_sensors(self):
    #     """Setup monitoring sensors
    #     """
    #     super().setup_sensors()

    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        _LOG.info("Configuring CG Calibrator")


    @state_change(target="ready", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        _LOG.info("CG Calibrator starts capturing")

    @state_change(target="set", allowed=["ready"], intermediate="measurement_preparing")
    async def measurement_prepare(self, config_json={}):
        _LOG.info("CG Calibrator prepares measurement")

    @state_change(target="measuring", allowed=["set"], intermediate="measurement_starting")
    async def measurement_start(self):
        _LOG.info("CG Calibrator starts measuring")

    @state_change(target="ready", allowed=["measuring", "set"], ignored=["ready"],
                  intermediate="measurement_stopping")
    async def measurement_stop(self):
        _LOG.info("CG Calibrator stops measuring")

    @state_change(target="idle", allowed=["ready"], intermediate="capture_stopping")
    async def capture_stop(self):
        _LOG.info("CG Calibrator  stops capturing")

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        _LOG.info("CG Calibrator Correlator")

if __name__ == "__main__":
    launchPipelineServer(ComplexGainCalibrator)
